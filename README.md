NGhostCommon
============

Introduction
------------

This project contains all data model/function
relative to all ghost projects

Token manager
-------------

The token manager is a way to obtain authentication to a ghost
device.

In a normal way, you should have to send username/password to device
when trying to communicate with it. Be when scanning for devices, and
adding unthrusted found items, it will be absolutly unsecured to send
configuration username and password to try to authenticate.

The solution is found through tokens. When the ghost client scans for
devices, it will then try to obtain a token with a GET request to the
`/ghost/token` URI.

On the other side, the requested will always be refused, except if accepted
explicitly with an avaible physical authorization method.

- Button connected to GPIO pressure on a raspberry PI
- Text input in terminal ("AllowToken") for other type of device
- Trusted IP if set in configuration file
- If you already have an access, just use your access way, and ask for a token

Action manager
--------------

> Action path

/ghost/action

> Options

- Push an existing action to known ghost watcher

  PUT /ghost/action/ACTION_HASH

- Execute an existing action

  POST /ghost/action/ACTION_HASH

For both previous requests, you can pass parameters with syntax:

```json
{
	"parameter": [ "Value1", 356 ]
}
```

- Add an action to service (by other services)

```json
{
	"count": 2,
	"0": {
		"type": "TypeAction",
		"sentence": "PhraseAction",
		"hostname": "127.0.0.1",
		"port": 12345,
		"requestURL": "REQUEST/URL",
		"requestType": "POST"
	},
	"1": {
		"type": "TypeAction",
		"sentence": "PhraseAction",
		"hostname": "127.0.0.1",
		"port": 12345,
		"requestURL": "REQUEST/URL",
		"requestType": "POST"
	}
}
```

Example:

```bash
curl test:test@127.0.0.1:16560/ghost/action/ -X PATCH -d "{ \"count\": 1, \"0\": { \"type\": \"Ouvrir porte\", \"sentence\": \"mdr c'est une action\", \"hostname\": \"192.168.0.109\", \"port\": 12345, \"requestURL\": \"/mdr/test\", \"requestType\": \"POST\" } }"
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostCommon.git
