#ifndef NGHOSTCOMMON_EVENT_NGHOSTEVENTSOURCE_PROTECT
#define NGHOSTCOMMON_EVENT_NGHOSTEVENTSOURCE_PROTECT

// ---------------------------------------------
// struct NGhostCommon::Event::NGhostEventSource
// ---------------------------------------------

typedef struct NGhostEventSource
{
	// IP
	char *m_ip;

	// Adding device type (if known or NULL)
	NDeviceType m_device;

	// Adding device UUID (if known or NULL)
	char *m_uuid;
} NGhostEventSource;

/**
 * Build event source instance
 *
 * @param ip
 * 		The sender ip
 * @param deviceType
 * 		The device type
 * @param deviceUUID
 * 		The device UUID (can be NULL)
 *
 * @return the built instance
 */
__ALLOC NGhostEventSource *NGhostCommon_Event_NGhostEventSource_Build( const char *ip,
	NDeviceType deviceType,
	const char *deviceUUID );

/**
 * Destroy event sender instance
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventSource_Destroy( NGhostEventSource** );

/**
 * Get sender ip
 *
 * @param this
 * 		This instance
 *
 * @return the sender ip
 */
const char *NGhostCommon_Event_NGhostEventSource_GetIP( const NGhostEventSource* );

/**
 * Get sender device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostCommon_Event_NGhostEventSource_GetDeviceType( const NGhostEventSource* );

/**
 * Get sender device UUID
 *
 * @param this
 * 		This instance
 *
 * @return the device UUID
 */
const char *NGhostCommon_Event_NGhostEventSource_GetDeviceUUID( const NGhostEventSource* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventSource_BuildParserOutputList( const NGhostEventSource*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventSource_ProcessRESTGETRequest( const NGhostEventSource*,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList );

#endif // !NGHOSTCOMMON_EVENT_NGHOSTEVENTSOURCE_PROTECT
