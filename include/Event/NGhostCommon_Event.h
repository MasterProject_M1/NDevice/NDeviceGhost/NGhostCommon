#ifndef NGHOSTCOMMON_EVENT_PROTECT
#define NGHOSTCOMMON_EVENT_PROTECT

// -----------------------------
// namespace NGhostCommon::Event
// -----------------------------

// struct NGhostCommon::Event::NGhostEventDestination
#include "NGhostCommon_Event_NGhostEventDestination.h"

// struct NGhostCommon::Event::NGhostEventSource
#include "NGhostCommon_Event_NGhostEventSource.h"

// struct NGhostCommon::Event::NGhostEvent
#include "NGhostCommon_Event_NGhostEvent.h"

// struct NGhostCommon::Event::NGhostEventList
#include "NGhostCommon_Event_NGhostEventList.h"

// Time frame for event hash calculation (ms)
#define NGHOST_COMMON_EVENT_EVENT_HASH_TIME_WINDOW			500

#endif // !NGHOSTCOMMON_EVENT_PROTECT
