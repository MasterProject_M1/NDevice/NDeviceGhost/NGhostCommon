#ifndef NGHOSTCOMMON_EVENT_NGHOSTEVENTDESTINATION_PROTECT
#define NGHOSTCOMMON_EVENT_NGHOSTEVENTDESTINATION_PROTECT

// --------------------------------------------------
// struct NGhostCommon::Event::NGhostEventDestination
// --------------------------------------------------

typedef struct NGhostEventDestination
{
	// Host
	char *m_host;

	// Port
	NU32 m_port;

	// Request path
	char *m_requestPath;
} NGhostEventDestination;

/**
 * Build destination
 *
 * @param host
 * 		The destination host
 * @param port
 * 		The destination port
 * @param requestPath
 * 		The request path
 *
 * @return the built instance
 */
__ALLOC NGhostEventDestination *NGhostCommon_Event_NGhostEventDestination_Build( const char *host,
	NU32 port,
	const char *requestPath );

/**
 * Destroy the destination
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventDestination_Destroy( NGhostEventDestination** );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventDestination_BuildParserOutputList( const NGhostEventDestination*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventDestination_ProcessRESTGETRequest( const NGhostEventDestination*,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList );

#endif // !NGHOSTCOMMON_EVENT_NGHOSTEVENTDESTINATION_PROTECT
