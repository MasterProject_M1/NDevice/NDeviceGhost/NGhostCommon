#ifndef NGHOSTCOMMON_EVENT_NGHOSTEVENTLIST_PROTECT
#define NGHOSTCOMMON_EVENT_NGHOSTEVENTLIST_PROTECT

// -------------------------------------------
// struct NGhostCommon::Event::NGhostEventList
// -------------------------------------------

typedef struct NGhostEventList
{
	// Event list
	NListe *m_event;
} NGhostEventList;

/**
 * Build event list
 *
 * @return the built instance
 */
__ALLOC NGhostEventList *NGhostCommon_Event_NGhostEventList_Build( void );

/**
 * Destroy event list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventList_Destroy( NGhostEventList** );

/**
 * Find event index from event hash
 *
 * @param this
 * 		This instance
 * @param eventHash
 * 		The base64 event hash
 *
 * @return the event index (or NERREUR)
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Event_NGhostEventList_FindEventIndexFromEventHash( const NGhostEventList*,
	const char *eventHash );

/**
 * Find event from event hash
 *
 * @param this
 * 		This instance
 * @param eventHash
 * 		The event hash
 *
 * @return the event
 */
__MUSTBEPROTECTED NGhostEvent *NGhostCommon_Event_NGhostEventList_FindEventFromEventHash( const NGhostEventList*,
	const char *eventHash );

/**
 * Add an event
 *
 * @param this
 * 		This instance
 * @param event
 * 		The event to add
 *
 * @return if the operation succeed
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_AddEvent( NGhostEventList*,
	__WILLBEOWNED NGhostEvent *event );

/**
 * Remove an event from index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The event index
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_RemoveEvent( NGhostEventList*,
	NU32 index );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Event_NGhostEventList_ActivateProtection( NGhostEventList* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Event_NGhostEventList_DeactivateProtection( NGhostEventList* );

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param requestType
 * 		The request type
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 * @param client
 * 		The client
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostCommon_Event_NGhostEventList_ProcessRESTRequest( NGhostEventList*,
	const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *clientData,
	NU32 clientDataLength,
	const NClientServeur *client );

/**
 * Get event count
 *
 * @param this
 * 		This instance
 *
 * @return the event count
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Event_NGhostEventList_GetEventCount( const NGhostEventList* );

/**
 * Get event from index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The event index
 *
 * @return the event
 */
__MUSTBEPROTECTED const NGhostEvent *NGhostCommon_Event_NGhostEventList_GetEvent( const NGhostEventList*,
	NU32 index );

#endif // !NGHOSTCOMMON_EVENT_NGHOSTEVENTLIST_PROTECT
