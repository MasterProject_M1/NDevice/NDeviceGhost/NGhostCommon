#ifndef NGHOSTCOMMON_EVENT_NGHOSTEVENT_PROTECT
#define NGHOSTCOMMON_EVENT_NGHOSTEVENT_PROTECT

// ---------------------------------------
// struct NGhostCommon::Event::NGhostEvent
// ---------------------------------------

typedef struct NGhostEvent
{
	// Hash for the event (built from timestamp (and time frame), and action hash)
	NU8 m_hash[ 16 ];
	char *m_eventHash;

	// Timestamp
	NU64 m_timestamp;
	NU64 m_addTick;

	// Delay before activation (ms)
	NU32 m_delayBeforeActivation;

	// Source info
	NGhostEventSource *m_source;

	// Action destination
	NGhostEventDestination *m_destination;

	// Hash action
	char *m_actionHash;

	// Parameters
	NParserOutputList *m_parameterList;
} NGhostEvent;

/**
 * Build event
 * When an event is built, its hash is based on
 *
 * action hash
 * AND
 * ticks - ( ticks % NGHOST_COMMON_EVENT_EVENT_HASH_TIME_WINDOW )
 *
 * @param senderIP
 * 		The sender IP
 * @param senderDeviceType
 * 		The sender type (or NDEVICE_TYPES if unknown)
 * @param senderDeviceUUID
 * 		The sender device (or NULL if unknown)
 * @param destinationHostname
 * 		The destination hostname
 * @param destinationPort
 * 		The destination port
 * @param actionHash
 * 		The action hash
 * @param userData
 * 		The user data, from where parameters will be extracted
 * @param delayBeforeActivation
 * 		The delay before activation
 *
 * @return the built instance
 */
__ALLOC NGhostEvent *NGhostCommon_Event_NGhostEvent_Build( const char *senderIP,
	NDeviceType senderDeviceType,
	const char *senderDeviceUUID,
	const char *destinationHostname,
	NU32 destinationPort,
	const char *actionHash,
	const NParserOutputList *userData,
	NU32 delayBeforeActivation );

/**
 * Build action from user data
 *
 * @param userData
 * 		The user data
 * @param senderIP
 * 		The sender IP
 *
 * @return the built instance
 */
__ALLOC NGhostEvent *NGhostCommon_Event_NGhostEvent_Build2( const NParserOutputList *parserOutputList,
	const char *senderIP );

/**
 * Destroy event
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEvent_Destroy( NGhostEvent** );

/**
 * Get event hash
 *
 * @param this
 * 		This instance
 *
 * @return the event hash
 */
const char *NGhostCommon_Event_NGhostEvent_GetEventHash( const NGhostEvent* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEvent_BuildParserOutputList( const NGhostEvent*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEvent_ProcessRESTGETRequest( const NGhostEvent*,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList );

#endif // !NGHOSTCOMMON_EVENT_NGHOSTEVENT_PROTECT
