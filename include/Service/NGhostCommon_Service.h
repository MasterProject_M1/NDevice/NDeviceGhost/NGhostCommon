#ifndef NGHOSTCOMMON_SERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_PROTECT

// -------------------------------
// namespace NGhostCommon::Service
// -------------------------------

// Event/action parameters key name
#define NGHOST_COMMON_SERVICE_ACTION_AND_EVENT_PARAMETER_KEY_NAME		"parameter"

// enum NGhostCommon::Service::NGhostCommonService
#include "NGhostCommon_Service_NGhostCommonService.h"

// namespace NGhostCommon::Service::Info
#include "Info/NGhostCommon_Service_Info.h"

// namespace NGhostCommon::Service::Status
#include "Status/NGhostCommon_Service_Status.h"

// namespace NGhostCommon::Service::Token
#include "Token/NGhostCommon_Service_Token.h"

// namespace NGhostCommon::Service::Action
#include "Action/NGhostCommon_Service_Action.h"

// namespace NGhostCommon::Service::Event
#include "Event/NGhostCommon_Service_Event.h"

/**
 * Check HTTP authentication
 *
 * @param username
 * 		The username set in configuration
 * @param password
 * 		The password set in configuration
 * @param tokenManager
 * 		The token manager
 * @param request
 * 		The http request
 *
 * @return if the authentication succeeded
 */
NBOOL NGhostCommon_Service_CheckHTTPAuthentication( const char *username,
	const char *password,
	const NGhostTokenManager *tokenManager,
	const NRequeteHTTP *request );

/**
 * Correct element to be processed
 *
 * @param element
 * 		The requested element
 * @param service
 * 		The api service
 *
 * @return the corrected element to be processed
 */
__ALLOC char *NGhostCommon_Service_CorrectElementToBeProcessed( const char *element,
	const char *service );

/**
 * Create unauthorized response
 *
 * @param clientID
 * 		The client id
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_CreateUnauthorizedResponse( NU32 clientID );

/**
 * Create unauthorized response
 *
 * @param clientID
 * 		The client id
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_CreateUnauthorizedResponse( NU32 clientID );

/**
 * Create internal error response
 *
 * @param clientID
 * 		The client ID
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_CreateInternalErrorResponse( NU32 clientID );

/**
 * Process basic services
 *
 * @param request
 * 		The http request
 * @param ghostCommon
 * 		The ghost common
 * @param isAuthenticationCorrect
 * 		Return NTRUE if the authentication is valid
 * @param serviceType
 * 		The service type
 * @param cursor
 * 		The cursor in requested element
 * @param client
 * 		The requesting client
 * @param contactingIP
 * 		The contacting IP
 *
 * @return the http response if correct common request, NULL else
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_ProcessBasicService( const NRequeteHTTP *request,
	void *ghostCommon,
	__OUTPUT NBOOL *isAuthenticationCorrect,
	__OUTPUT NGhostCommonService *serviceType,
	NU32 *cursor,
	const NClientServeur *client,
	const char *contactingIP );

#endif // !NGHOSTCOMMON_SERVICE_PROTECT
