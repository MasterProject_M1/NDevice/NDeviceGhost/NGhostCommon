#ifndef NGHOSTCOMMON_SERVICE_INFO_PROTECT
#define NGHOSTCOMMON_SERVICE_INFO_PROTECT

// -------------------------------------
// namespace NGhostCommon::Service::Info
// -------------------------------------

// struct NGhostCommon::Service::Info::NGhostInfo
#include "NGhostCommon_Service_Info_NGhostInfo.h"

#define NGHOST_CLIENT_SERVICE_INFO_MINOR_VERSION				"device.firmware.minorVersion"
#define NGHOST_CLIENT_SERVICE_INFO_MAJOR_VERSION				"device.firmware.majorVersion"
#define NGHOST_CLIENT_SERVICE_INFO_DEVICE_NAME					NDEVICE_COMMON_TYPE_GHOST_DEVICE_NAME
#define NGHOST_CLIENT_SERVICE_INFO_DEVICE_TYPE					NDEVICE_COMMON_TYPE_GHOST_DEVICE_TYPE
#define NGHOST_CLIENT_SERVICE_INFO_BUILD_INFO					"device.firmware.buildInfo"
#define NGHOST_CLIENT_SERVICE_INFO_DEVICE_UNIQUE_ID				NDEVICE_COMMON_TYPE_GHOST_UUID_KEY
#define NGHOST_CLIENT_SERVICE_INFO_CURRENT_FORMATTED_TIME		"currentFormattedTime"
#define NGHOST_CLIENT_SERVICE_INFO_CURRENT_TIMESTAMP			"currentTime"
#define NGHOST_CLIENT_SERVICE_INFO_REQUESTING_IP				NDEVICE_COMMON_TYPE_GHOST_YOUR_IP_KEY

/**
 * Build parser output list informations
 *
 * @param ghostInfo
 * 		The ghost info details
 * @param parserOutputList
 * 		The parser output
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the basic parser output list informations
 */
NBOOL NGhostCommon_Service_Info_BuildParserOutputListInformation( const NGhostInfo *ghostInfo,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestingIP );

/**
 * Create device info HTTP response
 *
 * @param responseCode
 * 		The http response code
 * @param ghostInfo
 * 		The ghost info details
 * @param requestingIP
 * 		The requesting IP
 * @param clientID
 * 		The client ID
 *
 * @return the basic informations on device
 */
NReponseHTTP *NGhostCommon_Service_Info_CreateDeviceInfo( NHTTPCode responseCode,
	const NGhostInfo *ghostInfo,
	const char *requestingIP,
	NU32 clientID );

#endif // !NGHOSTCOMMON_SERVICE_INFO_PROTECT

