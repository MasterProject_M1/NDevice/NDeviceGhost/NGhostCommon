#ifndef NGHOSTCOMMON_SERVICE_INFO_NGHOSTINFO_PROTECT
#define NGHOSTCOMMON_SERVICE_INFO_NGHOSTINFO_PROTECT

// ----------------------------------------------
// struct NGhostCommon::Service::Info::NGhostInfo
// ----------------------------------------------

typedef struct NGhostInfo
{
	// Device name
	char *m_deviceName;

	// Authorized login details
	char *m_authorizedUsername;
	char *m_authorizedPassword;

	// Version
	NU32 m_majorVersion;
	NU32 m_minorVersion;

	// Build string
	char m_buildDetail[ 32 ];

	// Device type
	NDeviceType m_deviceType;

	// Device UUID
	char *m_deviceUUID;
} NGhostInfo;

/**
 * Build device info
 *
 * @param deviceName
 * 		The device name
 * @param authorizedUsername
 * 		The authorized username
 * @param authorizedPassword
 * 		The authorized password
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 *		The minor version
 * @param deviceType
 * 		The device type
 *
 * @return the device info instance
 */
__ALLOC NGhostInfo *NGhostCommon_Service_Info_NGhostInfo_Build( const char *deviceName,
	const char *authorizedUsername,
	const char *authorizedPassword,
	NU32 majorVersion,
	NU32 minorVersion,
	NDeviceType deviceType );

/**
 * Destroy device info
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Info_NGhostInfo_Destroy( NGhostInfo** );

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetDeviceName( const NGhostInfo* );

/**
 * Get major version
 *
 * @param this
 * 		This instance
 *
 * @return the major version
 */
NU32 NGhostCommon_Service_Info_NGhostInfo_GetMajorVersion( const NGhostInfo* );

/**
 * Get minor version
 *
 * @param this
 * 		This instance
 *
 * @return the minor version
 */
NU32 NGhostCommon_Service_Info_NGhostInfo_GetMinorVersion( const NGhostInfo* );

/**
 * Get build string
 *
 * @param this
 * 		This instance
 *
 * @return the build string
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetBuildString( const NGhostInfo* );

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostCommon_Service_Info_NGhostInfo_GetDeviceType( const NGhostInfo* );

/**
 * Get device UUID
 *
 * @param this
 * 		This instance
 *
 * @return the device uuid
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( const NGhostInfo* );

/**
 * Get authorized username
 *
 * @param this
 * 		This instance
 *
 * @return the authorized username
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedUsername( const NGhostInfo* );

/**
 * Get authorized password
 *
 * @param this
 * 		This instance
 *
 * @return the authorized password
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedPassword( const NGhostInfo* );

#endif // !NGHOSTCOMMON_SERVICE_INFO_NGHOSTINFO_PROTECT
