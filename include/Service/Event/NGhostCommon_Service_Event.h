#ifndef NGHOSTCOMMON_SERVICE_EVENT_PROTECT
#define NGHOSTCOMMON_SERVICE_EVENT_PROTECT

// --------------------------------------
// namespace NGhostCommon::Service::Event
// --------------------------------------

// enum NGhostCommon::Service::Event::NGhostEventDestinationService
#include "NGhostCommon_Service_Event_NGhostEventDestinationService.h"

// enum NGhostCommon::Service::Event::NGhostEventSourceService
#include "NGhostCommon_Service_Event_NGhostEventSourceService.h"

// enum NGhostCommon::Service::Event::NGhostEventService
#include "NGhostCommon_Service_Event_NGhostEventService.h"

// enum NGhostCommon::Service::Event::NGhostEventListService
#include "NGhostCommon_Service_Event_NGhostEventListService.h"

#endif // !NGHOSTCOMMON_SERVICE_EVENT_PROTECT
