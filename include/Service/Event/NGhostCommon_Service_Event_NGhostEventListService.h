#ifndef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_PROTECT

// ---------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventListService
// ---------------------------------------------------------

typedef enum NGhostEventListService
{
	NGHOST_EVENT_LIST_SERVICE_ROOT,

	NGHOST_EVENT_LIST_SERVICE_COUNT,
	NGHOST_EVENT_LIST_SERVICE_EVENT,

	NGHOST_EVENT_LIST_SERVICES
} NGhostEventListService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param eventList
 * 		The event list (must be protected)
 * @param eventIndex
 * 		The event index (output)
 *
 * @return the service type or NGHOST_EVENT_LIST_SERVICES
 */
NGhostEventListService NGhostCommon_Service_Event_NGhostEventListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED NGhostEventList *eventList,
	__OUTPUT NU32 *eventIndex );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventListService_GetName( NGhostEventListService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_INTERNE
__PRIVATE const char NGhostEventListServiceName[ NGHOST_EVENT_LIST_SERVICES ][ 32 ] =
{
	"",

	"count",
	"event"
};
#endif // NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_PROTECT
