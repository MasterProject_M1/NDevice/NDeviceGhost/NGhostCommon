#ifndef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_PROTECT

// -----------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventSourceService
// -----------------------------------------------------------

typedef enum NGhostEventSourceService
{
	NGHOST_EVENT_SOURCE_SERVICE_ROOT,

	NGHOST_EVENT_SOURCE_SERVICE_IP,
	NGHOST_EVENT_SOURCE_SERVICE_DEVICE_TYPE,
	NGHOST_EVENT_SOURCE_SERVICE_DEVICE_UUID,

	NGHOST_EVENT_SOURCE_SERVICES
} NGhostEventSourceService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventSourceService NGhostCommon_Service_Event_NGhostEventSourceService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventSourceService_GetName( NGhostEventSourceService serviceType );

/**
 * Get service full name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full service name
 */
const char *NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGhostEventSourceService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_INTERNE
__PRIVATE const char NGhostEventSourceServiceName[ NGHOST_EVENT_SOURCE_SERVICES ][ 32 ] =
{
	"",

	"ip",
	"type",
	"uuid"
};

__PRIVATE const char NGhostEventSourceServiceFullName[ NGHOST_EVENT_SOURCE_SERVICES ][ 64 ] =
{
	"",

	"source.ip",
	"source.type",
	"source.uuid"
};
#endif // NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_PROTECT
