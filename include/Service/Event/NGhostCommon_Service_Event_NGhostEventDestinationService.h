#ifndef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_PROTECT

// ----------------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventDestinationService
// ----------------------------------------------------------------

typedef enum NGhostEventDestinationService
{
	NGHOST_EVENT_DESTINATION_SERVICE_ROOT,

	NGHOST_EVENT_DESTINATION_SERVICE_HOSTNAME,
	NGHOST_EVENT_DESTINATION_SERVICE_PORT,
	NGHOST_EVENT_DESTINATION_SERVICE_REQUEST_PATH,

	NGHOST_EVENT_DESTINATION_SERVICES
} NGhostEventDestinationService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventDestinationService NGhostCommon_Service_Event_NGhostEventDestinationService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventDestinationService_GetName( NGhostEventDestinationService serviceType );

/**
 * Get full service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full service name
 */
const char *NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGhostEventDestinationService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_INTERNE
__PRIVATE const char NGhostEventDestinationServiceName[ NGHOST_EVENT_DESTINATION_SERVICES ][ 32 ] =
{
	"",

	"hostname",
	"port",
	"requestPath"
};

__PRIVATE const char NGhostEventDestinationServiceFullName[ NGHOST_EVENT_DESTINATION_SERVICES ][ 64 ] =
{
	"",

	"destination.hostname",
	"destination.port",
	"destination.requestPath"
};
#endif // NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_PROTECT
