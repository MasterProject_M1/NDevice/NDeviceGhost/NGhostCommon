#ifndef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_PROTECT

// -----------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventService
// -----------------------------------------------------

typedef enum NGhostEventService
{
	NGHOST_EVENT_SERVICE_ROOT,

	NGHOST_EVENT_SERVICE_EVENT_HASH,
	NGHOST_EVENT_SERVICE_EVENT_TIMESTAMP,
	NGHOST_EVENT_SERVICE_EVENT_DELAY_BEFORE_ACTIVATION,
	NGHOST_EVENT_SERVICE_EVENT_IS_READY,
	NGHOST_EVENT_SERVICE_ACTION_HASH,
	NGHOST_EVENT_SERVICE_PARAMETER_LIST,
	NGHOST_EVENT_SERVICE_DESTINATION,
	NGHOST_EVENT_SERVICE_SOURCE,

	NGHOST_EVENT_SERVICES
} NGhostEventService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventService NGhostCommon_Service_Event_NGhostEventService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventService_GetName( NGhostEventService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_INTERNE
__PRIVATE const char NGhostEventServiceName[ NGHOST_EVENT_SERVICES ][ 32 ] =
{
	"",

	"eventHash",
	"timestamp",
	"delayBeforeActivation",
	"isReady",
	"actionHash",
	NGHOST_COMMON_SERVICE_ACTION_AND_EVENT_PARAMETER_KEY_NAME,
	"destination",
	"source"
};
#endif // NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_PROTECT
