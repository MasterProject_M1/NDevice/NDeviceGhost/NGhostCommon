#ifndef NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_PROTECT

// -------------------------------------------------------
// enum NGhostCommon::Service::Action::NGhostActionService
// -------------------------------------------------------

typedef enum NGhostActionService
{
	NGHOST_ACTION_SERVICE_ROOT,

	NGHOST_ACTION_SERVICE_HASH,
	NGHOST_ACTION_SERVICE_TYPE,
	NGHOST_ACTION_SERVICE_SENTENCE,
	NGHOST_ACTION_SERVICE_HOSTNAME,
	NGHOST_ACTION_SERVICE_PORT,
	NGHOST_ACTION_SERVICE_REQUEST_URL,
	NGHOST_ACTION_SERVICE_FULL_REQUEST_URL,
	NGHOST_ACTION_SERVICE_REQUEST_TYPE,
	NGHOST_ACTION_SERVICE_JSON,
	NGHOST_ACTION_SERVICE_JSON_PARAMETER,

	NGHOST_ACTION_SERVICES
} NGhostActionService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostActionService NGhostCommon_Service_Action_NGhostActionService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Action_NGhostActionService_GetName( NGhostActionService serviceType );

/**
 * Get service type
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service type
 */
NParserOutputType NGhostCommon_Service_Action_NGhostActionService_GetServiceType( NGhostActionService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_INTERNE
__PRIVATE const char NGhostActionServiceName[ NGHOST_ACTION_SERVICES ][ 32 ] =
{
	"",

	"hash",
	"type",
	"sentence",
	"hostname",
	"port",
	"requestURL",
	"fullRequestURL",
	"requestType",
	"json",
	NGHOST_COMMON_SERVICE_ACTION_AND_EVENT_PARAMETER_KEY_NAME
};

__PRIVATE const NParserOutputType NGhostActionServiceType[ NGHOST_ACTION_SERVICES ] =
{
	NPARSER_OUTPUT_TYPE_STRING,

	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING
};
#endif // NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_PROTECT
