#ifndef NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_PROTECT

// -----------------------------------------------------------
// enum NGhostCommon::Service::Action::NGhostActionListService
// -----------------------------------------------------------

typedef enum NGhostActionListService
{
	NGHOST_ACTION_LIST_SERVICE_ROOT,

	NGHOST_ACTION_LIST_SERVICE_COUNT,
	NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY,

	NGHOST_ACTION_LIST_SERVICES
} NGhostActionListService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param actionList
 * 		The action list
 * @param actionIndex
 * 		The entry index (output)
 *
 * @return the service
 */
NGhostActionListService NGhostCommon_Service_Action_NGhostActionListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NGhostActionList *actionList,
	__OUTPUT NU32 *actionIndex );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Action_NGhostActionListService_GetName( NGhostActionListService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_INTERNE
__PRIVATE const char NGhostActionListServiceName[ NGHOST_ACTION_LIST_SERVICES ][ 32 ] =
{
	"",

	"count",
	""
};
#endif // NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_PROTECT
