#ifndef NGHOSTCOMMON_SERVICE_ACTION_PROTECT
#define NGHOSTCOMMON_SERVICE_ACTION_PROTECT

// ---------------------------------------
// namespace NGhostCommon::Service::Action
// ---------------------------------------

// enum NGhostCommon::Service::Action::NGhostActionListService
#include "NGhostCommon_Service_Action_NGhostActionListService.h"

// enum NGhostCommon::Service::Action::NGhostActionService
#include "NGhostCommon_Service_Action_NGhostActionService.h"

#endif // !NGHOSTCOMMON_SERVICE_ACTION_PROTECT
