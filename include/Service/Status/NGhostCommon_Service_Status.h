#ifndef NGHOSTCOMMON_SERVICE_STATUS_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_PROTECT

// ---------------------------------------
// namespace NGhostCommon::Service::Status
// ---------------------------------------

// enum NGhostCommon::Service::Status::NGhostStatusService
#include "NGhostCommon_Service_Status_NGhostStatusService.h"

// namespace NGhostCommon::Service::Status::Referal
#include "Referal/NGhostCommon_Service_Status_Referal.h"

// struct NGhostCommon::Service::Status::NGhostDeviceStatus
#include "NGhostCommon_Service_Status_NGhostDeviceStatus.h"

#endif // !NGHOSTCOMMON_SERVICE_STATUS_PROTECT

