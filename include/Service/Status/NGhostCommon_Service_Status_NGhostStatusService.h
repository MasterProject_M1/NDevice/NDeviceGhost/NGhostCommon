#ifndef NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_PROTECT

// -------------------------------------------------------
// enum NGhostCommon::Service::Status::NGhostStatusService
// -------------------------------------------------------

typedef enum NGhostStatusService
{
	NGHOST_STATUS_SERVICE_ROOT,

	NGHOST_STATUS_SERVICE_UP_TIME,
	NGHOST_STATUS_SERVICE_STARTUP_TIMESTAMP,
	NGHOST_STATUS_SERVICE_TOTAL_REQUEST_COUNT,
	NGHOST_STATUS_SERVICE_REFERAL,

	NGHOST_STATUS_SERVICES
} NGhostStatusService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostStatusService NGhostCommon_Service_Status_NGhostStatusService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Status_NGhostStatusService_GetName( NGhostStatusService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_INTERNE
__PRIVATE const char NGhostStatusServiceName[ NGHOST_STATUS_SERVICES ][ 32 ] =
{
	"",

	"upTime",
	"startupTimestamp",
	"totalRequestCount",
	"referal"
};
#endif // NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_PROTECT
