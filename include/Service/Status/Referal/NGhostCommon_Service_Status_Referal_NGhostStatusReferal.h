#ifndef NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERAL_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERAL_PROTECT

// ------------------------------------------------------------------
// struct NGhostCommon::Service::Status::Referal::NGhostStatusReferal
// ------------------------------------------------------------------

typedef struct NGhostStatusReferal
{
	// Mutex
	NMutex *m_mutex;

	// Watchers list (NListe<NGhostStatusServiceReferal*>)
	NListe *m_watcher;

	// Client list (NListe<NGhostStatusServiceReferal*>)
	NListe *m_client;
} NGhostStatusReferal;

/**
 * Build ghost status referal
 *
 * @return the status referal instance
 */
__ALLOC NGhostStatusReferal *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Build( void );

/**
 * Destroy ghost status referal
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Destroy( NGhostStatusReferal** );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputList( NGhostStatusReferal*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTGETRequest( NGhostStatusReferal*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTPOSTRequest( NGhostStatusReferal*,
	const char *requestedElement,
	NU32 *cursor,
	const char *userData );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ActivateProtection( NGhostStatusReferal* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( NGhostStatusReferal* );

/**
 * Add watcher ip
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcherIP( NGhostStatusReferal*,
	const char *ip );

/**
 * Get watcher IP list
 *
 * @param this
 * 		This instance
 *
 * @return the watcher ip list
 */
__MUSTBEPROTECTED const NListe *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetWatcherIPList( const NGhostStatusReferal* );

/**
 * Get client IP list
 *
 * @param this
 * 		This instance
 *
 * @return the client ip list
 */
__MUSTBEPROTECTED const NListe *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetClientIPList( const NGhostStatusReferal* );

#endif // !NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERAL_PROTECT
