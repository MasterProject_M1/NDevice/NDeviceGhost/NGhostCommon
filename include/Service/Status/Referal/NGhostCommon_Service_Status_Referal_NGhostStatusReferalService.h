#ifndef NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_PROTECT

// -----------------------------------------------------------------------
// enum NGhostCommon::Service::Status::Referal::NGhostStatusReferalService
// -----------------------------------------------------------------------

typedef enum NGhostStatusReferalService
{
	NGHOST_STATUS_REFERAL_SERVICE_ROOT,

	NGHOST_STATUS_REFERAL_SERVICE_WATCHER,
	NGHOST_STATUS_REFERAL_SERVICE_CLIENT,

	NGHOST_STATUS_REFERAL_SERVICES
} NGhostStatusReferalService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostStatusReferalService NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_GetName( NGhostStatusReferalService serviceType );

#ifdef NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_INTERNE
__PRIVATE const char NGhostStatusReferalServiceName[ NGHOST_STATUS_REFERAL_SERVICES ][ 32 ] =
{
	"",

	"watcher",
	"client"
};
#endif // NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_INTERNE

#endif // !NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_PROTECT
