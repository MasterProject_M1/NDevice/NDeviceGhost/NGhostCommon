#ifndef NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSSERVICEREFERAL_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSSERVICEREFERAL_PROTECT

// -------------------------------------------------------------------------
// struct NGhostCommon::Service::Status::Referal::NGhostStatusServiceReferal
// -------------------------------------------------------------------------

typedef struct NGhostStatusServiceReferal
{
	// IP
	char *m_ip;

	// HTTP client
	NClientHTTP *m_httpClient;
} NGhostStatusServiceReferal;

/**
 * Build service referal
 *
 * @param ip
 * 		The service ip
 * @param port
 * 		The port
 *
 * @return the built instance
 */
__ALLOC NGhostStatusServiceReferal *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Build( const char *ip,
	NU32 port );

/**
 * Destroy service referal
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Destroy( NGhostStatusServiceReferal** );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the referal IP
 */
const char *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( const NGhostStatusServiceReferal* );

/**
 * Get http client
 *
 * @param this
 * 		This instance
 *
 * @return the http client
 */
NClientHTTP *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetHTTPClient( const NGhostStatusServiceReferal* );

#endif // !NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSSERVICEREFERAL_PROTECT
