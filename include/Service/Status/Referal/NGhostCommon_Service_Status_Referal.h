#ifndef NGHOSTCOMMON_SERVICE_STATUS_REFERAL_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_REFERAL_PROTECT

// ------------------------------------------------
// namespace NGhostCommon::Service::Status::Referal
// ------------------------------------------------

// enum NGhostCommon::Service::Status::Referal::NGhostStatusReferalService
#include "NGhostCommon_Service_Status_Referal_NGhostStatusReferalService.h"

// struct NGhostCommon::Service::Status::Referal::NGhostStatusWatcherReferal
#include "NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal.h"

// struct NGhostCommon::Service::Status::Referal::NGhostStatusReferal
#include "NGhostCommon_Service_Status_Referal_NGhostStatusReferal.h"

#endif // !NGHOSTCOMMON_SERVICE_STATUS_REFERAL_PROTECT
