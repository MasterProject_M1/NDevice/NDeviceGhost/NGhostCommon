#ifndef NGHOSTCOMMON_SERVICE_STATUS_NGHOSTDEVICESTATUS_PROTECT
#define NGHOSTCOMMON_SERVICE_STATUS_NGHOSTDEVICESTATUS_PROTECT

// --------------------------------------------------------
// struct NGhostCommon::Service::Status::NGhostDeviceStatus
// --------------------------------------------------------

typedef struct NGhostDeviceStatus
{
	// Start timestamp
	NU64 m_startTimestamp;

	// Processed request count
	NU32 m_processedRequestCount;

	// Referal
	NGhostStatusReferal *m_referal;
} NGhostDeviceStatus;

/**
 * Build client status
 *
 * @return the client status instance
 */
__ALLOC NGhostDeviceStatus *NGhostCommon_Service_Status_NGhostDeviceStatus_Build( void );

/**
 * Destroy client status
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_NGhostDeviceStatus_Destroy( NGhostDeviceStatus** );

/**
 * Get the up time
 *
 * @param this
 * 		This instance
 *
 * @return the uptime
 */
NU64 NGhostCommon_Service_Status_NGhostDeviceStatus_GetUpTime( const NGhostDeviceStatus* );

/**
 * Get the starting time
 *
 * @param this
 * 		This instance
 *
 * @return the starting time
 */
NU64 NGhostCommon_Service_Status_NGhostDeviceStatus_GetStartingTime( const NGhostDeviceStatus* );

/**
 * Get processed requests count
 *
 * @param this
 * 		This instance
 *
 * @return the processed requests count
 */
NU32 NGhostCommon_Service_Status_NGhostDeviceStatus_GetProcessedRequestCount( const NGhostDeviceStatus* );

/**
 * Get ghost status referal
 *
 * @param this
 * 		This instance
 *
 * @return the ghost status referal instance
 */
NGhostStatusReferal *NGhostCommon_Service_Status_NGhostDeviceStatus_GetGhostStatusReferal( const NGhostDeviceStatus* );

/**
 * Increase processed requests count
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_NGhostDeviceStatus_IncreaseProcessRequestCount( NGhostDeviceStatus* );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTGETRequest( const NGhostDeviceStatus*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The raw user data
 * @param clientID
 * 		The client id
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTPOSTRequest( NGhostDeviceStatus*,
	const char *requestedElement,
	NU32 *cursor,
	const char *userData,
	NU32 clientID );

/**
 * Add watcher IP
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip to add
 *
 * @return the watcher IP
 */
NBOOL NGhostCommon_Service_Status_NGhostDeviceStatus_AddWatcherIP( NGhostDeviceStatus*,
	const char *ip );

#endif // !NGHOSTCOMMON_SERVICE_STATUS_NGHOSTDEVICESTATUS_PROTECT

