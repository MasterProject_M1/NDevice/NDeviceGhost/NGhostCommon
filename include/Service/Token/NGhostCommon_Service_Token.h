#ifndef NGHOSTCOMMON_SERVICE_TOKEN_PROTECT
#define NGHOSTCOMMON_SERVICE_TOKEN_PROTECT

// --------------------------------------
// namespace NGhostCommon::Service::Token
// --------------------------------------

// Json token key
#define NGHOSTCOMMON_SERVICE_TOKEN_KEY		"token"

/**
 * Process token generation request
 *
 * @param tokenManager
 * 		The token manager
 * @param client
 * 		The requesting client
 * @param isAuthorized
 * 		Is authorized to get a token directly?
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Token_ProcessTokenGenerationRequest( NGhostTokenManager *tokenManager,
	const NClientServeur *client,
	NBOOL isAuthorized );

#endif // !NGHOSTCOMMON_SERVICE_TOKEN_PROTECT
