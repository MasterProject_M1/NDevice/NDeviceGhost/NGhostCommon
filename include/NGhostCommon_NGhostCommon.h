#ifndef NGHOSTCOMMON_NGHOSTCOMMON_PROTECT
#define NGHOSTCOMMON_NGHOSTCOMMON_PROTECT

// ---------------------------------
// struct NGhostCommon::NGhostCommon
// ---------------------------------

typedef struct NGhostCommon
{
	// HTTP server
	NHTTPServeur *m_server;

	// Status
	NGhostDeviceStatus *m_status;

	// Info
	NGhostInfo *m_info;

	/* Authentication */
	NAuthenticationManager *m_authentication;
	NGhostTokenManager *m_tokenManager;

	/* Action list */
	NGhostActionList *m_action;

	/* Device Type */
	NDeviceType m_deviceType;

	/* My IP */
	char *m_ip;
} NGhostCommon;

/**
 * Build common ghost basis
 *
 * @param listeningInterface
 * 		The listening interface (NULL for all interfaces)
 * @param listeningPort
 * 		The listening port
 * @param callbackPacketReceive
 * 		The receive callback
 * @param clientName
 * 		The client name
 * @param clientInstance
 * 		The client instance
 * @param deviceType
 * 		The device type
 * @param isAuthenticationManagerDataPersistent
 * 		Is authentication manager data persistent?
 * @param deviceName
 * 		The device name
 * @param authorizedUsername
 * 		The authorized username
 * @param authorizedPassword
 * 		The authorized password
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 * 		The minor version
 * @param trustedTokenIPList
 * 		The trusted token ip list
 *
 * @return the basis instance
 */
__ALLOC NGhostCommon *NGhostCommon_NGhostCommon_Build( const char *listeningInterface,
	NU32 listeningPort,
	__ALLOC NReponseHTTP *( __CALLBACK *callbackPacketReceive )( const NRequeteHTTP*,
	const NClientServeur* ),
	const char *clientName,
	void *clientInstance,
	NDeviceType deviceType,
	NBOOL isAuthenticationManagerDataPersistent,
	const char *deviceName,
	const char *authorizedUsername,
	const char *authorizedPassword,
	NU32 majorVersion,
	NU32 minorVersion,
	const NGhostTrustedTokenIPList *trustedTokenIPList );

/**
 * Destroy basis instance
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_NGhostCommon_Destroy( NGhostCommon** );

/**
 * Increase request count
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_NGhostCommon_IncreaseRequestCount( NGhostCommon* );

/**
 * Get device informations
 *
 * @param this
 * 		This instance
 *
 * @return the ghost informations
 */
const NGhostInfo *NGhostCommon_NGhostCommon_GetDeviceInfo( const NGhostCommon* );

/**
 * Get device status
 *
 * @param this
 * 		This instance
 *
 * @return the device status
 */
const NGhostDeviceStatus *NGhostCommon_NGhostCommon_GetDeviceStatus( const NGhostCommon* );

/**
 * Get authentication manager
 *
 * @param this
 * 		This instance
 *
 * @return the authentication manager
 */
NAuthenticationManager *NGhostCommon_NGhostCommon_GetAuthenticationManager( NGhostCommon* );

/**
 * Get token manager
 *
 * @param this
 * 		This instance
 *
 * @return the token manager
 */
NGhostTokenManager *NGhostCommon_NGhostCommon_GetTokenManager( NGhostCommon* );

/**
 * Get action list
 *
 * @param this
 * 		This instance
 *
 * @return the action list
 */
NGhostActionList *NGhostCommon_NGhostCommon_GetActionList( NGhostCommon* );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostCommon_NGhostCommon_GetIP( const NGhostCommon* );

/**
 * Add watcher
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip device
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_NGhostCommon_AddWatcherIP( NGhostCommon*,
	const char *ip );

#endif // !NGHOSTCOMMON_NGHOSTCOMMON_PROTECT
