#ifndef NGHOSTCOMMON_MONITORING_PROTECT
#define NGHOSTCOMMON_MONITORING_PROTECT

// ----------------------------------
// namespace NGhostCommon::Monitoring
// ----------------------------------

// enum NGhostCommon::Monitoring::NMonitoringCommand
#include "NGhostCommon_Monitoring_NMonitoringCommand.h"

// struct NGhostCommon::Monitoring::NMonitoringData
#include "NGhostCommon_Monitoring_NMonitoringAgent.h"

/**
 * Monitoring thread
 *
 * @param monitoringAgent
 * 		The monitoring data
 *
 * @return NTRUE
 */
__THREAD NBOOL NGhostCommon_Monitoring_MonitoringThread( NMonitoringAgent *monitoringAgent );

#endif // !NGHOSTCOMMON_MONITORING_PROTECT
