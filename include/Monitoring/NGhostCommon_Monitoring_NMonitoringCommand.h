#ifndef NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_PROTECT
#define NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_PROTECT

// -------------------------------------------------
// enum NGhostCommon::Monitoring::NMonitoringCommand
// -------------------------------------------------

typedef enum NMonitoringCommand
{
	NMONITORING_COMMAND_QUIT,
	NMONITORING_COMMAND_HELP,

	NMONITORING_COMMAND_ALLOW_TOKEN_GENERATION,

	NMONITORING_COMMANDS
} NMonitoringCommand;

/**
 * Parse command
 *
 * @param command
 * 		The command to parse
 *
 * @return the command type
 */
NMonitoringCommand NGhostCommon_Monitoring_NMonitoringCommand_Parse( const char *command );

/**
 * Get command
 *
 * @param command
 * 		The command
 *
 * @return the command
 */
const char *NGhostCommon_Monitoring_NMonitoringCommand_GetCommand( NMonitoringCommand command );

/**
 * Get command description
 *
 * @param command
 * 		The command
 *
 * @return the command description
 */
const char *NGhostCommon_Monitoring_NMonitoringCommand_GetDescription( NMonitoringCommand command );

#ifdef NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_INTERNE
static const char NMonitoringCommandText[ NMONITORING_COMMANDS ][ 32 ] =
{
	"Quit",
	"Help",

	"AllowToken"
};

static const char NMonitoringCommandDescription[ NMONITORING_COMMANDS ][ 64 ] =
{
	"Quit the program",
	"Display this help",

	"Allow token generation when requested"
};
#endif // NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_INTERNE

#endif // !NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_PROTECT
