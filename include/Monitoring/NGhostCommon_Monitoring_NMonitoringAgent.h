#ifndef NGHOSTCOMMON_MONITORING_NMONITORINGAGENT_PROTECT
#define NGHOSTCOMMON_MONITORING_NMONITORINGAGENT_PROTECT

// -------------------------------------------------
// struct NGhostCommon::Monitoring::NMonitoringAgent
// -------------------------------------------------

typedef struct NMonitoringAgent
{
	// Token manager
	NGhostTokenManager *m_tokenManager;

	// Client to pass to callback
	void *m_clientData;

	// Is running?
	NBOOL *m_isContinue;

	// Callback to call for unknown command
	NBOOL ( ___cdecl *m_callbackUnknownCommand )( const char *command,
		void *clientData );

	// Thread
	NThread *m_thread;
} NMonitoringAgent;

/**
 * Build monitoring agent instance
 *
 * @param tokenManager
 * 		The token manager
 * @param clientData
 * 		The client data
 * @param isContinue
 * 		Is continuing?
 * @param callbackUnknownCommand
 * 		The callback to call for unknown command
 *
 * @return NTRUE
 */
__ALLOC NMonitoringAgent *NGhostCommon_Monitoring_NMonitoringAgent_Build( NGhostTokenManager *tokenManager,
	void *clientData,
	NBOOL *isContinue,
	NBOOL ( ___cdecl *callbackUnknownCommand )( const char *command,
		void *clientData ) );

/**
 * Destroy monitoring data
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_Destroy( NMonitoringAgent ** );

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if is running
 */
NBOOL NGhostCommon_Monitoring_NMonitoringAgent_IsRunning( const NMonitoringAgent * );

/**
 * Stop main loop
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_Stop( NMonitoringAgent* );

/**
 * Allow token generation
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_AllowTokenGeneration( NMonitoringAgent* );

#endif // !NGHOSTCOMMON_MONITORING_NMONITORINGAGENT_PROTECT
