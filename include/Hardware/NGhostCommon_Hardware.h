#ifndef NGHOSTCOMMON_HARDWARE_PROTECT
#define NGHOSTCOMMON_HARDWARE_PROTECT

// --------------------------------
// namespace NGhostCommon::Hardware
// --------------------------------

/**
 * Get an id
 * It will be based only on NIC's mac xor for now... (won't change on a raspberry)
 *
 * @param deviceType
 * 		The device type
 *
 * @return a unique ID base64 encoded
 */
__ALLOC char *NGhostCommon_Hardware_GetUniqueID( NDeviceType deviceType );

#endif // !NGHOSTCOMMON_HARDWARE_PROTECT
