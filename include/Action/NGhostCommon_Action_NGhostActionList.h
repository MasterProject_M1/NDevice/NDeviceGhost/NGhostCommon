#ifndef NGHOSTCOMMON_ACTION_NGHOSTACTIONLIST_PROTECT
#define NGHOSTCOMMON_ACTION_NGHOSTACTIONLIST_PROTECT

// ---------------------------------------------
// struct NGhostCommon::Action::NGhostActionList
// ---------------------------------------------

typedef struct NGhostActionList
{
	// Action list
	NListe *m_list;

	// Authentication manager
	NAuthenticationManager *m_authenticationManager;

	// Ghost referal
	void *m_ghostStatusReferal;

	// Ghost common
	void *m_ghostCommon;
} NGhostActionList;

/**
 * Build action list
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param ghostStatusReferal
 * 		The ghost status referal
 *
 * @return the action list instance
 */
__ALLOC NGhostActionList *NGhostCommon_Action_NGhostActionList_Build( NAuthenticationManager *authenticationManager,
	void *ghostStatusReferal,
	void *ghostCommon );

/**
 * Destroy action list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostActionList_Destroy( NGhostActionList** );

/**
 * Add an action
 *
 * @param this
 * 		This instance
 * @param action
 * 		The action to add
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_AddAction( NGhostActionList*,
	__WILLBEOWNED NGhostAction *action );

/**
 * Is action already present?
 *
 * @param this
 * 		This instance
 * @param action
 * 		The action to check
 *
 * @return if action already present
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_IsActionExist( const NGhostActionList*,
	const NGhostAction *action );

/**
 * Find action index from hash
 *
 * @param this
 * 		This instance
 * @param hash
 * 		The action hash (base64)
 *
 * @return the action index or NERREUR
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Action_NGhostActionList_FindActionFromBase64Hash( const NGhostActionList*,
	const char *hash );

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param requestType
 * 		The request type
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 * @param deviceContactingIP
 * 		The IP used to contact the device
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostCommon_Action_NGhostActionList_ProcessRESTRequest( NGhostActionList*,
	const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *userData,
	NU32 clientID,
	const char *deviceContactingIP );

/**
 * Get action count
 *
 * @param this
 * 		This instance
 *
 * @return the action count
 */
NU32 NGhostCommon_Action_NGhostActionList_GetCount( const NGhostActionList* );

/**
 * Share action list
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The client on which to send
 * @param authenticationEntry
 * 		The authentication entry associated with destination
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ShareActionList( const NGhostActionList*,
	NClientHTTP *httpClient,
	const NAuthenticationEntry *authenticationEntry,
	const char *contactingIP );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Action_NGhostAction_ActivateProtection( NGhostActionList* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Action_NGhostAction_DeactivateProtection( NGhostActionList* );

#endif // !NGHOSTCOMMON_ACTION_NGHOSTACTIONLIST_PROTECT
