#ifndef NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_PROTECT
#define NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_PROTECT

// -------------------------------------------
// enum NGhostCommon::Action::NGhostActionType
// -------------------------------------------

typedef enum NGhostActionType
{
	// HUE action
	NGHOST_ACTION_TYPE_HUE_LIGHT_ACTIVATE,
	NGHOST_ACTION_ACTION_TYPE_HUE_LIGHT_DEACTIVATE,
	NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_COLOR_RGB,
	NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_BRIGHTNESS,
	NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK,
	NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK_LONG,
	NGHOST_ACTION_TYPE_HUE_LIGHT_COLOR_LOOP,
	NGHOST_ACTION_TYPE_HUE_LIGHT_STOP_COLOR_LOOP,

	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_ACTIVATE,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_DEACTIVATE,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_COLOR_RGB,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_BRIGHTNESS,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK_LONG,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_COLOR_LOOP,
	NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_STOP_COLOR_LOOP,

	// Door opener action
	NGHOST_ACTION_TYPE_DOOR_OPEN,

	// Outlet action
	NGHOST_ACTION_TYPE_OUTLET_ACTIVATE,
	NGHOST_ACTION_TYPE_OUTLET_DEACTIVATE,

	// Ghost audio
	NGHOST_ACTION_TYPE_AUDIO_PLAY_SOUND,
	NGHOST_ACTION_TYPE_AUDIO_STOP_ALL_SOUND,

	NGHOST_ACTION_TYPES
} NGhostActionType;

/**
 * Get sentence associated with action
 *
 * @param actionType
 * 		The action type
 *
 * @return the sentence
 */
const char *NGhostCommon_Action_NGhostActionType_GetSentence( NGhostActionType actionType );

/**
 * Get action name
 *
 * @param actionType
 * 		The action type
 *
 * @return the name
 */
const char *NGhostCommon_Action_NGhostActionType_GetName( NGhostActionType actionType );

/**
 * Parser action type
 *
 * @param actionType
 * 		The action type
 *
 * @return the action type
 */
NGhostActionType NGhostCommon_Action_NGhostActionType_ParseActionType( const char *actionType );

#ifdef NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_INTERNE
__PRIVATE const char NGhostDeviceActionTypeName[ NGHOST_ACTION_TYPES ][ 128 ] =
{
	"Allumer lumiere",
	"Eteindre lumiere",
	"Changer couleur lumiere RGB",
	"Modifier intensite lumiere",
	"Faire clignoter lumiere",
	"Faire clignoter lumiere x15",
	"Activer arc-en-ciel lumiere",
	"Desactiver arc-en-ciel lumiere",

	"Allumer lumieres",
	"Eteindre lumieres",
	"Changer couleur lumieres RGB",
	"Modifier intensite lumieres",
	"Faire clignoter lumieres",
	"Faire clignoter lumieres x15",
	"Activer arc-en-ciel lumieres",
	"Desactiver arc-en-ciel lumieres",

	"Ouvrir porte",

	"Allumer prise",
	"Eteindre prise",

	"Lire son",
	"Arreter sons"
};

// When using #PARAMETER# syntax, please ensure your syntax is correct!
// Which means:
// #INDEX:Name:type[Possible values]#
__PRIVATE const char NGhostDeviceActionTypeSentence[ NGHOST_ACTION_TYPES ][ 256 ] =
{
	// Hue actions
	"Allume la lumiere %s",
	"Eteins la lumiere %s",
	"Change la couleur de la lumiere %s avec les valeurs #1:Rouge:int[0-255]# #2:Vert:int[0-255]# #3:Bleu:int[0-255]#",
	"Modifie l'intensite de la lumiere %s avec la valeur #1:Intensite:int[0-255]#",
	"Fais clignoter la lumiere %s",
	"Fais clignoter quinze fois la lumiere %s",
	"Active la boucle sur la lumiere %s",
	"Eteins la boucle sur la lumiere %s",

	"Allume les lumieres",
	"Eteins les lumieres",
	"Change la couleur des lumieres avec les valeurs #1:Rouge:int[0-255]# #2:Vert:int[0-255]# #3:Bleu:int[0-255]#",
	"Modifie l'intensite des lumieres avec la valeur #1:Intensite:int[0-255]#",
	"Fais clignoter les lumieres",
	"Fais clignoter quinze fois les lumieres",
	"Active la boucle sur les lumieres",
	"Eteins la boucle sur les lumieres",

	"Ouvre la porte %s",

	"Allume prise",
	"Eteins prise",

	"Lire son sur %s #1:hostname:string#:#2:port:int#/#3:filePath:string# volume=#4:volume:int[0-100]# nomme #5:name:string# (boucle:#6:isLoop:boolean#)",
	"Arreter son sur %s"
};
#endif // NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_INTERNE

#endif // !NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_PROTECT
