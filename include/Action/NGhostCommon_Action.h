#ifndef NGHOSTCOMMON_ACTION_PROTECT
#define NGHOSTCOMMON_ACTION_PROTECT

// ------------------------------
// namespace NGhostCommon::Action
// ------------------------------

// enum NGhostCommon::Action::NGhostActionType
#include "NGhostCommon_Action_NGhostActionType.h"

// struct NGhostCommon::Action::NGhostAction
#include "NGhostCommon_Action_NGhostAction.h"

// struct NGhostCommon::Action::NGhostActionList
#include "NGhostCommon_Action_NGhostActionList.h"

// struct NGhostCommon::Action::NGhostActionUpdater
#include "NGhostCommon_Action_NGhostActionUpdater.h"

#define NGHOST_COMMON_ACTION_CONNECTION_TIMEOUT			500

/**
 * Set arguments in json
 *
 * @param json
 * 		The json text
 * @param userParameterList
 * 		The user parameter list
 * @param parameterKeyName
 * 		The parameter key name
 *
 * @return the new built json
 */
__ALLOC char *NGhostCommon_Action_SetJsonArgument( const char *json,
	const NParserOutputList *userParameterList,
	const char *parameterKeyName );

#endif // !NGHOSTCOMMON_ACTION_PROTECT
