#ifndef NGHOSTCOMMON_ACTION_NGHOSTACTIONUPDATER_PROTECT
#define NGHOSTCOMMON_ACTION_NGHOSTACTIONUPDATER_PROTECT

// ------------------------------------------------
// struct NGhostCommon::Action::NGhostActionUpdater
// ------------------------------------------------

typedef struct NGhostActionUpdater
{
	// Update thread state
	NThread *m_updateThread;

	// Is update thread running?
	NBOOL m_isUpdateThreadRunning;

	// Device container
	const void *m_device;

	// Action list
	NGhostActionList *m_actionList;
} NGhostActionUpdater;

/**
 * Build action updater
 *
 * @param actionList
 * 		The action list
 * @param device
 * 		The device
 * @param updateThread
 * 		The action update thread
 *
 * @return the action updater instance
 */
__ALLOC NGhostActionUpdater *NGhostCommon_Action_NGhostActionUpdater_Build( NGhostActionList *actionList,
	const void *device,
	NBOOL ( ___cdecl *updateThread )( NGhostActionUpdater *device ) );

/**
 * Destroy action updater
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostActionUpdater_Destroy( NGhostActionUpdater** );

/**
 * Get action list
 *
 * @param this
 * 		This instance
 *
 * @return the action list
 */
NGhostActionList *NGhostCommon_Action_NGhostActionUpdater_GetActionList( NGhostActionUpdater* );

/**
 * Get device
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
const void *NGhostCommon_Action_NGhostActionUpdater_GetDevice( NGhostActionUpdater* );

/**
 * Is update thread running
 *
 * @param this
 * 		This instance
 *
 * @return if the update thread is running
 */
NBOOL NGhostCommon_Action_NGhostActionUpdater_IsUpdateThreadRunning( const NGhostActionUpdater* );

#endif // !NGHOSTCOMMON_ACTION_NGHOSTACTIONUPDATER_PROTECT
