#ifndef NGHOSTCOMMON_ACTION_NGHOSTACTION_PROTECT
#define NGHOSTCOMMON_ACTION_NGHOSTACTION_PROTECT

// -----------------------------------------
// struct NGhostCommon::Action::NGhostAction
// -----------------------------------------

typedef struct NGhostAction
{
	// Action sentence
	char *m_sentence;

	// Hash (built from requested element, sentence, and device)
	unsigned char m_hash[ 16 ];

	// Base64 hash
	char *m_base64Hash;

	// Action type
	NGhostActionType m_type;

	// Destination hostname
	char *m_destinationHostname;

	// Destination port
	NU32 m_destinationPort;

	// Requested element
	char *m_requestedElement;

	// Request data
	char *m_requestData;

	// Request method
	NTypeRequeteHTTP m_requestType;

	// HTTP client for action execution
	NClientHTTP *m_httpClient;
} NGhostAction;

/**
 * Build action
 *
 * @param destinationHostname
 * 		The destination hostname
 * @param destinationPort
 * 		The destination port
 * @param requestedElement
 * 		The element to be requested
 * @param httpRequestType
 * 		The http request type
 * @param parserOutputList
 * 		The parser output list (json data)
 * @param actionType
 * 		The action type
 * @param sentence
 * 		The sentence associated with the action
 * @param deviceUUID
 * 		The device UUID
 *
 * @return the built action
 */
__ALLOC NGhostAction *NGhostCommon_Action_NGhostAction_Build( const char *destinationHostname,
	NU32 destinationPort,
	const char *requestedElement,
	NTypeRequeteHTTP httpRequestType,
	__WILLBEOWNED NParserOutputList *parserOutputList,
	NGhostActionType actionType,
	const char *sentence,
	const char *deviceUUID );

/**
 * Build an action from action data
 *
 * @param actionData
 * 		The action data
 * @param keyRoot
 * 		The key root
 * @param deviceUUID
 * 		The device UUID
 *
 * @return the build instance
 */
__ALLOC NGhostAction *NGhostCommon_Action_NGhostAction_Build2( const NParserOutputList *actionData,
	const char *keyRoot,
	const char *deviceUUID );

/**
 * Destroy action
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostAction_Destroy( NGhostAction** );

/**
 * Execute action
 *
 * @param this
 * 		This instance
 * @param authenticationManager
 * 		The authentication manager
 * @param parameterList
 * 		The parameters list
 *
 * @return if the operation started successfully
 */
NBOOL NGhostCommon_Action_NGhostAction_Execute( NGhostAction *this,
	__MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const NParserOutputList *parameterList );

/**
 * Add action to watcher
 *
 * @param this
 * 		This instance
 * @param ghostStatusReferal
 * 		The ghost status referal
 * @param userData
 * 		The user data
 * @param ghostCommon
 * 		The ghost common
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_AddActionToWatcher( const NGhostAction*,
	const void *ghostStatusReferal,
	const NParserOutputList *userData,
	void *ghostCommon,
	const char *contactingIP );

/**
 * Is action the same?
 *
 * @param this
 * 		This instance
 * @param action
 * 		The other action to compare with
 *
 * @return if the actions are the same
 */
NBOOL NGhostCommon_Action_NGhostAction_IsSame( const NGhostAction*,
	const NGhostAction *action );

/**
 * Get base64 hash
 *
 * @param this
 * 		This instance
 *
 * @return the base64 hash
 */
const char *NGhostCommon_Action_NGhostAction_GetBase64Hash( const NGhostAction* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_BuildParserOutputList( const NGhostAction*,
	const char *keyRoot,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_ProcessRESTGETRequest( NGhostAction*,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Add detail to parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_AddToParserOutputList( const NGhostAction*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	const char *contactingIP );

#endif // !NGHOSTCOMMON_ACTION_NGHOSTACTION_PROTECT
