#ifndef NGHOSTCOMMON_TOKEN_NGHOSTTRUSTEDTOKENIPLIST_PROTECT
#define NGHOSTCOMMON_TOKEN_NGHOSTTRUSTEDTOKENIPLIST_PROTECT

// ----------------------------------------------------
// struct NGhostCommon::Token::NGhostTrustedTokenIPList
// ----------------------------------------------------

typedef struct NGhostTrustedTokenIPList
{
	// IP list
	NListe *m_list;
} NGhostTrustedTokenIPList;

/**
 * Build token trusted ip list
 *
 * @param configurationLine
 * 		The configuration line (IP1,...,IPn)
 *
 * @return the built instance
 */
__ALLOC NGhostTrustedTokenIPList *NGhostCommon_Token_NGhostTrustedTokenIPList_Build( const char *configurationLine );

/**
 * Build from another instance
 *
 * @param source
 * 		The source
 *
 * @return the built instance
 */
__ALLOC NGhostTrustedTokenIPList *NGhostCommon_Token_NGhostTrustedTokenIPList_Build2( const NGhostTrustedTokenIPList *source );

/**
 * Destroy token trusted ip list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( NGhostTrustedTokenIPList** );

/**
 * Is trusted IP?
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip to check
 *
 * @return if the ip is trusted
 */
NBOOL NGhostCommon_Token_NGhostTrustedTokenIPList_IsTrusted( const NGhostTrustedTokenIPList*,
	const char *ip );

/**
 * Export
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostCommon_Token_NGhostTrustedTokenIPList_Export( const NGhostTrustedTokenIPList* );

#endif // !NGHOSTCOMMON_TOKEN_NGHOSTTRUSTEDTOKENIPLIST_PROTECT
