#ifndef NGHOSTCOMMON_TOKEN_PROTECT
#define NGHOSTCOMMON_TOKEN_PROTECT

// -----------------------------
// namespace NGhostCommon::Token
// -----------------------------

// Token persistence file extension
#define NGHOST_COMMON_TOKEN_PERSISTENT_FILE_EXTENSION	"token"

// Authorization timeout
#define NGHOST_COMMON_TOKEN_AUTHORIZATION_TIMEOUT		10000

// Token length (HEAD-RANDRANDRANDRAND-RAND)
#define NGHOST_COMMON_TOKEN_LENGTH						26

// Token middle part length
#define NGHOST_COMMON_TOKEN_MIDDLE_PART_LENGTH			16

// Token footer part length
#define NGHOST_COMMON_TOKEN_FOOTER_LENGTH				4

// Token header length
#define NGHOST_COMMON_TOKEN_HEADER_LENGTH				4

// struct NGhostCommon::Token::NGhostTrustedTokenIPList
#include "NGhostCommon_Token_NGhostTrustedTokenIPList.h"

// struct NGhostCommon::Token::NGhostTokenManager
#include "NGhostCommon_Token_NGhostTokenManager.h"

/**
 * Generate token
 *
 * @param header
 * 		The token header (must be length = 4)
 *
 * @return the generated token
 */
__ALLOC char *NGhostCommon_Token_GenerateToken( const char *header );

#endif // !NGHOSTCOMMON_TOKEN_PROTECT

