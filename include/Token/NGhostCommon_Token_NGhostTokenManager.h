#ifndef NGHOSTCOMMON_TOKEN_NGHOSTTOKENMANAGER_PROTECT
#define NGHOSTCOMMON_TOKEN_NGHOSTTOKENMANAGER_PROTECT

// ----------------------------------------------
// struct NGhostCommon::Token::NGhostTokenManager
// ----------------------------------------------

typedef struct NGhostTokenManager
{
	// Token list
	NListe *m_token;

	// Token constant header (length = 4) [HEAD-RANDRANDRANDRAND-RAND]
	char *m_header;

	// Device name
	char *m_deviceName;

	// Authorization time
	NU64 m_authorizationTime;

	// Trusted IP
	NGhostTrustedTokenIPList *m_trustedIP;
} NGhostTokenManager;

/**
 * Build token manager
 *
 * @param tokenHeader
 * 		The token constant header
 * @param deviceName
 * 		The device name
 * @param trustedTokenIPList
 * 		The trusted ip list
 *
 * @return the token manager
 */
__ALLOC NGhostTokenManager *NGhostCommon_Token_NGhostTokenManager_Build( const char *tokenHeader,
	const char *deviceName,
	const NGhostTrustedTokenIPList *trustedTokenIPList );

/**
 * Destroy token manager
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTokenManager_Destroy( NGhostTokenManager** );

/**
 * Authorize token generation
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTokenManager_AuthorizeTokenGeneration( NGhostTokenManager* );

/**
 * Is authorized to request token?
 *
 * @param this
 * 		This instance
 * @param clientIP
 * 		The client IP
 *
 * @return if authorized to request token
 */
NBOOL NGhostCommon_Token_IsAuthorizeTokenRequest( NGhostTokenManager*,
	const char *clientIP );

/**
 * Request token
 *
 * @param this
 * 		This instance
 * @param isForce
 * 		Do we force the creation?
 * @param clientIP
 * 		The requesting client IP
 *
 * @return the generated token or NULL if unauthorized
 */
__MUSTBEPROTECTED const char *NGhostCommon_Token_RequestToken( NGhostTokenManager*,
	NBOOL isForce,
	const char *clientIP );

/**
 * Is token authorized ?
 *
 * @param this
 * 		This instance
 * @param token
 * 		The token to be checked
 *
 * @return if token is authorized
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Token_IsTokenAuthorized( const NGhostTokenManager*,
	const char *token );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Token_NGhostTokenManager_ActivateProtection( NGhostTokenManager* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Token_NGhostTokenManager_DeactivateProtection( NGhostTokenManager* );

/**
 * Persist allowed tokens
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Token_NGhostTokenManager_PersistToken( const NGhostTokenManager* );

#endif // !NGHOSTCOMMON_TOKEN_NGHOSTTOKENMANAGER_PROTECT
