#ifndef NGHOSTCOMMON_PROTECT
#define NGHOSTCOMMON_PROTECT

// ----------------------
// namespace NGhostCommon
// ----------------------

// namespace NLib
#include "../../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser
#include "../../../../NParser/NParser/include/NParser.h"

// namespace NJson
#include "../../../../NParser/NJson/include/NJson.h"

// namespace NDeviceCommon
#include "../../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NGhostCommon::Token
#include "Token/NGhostCommon_Token.h"

// namespace NGhostCommon::Action
#include "Action/NGhostCommon_Action.h"

// namespace NGhostCommon::Event
#include "Event/NGhostCommon_Event.h"

// namespace NGhostCommon::Service
#include "Service/NGhostCommon_Service.h"

// __
#include "__/__NGhostCommon_Build.h"

// struct NGhostCommon::NGhostCommon
#include "NGhostCommon_NGhostCommon.h"

// namespace NGhostCommon::Monitoring
#include "Monitoring/NGhostCommon_Monitoring.h"

// namespace NGhostCommon::Hardware
#include "Hardware/NGhostCommon_Hardware.h"

// namespace NGhostCommon::Helper
#include "Helper/NGhostCommon_Helper.h"



#endif // !NGHOSTCOMMON_PROTECT
