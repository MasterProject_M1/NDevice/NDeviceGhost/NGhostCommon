#ifndef NGhostCommon_Helper_PROTECT
#define NGhostCommon_Helper_PROTECT

// ------------------------------
// namespace NGhostCommon::Helper
// ------------------------------

// Short call
#define NGhostClient_Helper_BuildVersionShort( version ) \
	NGhostCommon_Helper_BuildVersion( version, \
		NGHOST_VERSION_MAJOR, \
		NGHOST_VERSION_MINOR )

/**
 * Build version
 *
 * @param this
 * 		This instance
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 * 		The minor version
 */
void NGhostCommon_Helper_BuildVersion( __OUTPUT char version[ 32 ],
	NU32 majorVersion,
	NU32 minorVersion );

#endif // !NGhostCommon_Helper_PROTECT
