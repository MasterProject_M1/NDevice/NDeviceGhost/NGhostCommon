#include "../../include/NGhostCommon.h"

// --------------------------------
// namespace NGhostCommon::Hardware
// --------------------------------

/**
 * Get an id
 * It will be based only on NIC's mac xor for now... (won't change on a raspberry)
 *
 * @param deviceType
 * 		The device type
 *
 * @return a unique ID base64 encoded
 */
__ALLOC char *NGhostCommon_Hardware_GetUniqueID( NDeviceType deviceType )
{
	// Output
	char *out;

	// Interface list
	NListe *interfaceList;

	// ID
	NU64 id = 0;

	// Check digit
	NU32 checkDigit = 0;

	// Current mac
	NU64 currentMAC = 0;

	// Network interface
	const NInterfaceReseau *interface;

	// Iterator
	NU32 i;

	// Components
	char component[ 4 ][ 5 ];

	// Get interfaces list
	if( !( interfaceList = NLib_Module_Reseau_Interface_TrouverTouteInterface( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Quit
		return NULL;
	}

	// Iterate
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( interfaceList ); i++ )
		// Get interface
		if( ( interface = NLib_Memoire_NListe_ObtenirElementDepuisIndex( interfaceList,
			i ) ) != NULL
			&& NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresseMAC( interface ) != NULL )
		{
			// Get current mac
			NLib_Module_Reseau_IP_DivideIP( NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresseMAC( interface ),
				NPROTOCOLE_IP_IPV6,
				(NU8*)&currentMAC );

			// XOR with current mac
			id ^= currentMAC;
		}

	// Add device type
	id ^= NDeviceCommon_Type_NDeviceType_GetIdModifier( deviceType );

	// Destroy interfaces list
	NLib_Memoire_NListe_Detruire( &interfaceList );

	// Calculate check digits
	for( i = 2; (NS32)i >= 0; i-- )
		checkDigit ^= ( id >> ( i * 16 ) ) & 0x0000FFFF;

	// Convert check digit
	snprintf( component[ 0 ],
		5,
		"%04X",
		checkDigit );

	// Convert id parts
	for( i = 2; (NS32)i >= 0; i-- )
		snprintf( component[ 3 - i ],
			5,
			"%04llX",
			( id >> ( i * 16 ) ) & 0x0000FFFF );

	// Allocate output
	if( !( out = calloc( 4 * 4 + 3 + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Write in output
	for( i = 0; i < 4; i++ )
	{
		strcat( out,
			component[ i ] );
		if( i + 1 < 4 )
			strcat( out,
				"-" );
	}

	// OK
	return out;
}

