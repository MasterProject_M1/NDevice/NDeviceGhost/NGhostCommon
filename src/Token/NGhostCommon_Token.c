#include "../../include/NGhostCommon.h"

// -----------------------------
// namespace NGhostCommon::Token
// -----------------------------

/**
 * Generate token character (private)
 *
 * @return token character
 */
__PRIVATE char NGhostCommon_Token_GenerateTokenCharacter( void )
{
	// Output
	char c;

	do
	{
		// Generate random character
		c = (char)( rand( )%128 );
	} while( !NLib_Caractere_EstUnChiffre( c )
		&& !NLib_Caractere_EstUneLettre( c ) );

	// OK
	return c;
}

/**
 * Generate token
 *
 * @param header
 * 		The token header (must be length = 4)
 *
 * @return the generated token
 */
__ALLOC char *NGhostCommon_Token_GenerateToken( const char *header )
{
	// Output
	__OUTPUT char *token;

	// Iterator
	NU32 i = 0;

	// Check header
	if( strlen( header ) < NGHOST_COMMON_TOKEN_HEADER_LENGTH )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Allocate memory
	if( !( token = calloc( NGHOST_COMMON_TOKEN_LENGTH + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Copy header
	memcpy( token,
		header,
		NGHOST_COMMON_TOKEN_HEADER_LENGTH );

	// Add separator
	token[ strlen( token ) ] = '-';

	// Generate random content
	for( i = 0; i < NGHOST_COMMON_TOKEN_MIDDLE_PART_LENGTH; i++ )
		token[ strlen( token ) ] = NGhostCommon_Token_GenerateTokenCharacter( );

	// Add separator
	token[ strlen( token ) ] = '-';

	// Generate random footer
	for( i = 0; i < NGHOST_COMMON_TOKEN_FOOTER_LENGTH; i++ )
		token[ strlen( token ) ] = NGhostCommon_Token_GenerateTokenCharacter( );

	// OK
	return token;
}

