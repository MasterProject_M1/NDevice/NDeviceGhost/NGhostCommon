#include "../../include/NGhostCommon.h"

// ----------------------------------------------
// struct NGhostCommon::Token::NGhostTokenManager
// ----------------------------------------------

/**
 * Load tokens (private)
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Token_NGhostTokenManager_Load( NGhostTokenManager *this )
{
	// File
	NFichierTexte *file;

	// File path
	char filePath[ 256 ];

	// Line
	char *line;

	// Build file path
	snprintf( filePath,
		256,
		"%s.%s",
		this->m_deviceName,
		NGHOST_COMMON_TOKEN_PERSISTENT_FILE_EXTENSION );

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireLecture( filePath ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NFALSE;
	}

	// Load tokens
	while( ( line = NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( file,
		'#',
		NFALSE ) ) != NULL )
	{
		// Remove useless characters
		NLib_Chaine_SupprimerCaractere( line,
			'\r' );

		// Add
		NLib_Memoire_NListe_Ajouter( this->m_token,
			line );
	}

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

/**
 * Build token manager
 *
 * @param tokenHeader
 * 		The token constant header
 * @param deviceName
 * 		The device name
 * @param trustedTokenIPList
 * 		The trusted ip list
 *
 * @return the token manager
 */
__ALLOC NGhostTokenManager *NGhostCommon_Token_NGhostTokenManager_Build( const char *tokenHeader,
	const char *deviceName,
	const NGhostTrustedTokenIPList *trustedTokenIPList )
{
	// Output
	__OUTPUT NGhostTokenManager *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostTokenManager ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build list
	if( !( out->m_token = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void * ))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Duplicate informations
	if( !( out->m_deviceName = NLib_Chaine_Dupliquer( deviceName ) )
		|| !( out->m_header = NLib_Chaine_Dupliquer( tokenHeader ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Destroy
		NLib_Memoire_NListe_Detruire( &out->m_token );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save trusted ip
	if( !( out->m_trustedIP = NGhostCommon_Token_NGhostTrustedTokenIPList_Build2( trustedTokenIPList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NLib_Memoire_NListe_Detruire( &out->m_token );

		// Free
		NFREE( out->m_header );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Load tokens
	NGhostCommon_Token_NGhostTokenManager_Load( out );

	// OK
	return out;
}

/**
 * Destroy token manager
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTokenManager_Destroy( NGhostTokenManager **this )
{
	// Destroy
	NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &(*this)->m_trustedIP );
	NLib_Memoire_NListe_Detruire( &(*this)->m_token );

	// Free
	NFREE( (*this)->m_deviceName );
	NFREE( (*this)->m_header );
	NFREE( (*this) );
}

/**
 * Authorize token generation
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTokenManager_AuthorizeTokenGeneration( NGhostTokenManager *this )
{
	this->m_authorizationTime = NLib_Temps_ObtenirTick64( );
}

/**
 * Is authorized to request token?
 *
 * @param this
 * 		This instance
 * @param clientIP
 * 		The client IP
 *
 * @return if authorized to request token
 */
NBOOL NGhostCommon_Token_IsAuthorizeTokenRequest( NGhostTokenManager *this,
	const char *clientIP )
{
	return (NBOOL)( ( this->m_authorizationTime != NERREUR
			&& NLib_Temps_ObtenirTick64( ) - this->m_authorizationTime < NGHOST_COMMON_TOKEN_AUTHORIZATION_TIMEOUT )
		|| NGhostCommon_Token_NGhostTrustedTokenIPList_IsTrusted( this->m_trustedIP,
			clientIP ) );
}

/**
 * Request token
 *
 * @param this
 * 		This instance
 * @param isForce
 * 		Do we force the creation?
 * @param clientIP
 * 		The requesting client IP
 *
 * @return the generated token or NULL if unauthorized
 */
__MUSTBEPROTECTED const char *NGhostCommon_Token_RequestToken( NGhostTokenManager *this,
	NBOOL isForce,
	const char *clientIP )
{
	// Token
	char *token;

	// Check if authorized
	if( !NGhostCommon_Token_IsAuthorizeTokenRequest( this,
			clientIP )
		&& !isForce )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

		// Quit
		return NULL;
	}

	// Generate token
	if( !( token = NGhostCommon_Token_GenerateToken( this->m_header ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Quit
		return NULL;
	}

	// Reset authorization
	this->m_authorizationTime = NERREUR;

	// Add token
	NLib_Memoire_NListe_Ajouter( this->m_token,
		token );

	// Persist
	NGhostCommon_Token_NGhostTokenManager_PersistToken( this );

	// OK
	return token;
}

/**
 * Is token authorized ?
 *
 * @param this
 * 		This instance
 * @param token
 * 		The token to be checked
 *
 * @return if token is authorized
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Token_IsTokenAuthorized( const NGhostTokenManager *this,
	const char *token )
{
	// Iterator
	NU32 i = 0;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_token ); i++ )
		// Check
		if( NLib_Chaine_Comparer( token,
			NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_token,
				i ),
			NTRUE,
			0 ) )
			// Found it !
			return NTRUE;

	// Not found :(
	return NFALSE;
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Token_NGhostTokenManager_ActivateProtection( NGhostTokenManager *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_token );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Token_NGhostTokenManager_DeactivateProtection( NGhostTokenManager *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_token );
}

/**
 * Persist allowed tokens
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Token_NGhostTokenManager_PersistToken( const NGhostTokenManager *this )
{
	// File
	NFichierTexte *file;

	// File path
	char filePath[ 256 ];

	// Iterator
	NU32 i;

	// Token
	const char *token;

	// Build file path
	snprintf( filePath,
		256,
		"%s.%s",
		this->m_deviceName,
		NGHOST_COMMON_TOKEN_PERSISTENT_FILE_EXTENSION );

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( filePath,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OVERRIDE );

		// Quit
		return NFALSE;
	}

	// Write tokens
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_token ); i++ )
		// Get token
		if( ( token = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_token,
			i ) ) != NULL )
		{
			// Write token
			NLib_Fichier_NFichierTexte_Ecrire3( file,
				token );

			// Separate
			NLib_Fichier_NFichierTexte_Ecrire6( file,
				'\n' );
		}

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}
