#include "../../include/NGhostCommon.h"

// ----------------------------------------------------
// struct NGhostCommon::Token::NGhostTrustedTokenIPList
// ----------------------------------------------------

/**
 * Build token trusted ip list
 *
 * @param configurationLine
 * 		The configuration line (IP1, ..., IPn)
 *
 * @return the built instance
 */
__ALLOC NGhostTrustedTokenIPList *NGhostCommon_Token_NGhostTrustedTokenIPList_Build( const char *configurationLine )
{
	// Output
	NGhostTrustedTokenIPList *out;

	// Iterator
	NU32 i;

	// Element
	char *element;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostTrustedTokenIPList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Cut string
	if( !( out->m_list = NLib_Chaine_Decouper( configurationLine,
		',' ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NULL;
	}

	// Correct entries
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( out->m_list ); i++ )
		// Get element
		if( ( element = (char*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( out->m_list,
			i ) ) != NULL )
			// Remove space
			NLib_Chaine_SupprimerCaractereDebut( element,
				' ' );

	// OK
	return out;
}

/**
 * Build from another instance
 *
 * @param source
 * 		The source
 *
 * @return the built instance
 */
__ALLOC NGhostTrustedTokenIPList *NGhostCommon_Token_NGhostTrustedTokenIPList_Build2( const NGhostTrustedTokenIPList *source )
{
	// Output
	__ALLOC NGhostTrustedTokenIPList *out;

	// Iterator
	NU32 i;

	// Element
	const char *element;

	// New element
	char *newElement;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostTrustedTokenIPList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl*)( void*))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Add element
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( source->m_list ); i++ )
		// Get element
		if( ( element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( source->m_list,
			i ) ) != NULL )
			// Duplicate element
			if( ( newElement = NLib_Chaine_Dupliquer( element ) ) != NULL )
				// Add to list
				NLib_Memoire_NListe_Ajouter( out->m_list,
					newElement );

	// OK
	return out;
}

/**
 * Destroy token trusted ip list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( NGhostTrustedTokenIPList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this) );
}

/**
 * Is trusted IP?
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip to check
 *
 * @return if the ip is trusted
 */
NBOOL NGhostCommon_Token_NGhostTrustedTokenIPList_IsTrusted( const NGhostTrustedTokenIPList *this,
	const char *ip )
{
	// Iterator
	NU32 i = 0;

	// Element
	const char *element;

	// Look for ip
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Get element
		if( ( element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) != NULL )
			// Check
			if( NLib_Chaine_Comparer( ip,
				element,
				NTRUE,
				0 ) )
				// Found it
				return NTRUE;

	// Not found
	return NFALSE;
}

/**
 * Export
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostCommon_Token_NGhostTrustedTokenIPList_Export( const NGhostTrustedTokenIPList *this )
{
	// Output
	char *out = NULL;

	// Iterator
	NU32 i = 0;

	// Add ip
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
	{
		// Add IP
		NLib_Memoire_AjouterData2( &out,
			out != NULL ?
				(NU32)strlen( out )
				: 0,
			NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				i ) );

		// Add comma
		if( i + 1 < NLib_Memoire_NListe_ObtenirNombre( this->m_list ) )
			NLib_Memoire_AjouterData2( &out,
				(NU32)strlen( out ),
				"," );
	}

	// OK
	return out != NULL ?
		out
		: calloc( 1,
			sizeof( char ) );
}
