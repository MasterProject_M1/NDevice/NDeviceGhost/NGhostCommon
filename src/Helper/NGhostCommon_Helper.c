#include "../../include/NGhostCommon.h"

// ------------------------------
// namespace NGhostCommon::Helper
// ------------------------------

/**
 * Build version
 *
 * @param this
 * 		This instance
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 * 		The minor version
 */
void NGhostCommon_Helper_BuildVersion( __OUTPUT char version[ 32 ],
	NU32 majorVersion,
	NU32 minorVersion )
{
	// Cursor
	NU32 cursor = 0;

	// Clear
	memset( version,
		0,
		32 );

	// Build
	version[ cursor++ ] = (char)( '0' + majorVersion );
	version[ cursor++ ] = '.';
	version[ cursor++ ] = (char)( '0' + minorVersion );
	version[ cursor++ ] = '-';
	version[ cursor++ ] = 'V';
	version[ cursor++ ] = '-';
	version[ cursor++ ] = BUILD_YEAR_CH0;
	version[ cursor++ ] = BUILD_YEAR_CH1;
	version[ cursor++ ] = BUILD_YEAR_CH2;
	version[ cursor++ ] = BUILD_YEAR_CH3;
	version[ cursor++ ] = '-';
	version[ cursor++ ] = (char)BUILD_MONTH_CH0;
	version[ cursor++ ] = (char)BUILD_MONTH_CH1;
	version[ cursor++ ] = '-';
	version[ cursor++ ] = (char)BUILD_DAY_CH0;
	version[ cursor++ ] = BUILD_DAY_CH1;
	version[ cursor++ ] = 'T';
	version[ cursor++ ] = BUILD_HOUR_CH0;
	version[ cursor++ ] = BUILD_HOUR_CH1;
	version[ cursor++ ] = ':';
	version[ cursor++ ] = BUILD_MIN_CH0;
	version[ cursor++ ] = BUILD_MIN_CH1;
	version[ cursor++ ] = ':';
	version[ cursor++ ] = BUILD_SEC_CH0;
	version[ cursor++ ] = BUILD_SEC_CH1;
	version[ cursor ] = '\0';
}
