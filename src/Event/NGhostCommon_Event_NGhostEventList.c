#include "../../include/NGhostCommon.h"

// -------------------------------------------
// struct NGhostCommon::Event::NGhostEventList
// -------------------------------------------

/**
 * Build event list
 *
 * @return the built instance
 */
__ALLOC NGhostEventList *NGhostCommon_Event_NGhostEventList_Build( void )
{
	// Output
	__OUTPUT NGhostEventList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostEventList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build event list
	if( !( out->m_event = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostCommon_Event_NGhostEvent_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy event list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventList_Destroy( NGhostEventList **this )
{
	// Destroy event list
	NLib_Memoire_NListe_Detruire( &(*this)->m_event );

	// Free
	NFREE( *this );
}

/**
 * Find event index from event hash
 *
 * @param this
 * 		This instance
 * @param eventHash
 * 		The base64 event hash
 *
 * @return the event index or NERREUR
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Event_NGhostEventList_FindEventIndexFromEventHash( const NGhostEventList *this,
	const char *eventHash )
{
	// Iterator
	NU32 i = 0;

	// Event
	const NGhostEvent *event;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_event ); i++ )
		// Get event
		if( ( event = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_event,
			i ) ) != NULL )
			// Check
			if( NLib_Chaine_Comparer( NGhostCommon_Event_NGhostEvent_GetEventHash( event ),
				eventHash,
				NTRUE,
				0 ) )
				// Fount it
				return i;

	// Not found
	return NERREUR;
}

/**
 * Find event from event hash
 *
 * @param this
 * 		This instance
 * @param eventHash
 * 		The event hash
 *
 * @return the event
 */
__MUSTBEPROTECTED NGhostEvent *NGhostCommon_Event_NGhostEventList_FindEventFromEventHash( const NGhostEventList *this,
	const char *eventHash )
{
	// Index
	NU32 index;

	// Find event index
	if( ( index = NGhostCommon_Event_NGhostEventList_FindEventIndexFromEventHash( this,
		eventHash ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// OK
	return (NGhostEvent*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_event,
		index );
}

/**
 * Add an event
 *
 * @param this
 * 		This instance
 * @param event
 * 		The event to add
 *
 * @return if the operation succeed
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_AddEvent( NGhostEventList *this,
	__WILLBEOWNED NGhostEvent *event )
{
	// Check if exists
	if( NGhostCommon_Event_NGhostEventList_FindEventIndexFromEventHash( this,
		NGhostCommon_Event_NGhostEvent_GetEventHash( event ) ) != NERREUR )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_ALREADY_EXISTS );

		// Destroy event
		NGhostCommon_Event_NGhostEvent_Destroy( &event );

		// Quit
		return NFALSE;
	}

	// Add
	return NLib_Memoire_NListe_Ajouter( this->m_event,
		event );
}

/**
 * Remove an event from index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The event index
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_RemoveEvent( NGhostEventList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_event,
		index );
}

/**
 * Flush events
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_FlushEvent( NGhostEventList *this )
{
	// Result
	__OUTPUT NBOOL result = NTRUE;

	// Flush
	while( NLib_Memoire_NListe_ObtenirNombre( this->m_event ) > 0 )
		result = (NBOOL)( NGhostCommon_Event_NGhostEventList_RemoveEvent( this,
			0 ) && result );

	// OK?
	return result;
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Event_NGhostEventList_ActivateProtection( NGhostEventList *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_event );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Event_NGhostEventList_DeactivateProtection( NGhostEventList *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_event );
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_BuildParserOutputListInternal( const NGhostEventList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostEventListService serviceType )
{
	// Key
	char key[ 128 ];

	// Iterator
	NU32 i;

	// Event
	const NGhostEvent *event;

	switch( serviceType )
	{
		case NGHOST_EVENT_LIST_SERVICE_COUNT:
			// Build key
			snprintf( key,
				128,
				"%s",
				NGhostCommon_Service_Event_NGhostEventListService_GetName( serviceType ) );

			// Add event count
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NLib_Memoire_NListe_ObtenirNombre( this->m_event ) );

		case NGHOST_EVENT_LIST_SERVICE_EVENT:
			// Iterate events
			for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_event ); i++ )
				// Get event
				if( ( event = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_event,
					i ) ) != NULL )
				{
					// Build key
					snprintf( key,
						128,
						"%s",
						NGhostCommon_Event_NGhostEvent_GetEventHash( event ) );

					// Build parser output list
					NGhostCommon_Event_NGhostEvent_BuildParserOutputList( event,
						key,
						parserOutputList );
				}
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_BuildParserOutputList( const NGhostEventList *this,
	NParserOutputList *parserOutputList )
{
	// Service type
	NGhostEventListService serviceType = (NGhostEventListService)0;

	// Iterate
	for( ; serviceType < NGHOST_EVENT_LIST_SERVICES; serviceType++ )
		// Add
		NGhostCommon_Event_NGhostEventList_BuildParserOutputListInternal( this,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_ProcessRESTGETRequest( NGhostEventList *this,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostEventListService serviceType;

	// Event index
	NU32 eventIndex;

	// Ghost event
	const NGhostEvent *event;

	// Find service type
	switch( serviceType = NGhostCommon_Service_Event_NGhostEventListService_FindService( requestedElement,
		cursor,
		this,
		&eventIndex ) )
	{
		case NGHOST_EVENT_LIST_SERVICE_ROOT:
			return NGhostCommon_Event_NGhostEventList_BuildParserOutputList( this,
				parserOutputList );

		case NGHOST_EVENT_LIST_SERVICE_EVENT:
			if( ( event = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_event,
				eventIndex ) ) != NULL )
				return NGhostCommon_Event_NGhostEvent_ProcessRESTGETRequest( event,
					requestedElement,
					cursor,
					parserOutputList );
			else
				return NFALSE;

		default:
			return NGhostCommon_Event_NGhostEventList_BuildParserOutputListInternal( this,
				parserOutputList,
				serviceType );
	}
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param senderIP
 * 		The sender IP
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_ProcessRESTPOSTRequest( NGhostEventList *this,
	const char *requestedElement,
	NU32 *cursor,
	const char *senderIP,
	const NParserOutputList *parserOutputList )
{
	// Event index
	NU32 eventIndex;

	// Event
	NGhostEvent *event;

	// Process
	switch( NGhostCommon_Service_Event_NGhostEventListService_FindService( requestedElement,
		cursor,
		this,
		&eventIndex ) )
	{
		case NGHOST_EVENT_LIST_SERVICE_ROOT:
			// Build event
			if( !( event = NGhostCommon_Event_NGhostEvent_Build2( parserOutputList,
				senderIP ) ) )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// Add event
			return NGhostCommon_Event_NGhostEventList_AddEvent( this,
				event );

		default:
			return NFALSE;
	}
}

/**
 * Process REST DELETE request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Event_NGhostEventList_ProcessRESTDELETERequest( NGhostEventList *this,
	const char *requestedElement,
	NU32 *cursor )
{
	// Event index
	NU32 eventIndex;

	// Process
	switch( NGhostCommon_Service_Event_NGhostEventListService_FindService( requestedElement,
		cursor,
		this,
		&eventIndex ) )
	{
		case NGHOST_EVENT_LIST_SERVICE_EVENT:
			return NGhostCommon_Event_NGhostEventList_RemoveEvent( this,
				eventIndex );

		case NGHOST_EVENT_LIST_SERVICE_ROOT:
			return NGhostCommon_Event_NGhostEventList_FlushEvent( this );

		default:
			return NFALSE;
	}
}

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param requestType
 * 		The request type
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 * @param client
 * 		The client
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostCommon_Event_NGhostEventList_ProcessRESTRequest( NGhostEventList *this,
	const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *clientData,
	NU32 clientDataLength,
	const NClientServeur *client )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Result
	NBOOL result = NFALSE;

	// Process
	switch( requestType )
	{
		case NTYPE_REQUETE_HTTP_GET:
			// Build parser output list
			if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Quit
				return NULL;
			}

			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_event );

			// Process REST GET request
			result = NGhostCommon_Event_NGhostEventList_ProcessRESTGETRequest( this,
				requestedElement,
				cursor,
				parserOutputList );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_event );

			// Generate response packet
			return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( result ?
					NHTTP_CODE_200_OK
					: NHTTP_CODE_206_PARTIAL_CONTENT,
				parserOutputList,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NTYPE_REQUETE_HTTP_POST:
			// Parse
			if( !( parserOutputList = NJson_Engine_Parser_Parse( clientData,
				clientDataLength ) ) )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple(	NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
					NHTTP_CODE_400_BAD_REQUEST,
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
			}

			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_event );

			// Process
			result = NGhostCommon_Event_NGhostEventList_ProcessRESTPOSTRequest( this,
				requestedElement,
				cursor,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				parserOutputList );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_event );

			// Destroy parser output list
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );
			break;
		case NTYPE_REQUETE_HTTP_DELETE:
			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_event );

			// Process
			result = NGhostCommon_Event_NGhostEventList_ProcessRESTDELETERequest( this,
				requestedElement,
				cursor );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_event );
			break;

		default:
			break;
	}

	return result ?
		NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
			NHTTP_CODE_200_OK,
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) )
		: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple(	NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
			NHTTP_CODE_400_BAD_REQUEST,
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
}

/**
 * Get event count
 *
 * @param this
 * 		This instance
 *
 * @return the event count
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Event_NGhostEventList_GetEventCount( const NGhostEventList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_event );
}

/**
 * Get event from index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The event index
 *
 * @return the event
 */
__MUSTBEPROTECTED const NGhostEvent *NGhostCommon_Event_NGhostEventList_GetEvent( const NGhostEventList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_event,
		index );
}
