#include "../../include/NGhostCommon.h"

// --------------------------------------------------
// struct NGhostCommon::Event::NGhostEventDestination
// --------------------------------------------------

/**
 * Build destination
 *
 * @param host
 * 		The destination host
 * @param port
 * 		The destination port
 * @param requestPath
 * 		The request path
 *
 * @return the built instance
 */
__ALLOC NGhostEventDestination *NGhostCommon_Event_NGhostEventDestination_Build( const char *host,
	NU32 port,
	const char *requestPath )
{
	// Output
	__OUTPUT NGhostEventDestination *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostEventDestination ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out->m_host = NLib_Chaine_Dupliquer( host ) )
		|| !( out->m_requestPath = NLib_Chaine_Dupliquer( requestPath ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_host );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_port = port;

	// OK
	return out;
}

/**
 * Destroy the destination
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventDestination_Destroy( NGhostEventDestination **this )
{
	NFREE( (*this)->m_requestPath );
	NFREE( (*this)->m_host );
	NFREE( (*this) );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Event_NGhostEventDestination_BuildParserOutputListInternal( const NGhostEventDestination *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostEventDestinationService serviceType )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
			: "",
		NGhostCommon_Service_Event_NGhostEventDestinationService_GetName( serviceType ) );

	// Add entry
	switch( serviceType )
	{
		case NGHOST_EVENT_DESTINATION_SERVICE_HOSTNAME:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_host );
		case NGHOST_EVENT_DESTINATION_SERVICE_PORT:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_port );

		case NGHOST_EVENT_DESTINATION_SERVICE_REQUEST_PATH:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_requestPath );

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventDestination_BuildParserOutputList( const NGhostEventDestination *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostEventDestinationService serviceType = (NGhostEventDestinationService)0;

	// Iterate
	for( ; serviceType < NGHOST_EVENT_DESTINATION_SERVICES; serviceType++ )
		NGhostCommon_Event_NGhostEventDestination_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventDestination_ProcessRESTGETRequest( const NGhostEventDestination *this,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostEventDestinationService serviceType;

	// Process service
	switch( serviceType = NGhostCommon_Service_Event_NGhostEventDestinationService_FindService( requestedElement,
		cursor ) )
	{
		default:
			return NGhostCommon_Event_NGhostEventDestination_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );

		case NGHOST_EVENT_DESTINATION_SERVICE_ROOT:
			return NGhostCommon_Event_NGhostEventDestination_BuildParserOutputList( this,
				"",
				parserOutputList );
	}
}
