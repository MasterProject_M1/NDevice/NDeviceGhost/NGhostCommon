#include "../../include/NGhostCommon.h"

// ---------------------------------------
// struct NGhostCommon::Event::NGhostEvent
// ---------------------------------------

/**
 * Build event
 * When an event is built, its hash is based on:
 *
 * The action hash
 * AND
 * currentTicks - ( currentTicks % NGHOST_COMMON_EVENT_EVENT_HASH_TIME_WINDOW )
 *
 * @param senderIP
 * 		The sender IP
 * @param senderDeviceType
 * 		The sender type (or NDEVICE_TYPES if unknown)
 * @param senderDeviceUUID
 * 		The sender device (or NULL if unknown)
 * @param destinationHostname
 * 		The destination hostname
 * @param destinationPort
 * 		The destination port
 * @param actionHash
 * 		The action hash
 * @param userData
 * 		The user data, from where parameters will be extracted
 * @param delayBeforeActivation
 * 		The delay before activation (ms)
 *
 * @return the built instance
 */
__ALLOC NGhostEvent *NGhostCommon_Event_NGhostEvent_Build( const char *senderIP,
	NDeviceType senderDeviceType,
	const char *senderDeviceUUID,
	const char *destinationHostname,
	NU32 destinationPort,
	const char *actionHash,
	const NParserOutputList *userData,
	NU32 delayBeforeActivation )
{
	// Output
	__OUTPUT NGhostEvent *out;

	// Buffer
	char buffer[ 256 ];

	// Time
	NU64 tick;

	// MD5 hash context
	NMD5_CTX md5CTX;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostEvent ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build source
	if( !( out->m_source = NGhostCommon_Event_NGhostEventSource_Build( senderIP,
		senderDeviceType,
		senderDeviceUUID ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build path
	snprintf( buffer,
		256,
		"%s:%d%s%s",
		destinationHostname,
		destinationPort,
		NDEVICE_COMMON_TYPE_GHOST_ACTION_SERVICE,
		actionHash );

	// Build destination
	if( !( out->m_destination = NGhostCommon_Event_NGhostEventDestination_Build( destinationHostname,
		destinationPort,
		buffer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy source
		NGhostCommon_Event_NGhostEventSource_Destroy( &out->m_source );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out->m_actionHash = NLib_Chaine_Dupliquer( actionHash ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Destroy
		NGhostCommon_Event_NGhostEventDestination_Destroy( &out->m_destination );
		NGhostCommon_Event_NGhostEventSource_Destroy( &out->m_source );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_timestamp = NLib_Temps_ObtenirTimestamp( );
	out->m_addTick = NLib_Temps_ObtenirTick64( );
	out->m_delayBeforeActivation = delayBeforeActivation;

	// Extract parameters
	if( !( out->m_parameterList = NParser_Output_NParserOutputList_GetAllOutput( userData,
		NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_PARAMETER_LIST ),
		NTRUE,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Destroy
		NGhostCommon_Event_NGhostEventDestination_Destroy( &out->m_destination );
		NGhostCommon_Event_NGhostEventSource_Destroy( &out->m_source );

		// Free
		NFREE( out->m_actionHash );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Calculate hash
	NLib_Math_MD5_Init( &md5CTX );
	NLib_Math_MD5_Update( &md5CTX,
		out->m_actionHash,
		strlen( out->m_actionHash ) );
	tick = NLib_Temps_ObtenirTick64( );
	tick = tick - ( tick % NGHOST_COMMON_EVENT_EVENT_HASH_TIME_WINDOW );
	NLib_Math_MD5_Update( &md5CTX,
		&tick,
		sizeof( NU64 ) );
	NLib_Math_MD5_Final( out->m_hash,
		&md5CTX );

	// Generate base64 hash
	if( !( out->m_eventHash = NLib_Chaine_ConvertirBase64_2( (char*)out->m_hash,
		16,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Destroy parameters list
		NParser_Output_NParserOutputList_Destroy( &out->m_parameterList );

		// Destroy
		NGhostCommon_Event_NGhostEventDestination_Destroy( &out->m_destination );
		NGhostCommon_Event_NGhostEventSource_Destroy( &out->m_source );

		// Free
		NFREE( out->m_actionHash );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Build action from user data
 *
 * @param userData
 * 		The user data
 * @param senderIP
 * 		The sender ip
 *
 * @return the built instance
 */
__ALLOC NGhostEvent *NGhostCommon_Event_NGhostEvent_Build2( const NParserOutputList *parserOutputList,
	const char *senderIP )
{
	// Entry
	const NParserOutput *parserOutput;

#define PARAMETER_ERROR( ) \
    { \
        /* Notify */ \
        NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX ); \
 \
        /* Quit */ \
        return NULL; \
    }

	// Parameters
	NDeviceType senderDeviceType = NDEVICES_TYPE;
	const char *senderDeviceUUID = NULL;
	const char *destinationHostname;
	NU32 destinationPort;
	const char *actionHash;
	NU32 delayBeforeActivation = 0;

	/* Optional parameters */
	// Get sender device type
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGHOST_EVENT_SOURCE_SERVICE_DEVICE_TYPE ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
		senderDeviceType = NDeviceCommon_Type_NDeviceType_FindTypeFromName( NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Get sender device UUID
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGHOST_EVENT_SOURCE_SERVICE_DEVICE_UUID ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
		senderDeviceUUID = NParser_Output_NParserOutput_GetValue( parserOutput );

	// Get delay before activation
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_EVENT_DELAY_BEFORE_ACTIVATION ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_INTEGER )
		delayBeforeActivation = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );

	/* Not optional parameters */
	// Get destination hostname
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGHOST_EVENT_DESTINATION_SERVICE_HOSTNAME ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
		destinationHostname = NParser_Output_NParserOutput_GetValue( parserOutput );
	else
		PARAMETER_ERROR( );

	// Get destination port
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGHOST_EVENT_DESTINATION_SERVICE_PORT ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_INTEGER )
		destinationPort = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );
	else
		PARAMETER_ERROR( );

	// Get action hash
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_ACTION_HASH ),
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
		actionHash = NParser_Output_NParserOutput_GetValue( parserOutput );
	else
		PARAMETER_ERROR( );

#undef PARAMETER_ERROR

	// Build event
	return NGhostCommon_Event_NGhostEvent_Build( senderIP,
		senderDeviceType,
		senderDeviceUUID,
		destinationHostname,
		destinationPort,
		actionHash,
		parserOutputList,
		delayBeforeActivation );
}

/**
 * Destroy event
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEvent_Destroy( NGhostEvent **this )
{
	// Destroy parameters
	NParser_Output_NParserOutputList_Destroy( &(*this)->m_parameterList );

	// Destroy
	NGhostCommon_Event_NGhostEventDestination_Destroy( &(*this)->m_destination );
	NGhostCommon_Event_NGhostEventSource_Destroy( &(*this)->m_source );

	// Free
	NFREE( (*this)->m_eventHash );
	NFREE( (*this)->m_actionHash );
	NFREE( *this );
}

/**
 * Get event hash
 *
 * @param this
 * 		This instance
 *
 * @return the event hash
 */
const char *NGhostCommon_Event_NGhostEvent_GetEventHash( const NGhostEvent *this )
{
	return this->m_eventHash;
}

/**
 * Is ready?
 *
 * @param this
 * 		This instance
 *
 * @return if the event is ready
 */
__PRIVATE NBOOL NGhostCommon_Event_NGhostEvent_IsReady( const NGhostEvent *this )
{
	return (NBOOL)( NLib_Temps_ObtenirTick64( ) - this->m_addTick >= this->m_delayBeforeActivation );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Event_NGhostEvent_BuildParserOutputListInternal( const NGhostEvent *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostEventService serviceType )
{
	// Key
	char key[ 128 ];

	// Iterator
	NU32 i;

	// Parameter
	const NParserOutput *parameter;

	// Value copy
	char *valueCopy;

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
			: "",
		NGhostCommon_Service_Event_NGhostEventService_GetName( serviceType ) );

	// Add property
	switch( serviceType )
	{
		case NGHOST_EVENT_SERVICE_EVENT_HASH:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_eventHash );
		case NGHOST_EVENT_SERVICE_EVENT_TIMESTAMP:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				(NU32)this->m_timestamp );
		case NGHOST_EVENT_SERVICE_EVENT_DELAY_BEFORE_ACTIVATION:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_delayBeforeActivation );
		case NGHOST_EVENT_SERVICE_EVENT_IS_READY:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				NGhostCommon_Event_NGhostEvent_IsReady( this ) );
		case NGHOST_EVENT_SERVICE_ACTION_HASH:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_actionHash );
		case NGHOST_EVENT_SERVICE_PARAMETER_LIST:
			for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( this->m_parameterList ); i++ )
				if( ( parameter = NParser_Output_NParserOutputList_GetEntry( this->m_parameterList,
					i ) ) )
					if( ( valueCopy = NParser_Output_NParserOutput_GetValueCopy( parameter ) ) != NULL )
					NParser_Output_NParserOutputList_AddEntry( parserOutputList,
						key,
						valueCopy,
						NParser_Output_NParserOutput_GetType( parameter ) );

			return NTRUE;
		case NGHOST_EVENT_SERVICE_DESTINATION:
			return NGhostCommon_Event_NGhostEventDestination_BuildParserOutputList( this->m_destination,
				key,
				parserOutputList );
		case NGHOST_EVENT_SERVICE_SOURCE:
			return NGhostCommon_Event_NGhostEventSource_BuildParserOutputList( this->m_source,
				key,
				parserOutputList );

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEvent_BuildParserOutputList( const NGhostEvent *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostEventService serviceType = (NGhostEventService)0;

	// Iterate
	for( ; serviceType < NGHOST_EVENT_SERVICES; serviceType++ )
		// Process
		NGhostCommon_Event_NGhostEvent_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEvent_ProcessRESTGETRequest( const NGhostEvent *this,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostEventService serviceType;

	// Process service
	switch( serviceType = NGhostCommon_Service_Event_NGhostEventService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_EVENT_SERVICE_ROOT:
			return NGhostCommon_Event_NGhostEvent_BuildParserOutputList( this,
				"",
				parserOutputList );

		case NGHOST_EVENT_SERVICE_SOURCE:
			return NGhostCommon_Event_NGhostEventSource_ProcessRESTGETRequest( this->m_source,
				requestedElement,
				cursor,
				parserOutputList );

		case NGHOST_EVENT_SERVICE_DESTINATION:
			return NGhostCommon_Event_NGhostEventDestination_ProcessRESTGETRequest( this->m_destination,
				requestedElement,
				cursor,
				parserOutputList );

		default:
			return NGhostCommon_Event_NGhostEvent_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );
	}
}
