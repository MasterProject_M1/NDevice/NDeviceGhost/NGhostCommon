#include "../../include/NGhostCommon.h"

// ---------------------------------------------
// struct NGhostCommon::Event::NGhostEventSource
// ---------------------------------------------

/**
 * Build event source instance
 *
 * @param ip
 * 		The sender ip
 * @param deviceType
 * 		The device type
 * @param deviceUUID
 * 		The device UUID (can be NULL)
 *
 * @return the built instance
 */
__ALLOC NGhostEventSource *NGhostCommon_Event_NGhostEventSource_Build( const char *ip,
	NDeviceType deviceType,
	const char *deviceUUID )
{
	// Output
	__OUTPUT NGhostEventSource *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostEventSource ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) )
		|| ( deviceUUID != NULL
				&& !( out->m_uuid = NLib_Chaine_Dupliquer( deviceUUID ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_device = deviceType < NDEVICES_TYPE ?
		deviceType
		: NDEVICES_TYPE;

	// OK
	return out;
}

/**
 * Destroy event sender instance
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Event_NGhostEventSource_Destroy( NGhostEventSource **this )
{
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_uuid );
	NFREE( (*this) );
}

/**
 * Get sender ip
 *
 * @param this
 * 		This instance
 *
 * @return the sender ip
 */
const char *NGhostCommon_Event_NGhostEventSource_GetIP( const NGhostEventSource *this )
{
	return this->m_ip;
}

/**
 * Get sender device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostCommon_Event_NGhostEventSource_GetDeviceType( const NGhostEventSource *this )
{
	return this->m_device;
}

/**
 * Get sender device UUID
 *
 * @param this
 * 		This instance
 *
 * @return the device UUID
 */
const char *NGhostCommon_Event_NGhostEventSource_GetDeviceUUID( const NGhostEventSource *this )
{
	return this->m_uuid;
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Event_NGhostEventSource_BuildParserOutputListInternal( const NGhostEventSource *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostEventSourceService serviceType )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
			: "",
		NGhostCommon_Service_Event_NGhostEventSourceService_GetName( serviceType ) );

	// Add entry
	switch( serviceType )
	{
		case NGHOST_EVENT_SOURCE_SERVICE_DEVICE_TYPE:
			return this->m_device != NDEVICES_TYPE ?
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NDeviceCommon_Type_NDeviceType_GetName( this->m_device ) )
				: NFALSE;
		case NGHOST_EVENT_SOURCE_SERVICE_DEVICE_UUID:
			return this->m_uuid != NULL ?
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_uuid )
				: NFALSE;
		case NGHOST_EVENT_SOURCE_SERVICE_IP:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_ip );

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventSource_BuildParserOutputList( const NGhostEventSource *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostEventSourceService serviceType = (NGhostEventSourceService)0;

	// Iterate
	for( ; serviceType < NGHOST_EVENT_SOURCE_SERVICES; serviceType++ )
		// Add
		NGhostCommon_Event_NGhostEventSource_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Event_NGhostEventSource_ProcessRESTGETRequest( const NGhostEventSource *this,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostEventSourceService serviceType;

	// Process service
	switch( serviceType = NGhostCommon_Service_Event_NGhostEventSourceService_FindService( requestedElement,
		cursor ) )
	{
		default:
			return NGhostCommon_Event_NGhostEventSource_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );

		case NGHOST_EVENT_SOURCE_SERVICE_ROOT:
			return NGhostCommon_Event_NGhostEventSource_BuildParserOutputList( this,
				"",
				parserOutputList );
	}
}
