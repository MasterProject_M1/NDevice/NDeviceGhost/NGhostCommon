#define NGHOSTCOMMON_SERVICE_STATUS_REFERAL_NGHOSTSTATUSREFERALSERVICE_INTERNE
#include "../../../../include/NGhostCommon.h"

// -----------------------------------------------------------------------
// enum NGhostCommon::Service::Status::Referal::NGhostStatusReferalService
// -----------------------------------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostStatusReferalService NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NGhostStatusReferalService serviceType;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_STATUS_REFERAL_SERVICES;
	}

	// Look for
	for( serviceType = (NGhostStatusReferalService)0; serviceType < NGHOST_STATUS_REFERAL_SERVICES; serviceType++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostStatusReferalServiceName[ serviceType ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Quit
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_STATUS_REFERAL_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_GetName( NGhostStatusReferalService serviceType )
{
	return NGhostStatusReferalServiceName[ serviceType ];
}
