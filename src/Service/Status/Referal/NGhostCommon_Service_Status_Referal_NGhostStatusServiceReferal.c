#include "../../../../include/NGhostCommon.h"

// -------------------------------------------------------------------------
// struct NGhostCommon::Service::Status::Referal::NGhostStatusServiceReferal
// -------------------------------------------------------------------------

/**
 * Build service referal
 *
 * @param ip
 * 		The ip
 * @param port
 * 		The port
 *
 * @return the built instance
 */
__ALLOC NGhostStatusServiceReferal *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Build( const char *ip,
	NU32 port )
{
	// Output
	__OUTPUT NGhostStatusServiceReferal *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostStatusServiceReferal ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate IP
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build client
	if( !( out->m_httpClient = NHTTP_Client_NClientHTTP_Construire( ip,
		port,
		out ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy referal
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Destroy( NGhostStatusServiceReferal **this )
{
	// Destroy http client
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_httpClient );

	// Free
	NFREE( (*this)->m_ip );
	NFREE( (*this) );
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the referal IP
 */
const char *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( const NGhostStatusServiceReferal *this )
{
	return this->m_ip;
}

/**
 * Get http client
 *
 * @param this
 * 		This instance
 *
 * @return the http client
 */
NClientHTTP *NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetHTTPClient( const NGhostStatusServiceReferal *this )
{
	return this->m_httpClient;
}
