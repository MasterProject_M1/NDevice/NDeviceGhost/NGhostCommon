#include "../../../../include/NGhostCommon.h"

// ------------------------------------------------------------------
// struct NGhostCommon::Service::Status::Referal::NGhostStatusReferal
// ------------------------------------------------------------------

/**
 * Build ghost status referal
 *
 * @return the status referal instance
 */
__ALLOC NGhostStatusReferal *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Build( void )
{
	// Output
	__OUTPUT NGhostStatusReferal *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostStatusReferal ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build watcher list
	if( !( out->m_watcher = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build client list
	if( !( out->m_client = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NLib_Memoire_NListe_Detruire( &out->m_watcher );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NLib_Memoire_NListe_Detruire( &out->m_client );
		NLib_Memoire_NListe_Detruire( &out->m_watcher );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy ghost status referal
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Destroy( NGhostStatusReferal **this )
{
	// Destroy mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Destroy
	NLib_Memoire_NListe_Detruire( &(*this)->m_client );
	NLib_Memoire_NListe_Detruire( &(*this)->m_watcher );

	// Free
	NFREE( (*this) );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputListInternal( NGhostStatusReferal *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostStatusReferalService serviceType )
{
	// Key
	char key[ 128 ];

	// Ghost watcher referal
	const NGhostStatusServiceReferal *ghostStatusServiceReferal;

	// Iterator
	NU32 i = 0;

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
   			: "",
		NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_GetName( serviceType ) );

	switch( serviceType )
	{
		case NGHOST_STATUS_REFERAL_SERVICE_WATCHER:
			// Fill
			for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_watcher ); i++ )
				if( ( ghostStatusServiceReferal = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_watcher,
					i ) ) != NULL )
					NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
						key,
						NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( ghostStatusServiceReferal ) );

			// Check for emptyness
			if( i == 0 )
				// Add one empty entry
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					"" );
			break;
		case NGHOST_STATUS_REFERAL_SERVICE_CLIENT:
			// Fill
			for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_client ); i++ )
				if( ( ghostStatusServiceReferal = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_client,
					i ) ) != NULL )
					NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
						key,
						NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( ghostStatusServiceReferal ) );

			// Check for emptyness
			if( i == 0 )
				// Add one empty entry
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					"" );
			break;

		default:
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputList( NGhostStatusReferal *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostStatusReferalService serviceType = (NGhostStatusReferalService)0;

	// Iterate
	for( ; serviceType < NGHOST_STATUS_REFERAL_SERVICES; serviceType++ )
		NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTGETRequest( NGhostStatusReferal *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Service
	NGhostStatusReferalService serviceType;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Response
	__OUTPUT NReponseHTTP *response = NULL;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Lock
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Process
	switch( serviceType = NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_STATUS_REFERAL_SERVICE_ROOT:
			NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputList( this,
				"",
				parserOutputList );
			break;

		default:
			NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );
			break;
	}

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Build response
	response = NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );

	// OK?
	return response;
}

/**
 * Is IP already in list?
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip to check
 * @param serviceType
 * 		The service type
 *
 * @return if the IP is in list
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_IsIPAlreadyInList( const NGhostStatusReferal *this,
	const char *ip,
	NGhostStatusReferalService serviceType )
{
	// Iterator
	NU32 i;

	// Ghost watcher referal
	const NGhostStatusServiceReferal *ghostStatusServiceReferal;

	// Select list
	switch( serviceType )
	{
		case NGHOST_STATUS_REFERAL_SERVICE_CLIENT:
			for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_client ); i++ )
				// Get client referal
				if( ( ghostStatusServiceReferal = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_client,
					i ) ) != NULL )
					// Check ip
					if( NLib_Chaine_Comparer( NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( ghostStatusServiceReferal ),
						ip,
						NTRUE,
						0 ) )
						// Already in list
						return NTRUE;
			break;
		case NGHOST_STATUS_REFERAL_SERVICE_WATCHER:
			for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_watcher ); i++ )
				// Get watcher referal
				if( ( ghostStatusServiceReferal = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_watcher,
					i ) ) != NULL )
					// Check ip
					if( NLib_Chaine_Comparer( NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( ghostStatusServiceReferal ),
						ip,
						NTRUE,
						0 ) )
						// Already in list
						return NTRUE;
			break;

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quit
			return NFALSE;
	}

	// Not found
	return NFALSE;
}

/**
 * Add client referal
 *
 * @param this
 * 		This instance
 * @param clientIP
 * 		The referal client ip
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddClient( NGhostStatusReferal *this,
	const char *clientIP )
{
	// Ghost referal
	NGhostStatusServiceReferal *ghostStatusServiceReferal;

	// Check existence
	if( NGhostCommon_Service_Status_Referal_NGhostStatusReferal_IsIPAlreadyInList( this,
		clientIP,
		NGHOST_STATUS_REFERAL_SERVICE_CLIENT ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Build watcher referal
	if( !( ghostStatusServiceReferal = NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Build( clientIP,
		NDeviceCommon_Type_NDeviceType_GetListeningPort( NDEVICE_TYPE_GHOST_CLIENT ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add
	return NLib_Memoire_NListe_Ajouter( this->m_client,
		ghostStatusServiceReferal );
}

/**
 * Add watcher referal
 *
 * @param this
 * 		This instance
 * @param watcherIP
 * 		The referal watcher ip
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcher( NGhostStatusReferal *this,
	const char *watcherIP )
{
	// Ghost referal
	NGhostStatusServiceReferal *ghostStatusWatcherReferal;

	// Check existence
	if( NGhostCommon_Service_Status_Referal_NGhostStatusReferal_IsIPAlreadyInList( this,
		watcherIP,
		NGHOST_STATUS_REFERAL_SERVICE_WATCHER ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		return NFALSE;
	}

	// Build watcher referal
	if( !( ghostStatusWatcherReferal = NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_Build( watcherIP,
		NDeviceCommon_Type_NDeviceType_GetListeningPort( NDEVICE_TYPE_GHOST_WATCHER ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add
	return NLib_Memoire_NListe_Ajouter( this->m_watcher,
		ghostStatusWatcherReferal );
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTPOSTRequest( NGhostStatusReferal *this,
	const char *requestedElement,
	NU32 *cursor,
	const char *userData )
{
	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Parser output
	const NParserOutput *parserOutput;

	// Service type
	NGhostStatusReferalService serviceType;

	// Check user data
	if( !userData )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Parse
	if( !( parserOutputList = NJson_Engine_Parser_Parse( userData,
		(NU32)strlen( userData ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Find what to add
	switch( serviceType = NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_STATUS_REFERAL_SERVICE_WATCHER:
		case NGHOST_STATUS_REFERAL_SERVICE_CLIENT:
			if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
				NGhostCommon_Service_Status_Referal_NGhostStatusReferalService_GetName( serviceType ),
				NTRUE,
				NTRUE ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Destroy parser output list
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );

				// Quit
				return NFALSE;
			}
			break;

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NFALSE;
	}

	// Check if entry is an ip
	if( !NLib_Module_Reseau_IP_EstAdresseIPv4( NParser_Output_NParserOutput_GetValue( parserOutput ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Lock
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Process
	switch( serviceType )
	{
		case NGHOST_STATUS_REFERAL_SERVICE_WATCHER:
			result = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcher( this,
				NParser_Output_NParserOutput_GetValue( parserOutput ) );
			break;
		case NGHOST_STATUS_REFERAL_SERVICE_CLIENT:
			result = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddClient( this,
				NParser_Output_NParserOutput_GetValue( parserOutput ) );
			break;

		default:
			break;
	}

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// OK
	return result;
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ActivateProtection( NGhostStatusReferal *this )
{
	// Lock
	NLib_Mutex_NMutex_Lock( this->m_mutex );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( NGhostStatusReferal *this )
{
	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

/**
 * Add watcher ip
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcherIP( NGhostStatusReferal *this,
	const char *ip )
{
	// Result
	__OUTPUT NBOOL result;

	// Lock
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Add
	result = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcher( this,
		ip );

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return result;
}

/**
 * Get watcher IP list
 *
 * @param this
 * 		This instance
 *
 * @return the watcher ip list
 */
__MUSTBEPROTECTED const NListe *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetWatcherIPList( const NGhostStatusReferal *this )
{
	return this->m_watcher;
}

/**
 * Get client IP list
 *
 * @param this
 * 		This instance
 *
 * @return the client ip list
 */
__MUSTBEPROTECTED const NListe *NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetClientIPList( const NGhostStatusReferal *this )
{
	return this->m_client;
}
