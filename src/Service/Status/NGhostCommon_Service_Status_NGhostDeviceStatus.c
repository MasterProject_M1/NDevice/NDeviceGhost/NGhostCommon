#include "../../../include/NGhostCommon.h"

// --------------------------------------------------------
// struct NGhostCommon::Service::Status::NGhostDeviceStatus
// --------------------------------------------------------

/**
 * Build client status
 *
 * @return the client status instance
 */
__ALLOC NGhostDeviceStatus *NGhostCommon_Service_Status_NGhostDeviceStatus_Build( void )
{
	// Output
	__OUTPUT NGhostDeviceStatus *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostDeviceStatus ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build referal
	if( !( out->m_referal = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save starting time
	out->m_startTimestamp = NLib_Temps_ObtenirTimestamp( );

	// OK
	return out;
}

/**
 * Destroy client status
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_NGhostDeviceStatus_Destroy( NGhostDeviceStatus **this )
{
	// Destroy
	NGhostCommon_Service_Status_Referal_NGhostStatusReferal_Destroy( &(*this)->m_referal );

	// Free
	NFREE( (*this) );
}

/**
 * Get the up time
 *
 * @param this
 * 		This instance
 *
 * @return the uptime
 */
NU64 NGhostCommon_Service_Status_NGhostDeviceStatus_GetUpTime( const NGhostDeviceStatus *this )
{
	return NLib_Temps_ObtenirTimestamp( ) - this->m_startTimestamp;
}

/**
 * Get the starting time
 *
 * @param this
 * 		This instance
 *
 * @return the starting time
 */
NU64 NGhostCommon_Service_Status_NGhostDeviceStatus_GetStartingTime( const NGhostDeviceStatus *this )
{
	return this->m_startTimestamp;
}

/**
 * Get processed requests count
 *
 * @param this
 * 		This instance
 *
 * @return the processed requests count
 */
NU32 NGhostCommon_Service_Status_NGhostDeviceStatus_GetProcessedRequestCount( const NGhostDeviceStatus *this )
{
	return this->m_processedRequestCount;
}

/**
 * Get ghost status referal
 *
 * @param this
 * 		This instance
 *
 * @return the ghost status referal instance
 */
NGhostStatusReferal *NGhostCommon_Service_Status_NGhostDeviceStatus_GetGhostStatusReferal( const NGhostDeviceStatus *this )
{
	return this->m_referal;
}

/**
 * Increase processed requests count
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Status_NGhostDeviceStatus_IncreaseProcessRequestCount( NGhostDeviceStatus *this )
{
	this->m_processedRequestCount++;
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Service_Status_NGhostDeviceStatus_BuildParserOutputListInternal( const NGhostDeviceStatus *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostStatusService serviceType )
{
	// Output
	__OUTPUT NBOOL result = NFALSE;
	// Uptime
	char *uptime;

	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
   			: "",
		NGhostCommon_Service_Status_NGhostStatusService_GetName( serviceType ) );

	switch( serviceType )
	{
		case NGHOST_STATUS_SERVICE_UP_TIME:
			if( ( uptime = NLib_Temps_ObtenirHeureFormate( NGhostCommon_Service_Status_NGhostDeviceStatus_GetUpTime( this ) ) ) !=
				NULL )
				NParser_Output_NParserOutputList_AddEntry( parserOutputList,
					key,
					uptime,
					NPARSER_OUTPUT_TYPE_STRING );
			break;
		case NGHOST_STATUS_SERVICE_STARTUP_TIMESTAMP:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				(NU32)NGhostCommon_Service_Status_NGhostDeviceStatus_GetStartingTime( this ) );

		case NGHOST_STATUS_SERVICE_TOTAL_REQUEST_COUNT:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NGhostCommon_Service_Status_NGhostDeviceStatus_GetProcessedRequestCount( this ) );

		case NGHOST_STATUS_SERVICE_REFERAL:
			// Lock
			NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ActivateProtection( this->m_referal );

			// Process
			result = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_BuildParserOutputList( this->m_referal,
				key,
				parserOutputList );

			// Unlock
			NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( this->m_referal );
			break;

		default:
			break;
	}

	// Error
	return result;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Service_Status_NGhostDeviceStatus_BuildParserOutputList( const NGhostDeviceStatus *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostStatusService serviceType = (NGhostStatusService)0;

	// Iterate
	for( ; serviceType < NGHOST_STATUS_SERVICES; serviceType++ )
		NGhostCommon_Service_Status_NGhostDeviceStatus_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTGETRequest( const NGhostDeviceStatus *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Service type
	NGhostStatusService serviceType;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Process
	switch( serviceType = NGhostCommon_Service_Status_NGhostStatusService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_STATUS_SERVICE_ROOT:
			NGhostCommon_Service_Status_NGhostDeviceStatus_BuildParserOutputList( this,
				"",
				parserOutputList );
			break;

		case NGHOST_STATUS_SERVICE_REFERAL:
			// Destroy parser output list
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Process
			return NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTGETRequest( this->m_referal,
				requestedElement,
				cursor,
				clientID );

		default:
			NGhostCommon_Service_Status_NGhostDeviceStatus_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );
	}

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The raw user data
 * @param clientID
 * 		The client id
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTPOSTRequest( NGhostDeviceStatus *this,
	const char *requestedElement,
	NU32 *cursor,
	const char *userData,
	NU32 clientID )
{
	// Process service
	switch( NGhostCommon_Service_Status_NGhostStatusService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_STATUS_SERVICE_REFERAL:
			return NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ProcessRESTPOSTRequest( this->m_referal,
				requestedElement,
				cursor,
				userData ) ?
				   NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
						NHTTP_CODE_200_OK,
						clientID )
				   : NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						clientID );

		default:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				clientID );
	}
}

/**
 * Add watcher IP
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip to add
 *
 * @return the watcher IP
 */
NBOOL NGhostCommon_Service_Status_NGhostDeviceStatus_AddWatcherIP( NGhostDeviceStatus *this,
	const char *ip )
{
	return NGhostCommon_Service_Status_Referal_NGhostStatusReferal_AddWatcherIP( this->m_referal,
		ip );
}
