#define NGHOSTCOMMON_SERVICE_STATUS_NGHOSTSTATUSSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// -------------------------------------------------------
// enum NGhostCommon::Service::Status::NGhostStatusService
// -------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostStatusService NGhostCommon_Service_Status_NGhostStatusService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	NGhostStatusService serviceType;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_STATUS_SERVICES;
	}

	// Look for
	for( serviceType = (NGhostStatusService)0; serviceType < NGHOST_STATUS_SERVICES; serviceType++ )
		// Check name
		if( NLib_Chaine_Comparer( buffer,
			NGhostStatusServiceName[ serviceType ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_STATUS_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Status_NGhostStatusService_GetName( NGhostStatusService serviceType )
{
	return NGhostStatusServiceName[ serviceType ];
}
