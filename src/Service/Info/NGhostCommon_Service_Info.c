#include "../../../include/NGhostCommon.h"

// -------------------------------------
// namespace NGhostCommon::Service::Info
// -------------------------------------

/**
 * Build parser output list informations
 *
 * @param ghostInfo
 * 		The ghost info details
 * @param parserOutputList
 * 		The parser output
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the basic parser output list informations
 */
NBOOL NGhostCommon_Service_Info_BuildParserOutputListInformation( const NGhostInfo *ghostInfo,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestingIP )
{
	// Date
	char *date;

	/* Fill with informations */
	// Device name
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_DEVICE_NAME,
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceName( ghostInfo ) );

	// Major version
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_MAJOR_VERSION,
		NGhostCommon_Service_Info_NGhostInfo_GetMajorVersion( ghostInfo ) );

	// Minor version
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_MINOR_VERSION,
		NGhostCommon_Service_Info_NGhostInfo_GetMinorVersion( ghostInfo ) );

	// Build info
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_BUILD_INFO,
		NGhostCommon_Service_Info_NGhostInfo_GetBuildString( ghostInfo ) );

	// Device type
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_DEVICE_TYPE,
		NDeviceCommon_Type_NDeviceType_GetName( NGhostCommon_Service_Info_NGhostInfo_GetDeviceType( ghostInfo ) ) );

	// Current formatted time
	if( ( date = NLib_Temps_ObtenirDateFormate( ) ) != NULL )
		NParser_Output_NParserOutputList_AddEntry( parserOutputList,
			NGHOST_CLIENT_SERVICE_INFO_CURRENT_FORMATTED_TIME,
			date,
			NPARSER_OUTPUT_TYPE_STRING );

	// Unique id
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_DEVICE_UNIQUE_ID,
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( ghostInfo ) );

	// Current timestamp
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_CURRENT_TIMESTAMP,
		(NU32)NLib_Temps_ObtenirTimestamp( ) );

	// Requesting IP
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGHOST_CLIENT_SERVICE_INFO_REQUESTING_IP,
		requestingIP );

	// OK
	return NTRUE;
}

/**
 * Create device info HTTP response
 *
 * @param responseCode
 * 		The http response code
 * @param ghostInfo
 * 		The ghost info details
 * @param requestingIP
 * 		The requesting IP
 * @param clientID
 * 		The client ID
 *
 * @return the basic informations on device
 */
NReponseHTTP *NGhostCommon_Service_Info_CreateDeviceInfo( NHTTPCode responseCode,
	const NGhostInfo *ghostInfo,
	const char *requestingIP,
	NU32 clientID )
{
	// Parser output
	NParserOutputList *parserOutputList;

	// Create parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Build informations
	if( !NGhostCommon_Service_Info_BuildParserOutputListInformation( ghostInfo,
		parserOutputList,
		requestingIP ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( responseCode,
		parserOutputList,
		clientID );
}
