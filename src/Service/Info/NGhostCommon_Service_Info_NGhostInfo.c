#include "../../../include/NGhostCommon.h"

// ----------------------------------------------
// struct NGhostCommon::Service::Info::NGhostInfo
// ----------------------------------------------

/**
 * Build device info
 *
 * @param deviceName
 * 		The device name
 * @param authorizedUsername
 * 		The authorized username
 * @param authorizedPassword
 * 		The authorized password
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 *		The minor version
 * @param deviceType
 * 		The device type
 *
 * @return the device info instance
 */
__ALLOC NGhostInfo *NGhostCommon_Service_Info_NGhostInfo_Build( const char *deviceName,
	const char *authorizedUsername,
	const char *authorizedPassword,
	NU32 majorVersion,
	NU32 minorVersion,
	NDeviceType deviceType )
{
	// Output
	__OUTPUT NGhostInfo *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostInfo ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_deviceType = deviceType;
	out->m_majorVersion = majorVersion;
	out->m_minorVersion = minorVersion;

	// Duplicate informations
	if( !( out->m_deviceName = NLib_Chaine_Dupliquer( deviceName ) )
		|| !( out->m_authorizedUsername = NLib_Chaine_Dupliquer( authorizedUsername ) )
		|| !( out->m_authorizedPassword = NLib_Chaine_Dupliquer( authorizedPassword ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_authorizedUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Extract build info
	NGhostCommon_Helper_BuildVersion( out->m_buildDetail,
		majorVersion,
		minorVersion );

	// Get UUID
	if( !( out->m_deviceUUID = NGhostCommon_Hardware_GetUniqueID( out->m_deviceType ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_authorizedPassword );
		NFREE( out->m_authorizedUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy device info
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Service_Info_NGhostInfo_Destroy( NGhostInfo **this )
{
	// Free
	NFREE( (*this)->m_authorizedPassword );
	NFREE( (*this)->m_authorizedUsername );
	NFREE( (*this)->m_deviceUUID );
	NFREE( (*this)->m_deviceName );
	NFREE( (*this) );
}

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetDeviceName( const NGhostInfo *this )
{
	return this->m_deviceName;
}

/**
 * Get major version
 *
 * @param this
 * 		This instance
 *
 * @return the major version
 */
NU32 NGhostCommon_Service_Info_NGhostInfo_GetMajorVersion( const NGhostInfo *this )
{
	return this->m_majorVersion;
}

/**
 * Get minor version
 *
 * @param this
 * 		This instance
 *
 * @return the minor version
 */
NU32 NGhostCommon_Service_Info_NGhostInfo_GetMinorVersion( const NGhostInfo *this )
{
	return this->m_minorVersion;
}

/**
 * Get build string
 *
 * @param this
 * 		This instance
 *
 * @return the build string
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetBuildString( const NGhostInfo *this )
{
	return this->m_buildDetail;
}

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostCommon_Service_Info_NGhostInfo_GetDeviceType( const NGhostInfo *this )
{
	return this->m_deviceType;
}

/**
 * Get device UUID
 *
 * @param this
 * 		This instance
 *
 * @return the device uuid
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( const NGhostInfo *this )
{
	return this->m_deviceUUID;
}

/**
 * Get authorized username
 *
 * @param this
 * 		This instance
 *
 * @return the authorized username
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedUsername( const NGhostInfo *this )
{
	return this->m_authorizedUsername;
}

/**
 * Get authorized password
 *
 * @param this
 * 		This instance
 *
 * @return the authorized password
 */
const char *NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedPassword( const NGhostInfo *this )
{
	return this->m_authorizedPassword;
}
