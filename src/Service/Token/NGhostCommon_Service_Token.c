#include "../../../include/NGhostCommon.h"

// --------------------------------------
// namespace NGhostCommon::Service::Token
// --------------------------------------

/**
 * Process token generation request
 *
 * @param tokenManager
 * 		The token manager
 * @param client
 * 		The requesting client
 * @param isAuthorized
 * 		Is authorized to get a token directly?
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_Token_ProcessTokenGenerationRequest( NGhostTokenManager *tokenManager,
	const NClientServeur *client,
	NBOOL isAuthorized )
{
	// HTTP response
	__OUTPUT NReponseHTTP *response = NULL;

	// Token
	const char *token;

	// Buffer
	char buffer[ 512 ];

	// Lock
	NGhostCommon_Token_NGhostTokenManager_ActivateProtection( tokenManager );

	// Generate token
	token = NGhostCommon_Token_RequestToken( tokenManager,
		isAuthorized,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ));

	// Build http content
	if( token != NULL )
		// Token generated
		snprintf( buffer,
			512,
			"{ \""NGHOSTCOMMON_SERVICE_TOKEN_KEY"\": \"%s\" }",
			token );
	else
		// Error
		snprintf( buffer,
			512,
			NDEVICE_COMMON_TYPE_GHOST_UNAUTHORIZED_MESSAGE );

	// Unlock
	NGhostCommon_Token_NGhostTokenManager_DeactivateProtection( tokenManager );

	// Build http response
	if( ( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( token == NULL ?
			NHTTP_CODE_401_UNAUTHORIZED
			: NHTTP_CODE_200_OK,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) != NULL )
		// Add content
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( response,
			buffer );

	// OK
	return response;
}

