#include "../../include/NGhostCommon.h"

// -------------------------------
// namespace NGhostCommon::Service
// -------------------------------

/**
 * Check HTTP authentication
 *
 * @param username
 * 		The username set in configuration
 * @param password
 * 		The password set in configuration
 * @param tokenManager
 * 		The token manager
 * @param request
 * 		The http request
 *
 * @return if the authentication succeeded
 */
NBOOL NGhostCommon_Service_CheckHTTPAuthentication( const char *username,
	const char *password,
	const NGhostTokenManager *tokenManager,
	const NRequeteHTTP *request )
{
	// Authentication clear string
	char *authenticationClear;

	// Username authentication attempt
	char *usernameAuthenticationAttempt;

	// Password authentication attemp
	char *passwordAuthenticationAttemp;

	// Cursor
	NU32 cursor = 0;

	// Element HTTP
	const NElementRequeteHTTP *element;

	// Element content
	const char *elementContent;

	// Token check result
	__OUTPUT NBOOL isTokenCorrect;

	if( ( element = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION ) ) != NULL )
	{
		// Read content
		if( !( elementContent = NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( element ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Quit
			return NFALSE;
		}

		// Skip method
		if( !NLib_Chaine_PlacerALaChaine( elementContent,
			"Basic ",
			&cursor ) )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_MALFORMED_URL );

			// Quit
			return NFALSE;
		}

		// Decode auth
		if( !( authenticationClear = NLib_Chaine_DecoderBase64( elementContent + cursor,
			(NU32)strlen( elementContent + cursor ) ) ) )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

			// Quit
			return NFALSE;
		}

		// Is it a token?
		if( !NLib_Chaine_EstChaineContient( authenticationClear,
			':' ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( authenticationClear );

			// Quit
			return NFALSE;
		}

		// Read username
		cursor = 0;
		if( !( usernameAuthenticationAttempt = NLib_Chaine_LireJusqua( authenticationClear,
			':',
			&cursor,
			NFALSE ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( authenticationClear );

			// Quit
			return NFALSE;
		}

		// Read password
		if( !( passwordAuthenticationAttemp = NLib_Chaine_LireJusqua( authenticationClear,
			'\0',
			&cursor,
			NFALSE ) ) )
		{
			// Lock token manager
			NGhostCommon_Token_NGhostTokenManager_ActivateProtection( (NGhostTokenManager*)tokenManager );

			// Check if this is a correct token
			isTokenCorrect = NGhostCommon_Token_IsTokenAuthorized( tokenManager,
				usernameAuthenticationAttempt );

			// Unlock token manager
			NGhostCommon_Token_NGhostTokenManager_DeactivateProtection( (NGhostTokenManager*)tokenManager );

			// Free
			NFREE( usernameAuthenticationAttempt );
			NFREE( authenticationClear );

			// OK?
			return isTokenCorrect;
		}

		// Check authentication
		if( !NLib_Chaine_Comparer( usernameAuthenticationAttempt,
			username,
			NTRUE,
			0 )
			|| !NLib_Chaine_Comparer( passwordAuthenticationAttemp,
			password,
			NTRUE,
			0 ) )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

			// Free
			NFREE( passwordAuthenticationAttemp );
			NFREE( usernameAuthenticationAttempt );
			NFREE( authenticationClear );

			// Quit
			return NFALSE;
		}

		// Free authentication
		NFREE( usernameAuthenticationAttempt );
		NFREE( passwordAuthenticationAttemp );
		NFREE( authenticationClear );
	}
	else
		// Unauthorized
		return NFALSE;

	// Authorized
	return NTRUE;
}

/**
 * Correct element to be processed
 *
 * @param element
 * 		The requested element
 * @param service
 * 		The api service
 *
 * @return the corrected element to be processed
 */
__ALLOC char *NGhostCommon_Service_CorrectElementToBeProcessed( const char *element,
	const char *service )
{
	// Output
	__OUTPUT char *output;

	// Cursor
	NU32 cursor = 0;

	// Get to the correct location
	if( !NLib_Chaine_PlacerALaChaine( element,
		service,
		&cursor ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NULL;
	}

	// Correct element
	if( !( output = NLib_Chaine_LireJusqua( element,
		'\0',
		&cursor,
		NFALSE ) ) )
		// Quit
		return NULL;

	// OK
	return output;
}

/**
 * Create unauthorized response
 *
 * @param clientID
 * 		The client id
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_CreateUnauthorizedResponse( NU32 clientID )
{
	// Create response
	__OUTPUT NReponseHTTP *response;

	// Buffer
	char buffer[ 2048 ];

	// Create response
	if( ( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( NHTTP_CODE_401_UNAUTHORIZED,
		clientID ) ) != NULL )
	{
		// Add unauthorized message
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( response,
			NDEVICE_COMMON_TYPE_GHOST_UNAUTHORIZED_MESSAGE );

		// Build realm header
		snprintf( buffer,
			2048,
			"Basic realm=\"%s\"",
			NDEVICE_COMMON_AUTHENTICATION_FAILURE_REALM_MESSAGE );

		// Add realm header
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_AjouterAttribut( response,
			NMOT_CLEF_REPONSE_HTTP_WWW_AUTHENTICATE,
			buffer );
	}

	// OK
	return response;
}

/**
 * Create internal error response
 *
 * @param clientID
 * 		The client ID
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_CreateInternalErrorResponse( NU32 clientID )
{
	// Create response
	__OUTPUT NReponseHTTP *response;

	// Create response
	if( ( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( NHTTP_CODE_500_INTERNAL_SERVER_ERROR,
		clientID ) ) != NULL )
		// Add unauthorized message
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( response,
			NDEVICE_COMMON_TYPE_GHOST_INTERNAL_ERROR_MESSAGE );

	// OK
	return response;
}

/**
 * Process basic services
 *
 * @param request
 * 		The http request
 * @param ghostCommon
 * 		The ghost common
 * @param isAuthenticationCorrect
 * 		Return NTRUE if the authentication is valid
 * @param serviceType
 * 		The service type
 * @param cursor
 * 		The cursor in requested element
 * @param client
 * 		The requesting client
 * @param contactingIP
 * 		The contacting IP
 *
 * @return the http response if correct common request, NULL else
 */
__ALLOC NReponseHTTP *NGhostCommon_Service_ProcessBasicService( const NRequeteHTTP *request,
	void *ghostCommon,
	__OUTPUT NBOOL *isAuthenticationCorrect,
	__OUTPUT NGhostCommonService *serviceType,
	NU32 *cursor,
	const NClientServeur *client,
	const char *contactingIP )
{
	// Response
	__OUTPUT NReponseHTTP *response;

	// Submodules
	NGhostTokenManager *ghostTokenManager;
	NAuthenticationManager *authenticationManager;
	NGhostDeviceStatus *ghostDeviceStatus;
	NGhostInfo *ghostInfo;

	// Get sub modules addresses
	if( !( ghostTokenManager = NGhostCommon_NGhostCommon_GetTokenManager( ghostCommon ) )
		|| !( authenticationManager = NGhostCommon_NGhostCommon_GetAuthenticationManager( ghostCommon ) )
		|| !( ghostDeviceStatus = (NGhostDeviceStatus*)NGhostCommon_NGhostCommon_GetDeviceStatus( ghostCommon ) )
		|| !( ghostInfo = (NGhostInfo*)NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Set incorrects authentication
		*isAuthenticationCorrect = NFALSE;

		// Quit
		return NULL;
	}

	// Increase processed request count
	NGhostCommon_Service_Status_NGhostDeviceStatus_IncreaseProcessRequestCount( ghostDeviceStatus );

	// Check authentication
	*isAuthenticationCorrect = NGhostCommon_Service_CheckHTTPAuthentication( NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedUsername( ghostInfo ),
		NGhostCommon_Service_Info_NGhostInfo_GetAuthorizedPassword( ghostInfo ),
		ghostTokenManager,
		request );

	// Determine service type
	switch( *serviceType = NGhostCommon_Service_NGhostCommonService_FindService( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
		cursor,
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ) ) )
	{
		case NGHOST_COMMON_SERVICE_INFO:
			// Process request
			return NGhostCommon_Service_Info_CreateDeviceInfo( NHTTP_CODE_200_OK,
				ghostInfo,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NGHOST_COMMON_SERVICE_TOKEN:
			// Process request
			return NGhostCommon_Service_Token_ProcessTokenGenerationRequest( ghostTokenManager,
				client,
				*isAuthenticationCorrect );

		default:
			break;
	}

	// Check authentication
	if( !*isAuthenticationCorrect )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

		// Quit
		return NGhostCommon_Service_CreateUnauthorizedResponse( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
	}

	// Analyze service
	switch( *serviceType )
	{
		case NGHOST_COMMON_SERVICE_STATUS_GET:
			return NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTGETRequest( ghostDeviceStatus,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
				cursor,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
		case NGHOST_COMMON_SERVICE_STATUS_POST:
			return NGhostCommon_Service_Status_NGhostDeviceStatus_ProcessRESTPOSTRequest( ghostDeviceStatus,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
				cursor,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NGHOST_COMMON_SERVICE_AUTHENTICATION_ADD:
		case NGHOST_COMMON_SERVICE_AUTHENTICATION_DELETE:
		case NGHOST_COMMON_SERVICE_AUTHENTICATION_UPDATE:
		case NGHOST_COMMON_SERVICE_AUTHENTICATION_GET:
			return ( response = NDeviceCommon_Authentication_NAuthenticationManager_ProcessRESTRequest( authenticationManager,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ) + (NU32)strlen( NGhostCommon_Service_NGhostCommonService_GetServiceURI( *serviceType ) ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( request ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) != NULL ?
					response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NGHOST_COMMON_SERVICE_ACTION_LIST_GET:
		case NGHOST_COMMON_SERVICE_ACTION_LIST_POST:
		case NGHOST_COMMON_SERVICE_ACTION_LIST_PUT:
		case NGHOST_COMMON_SERVICE_ACTION_LIST_PATCH:
			return ( response = NGhostCommon_Action_NGhostActionList_ProcessRESTRequest( NGhostCommon_NGhostCommon_GetActionList( ghostCommon ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
				cursor,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				contactingIP ) ) != NULL ?
				   	response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_EMPTY_MESSAGE,
					   NHTTP_CODE_200_OK,
					   NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		default:
			// Quit
			return NULL;
	}
}
