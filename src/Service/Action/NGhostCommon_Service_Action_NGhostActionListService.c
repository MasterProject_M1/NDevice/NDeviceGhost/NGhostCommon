#define NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONLISTSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// -----------------------------------------------------------
// enum NGhostCommon::Service::Action::NGhostActionListService
// -----------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param actionList
 * 		The action list
 * @param actionIndex
 * 		The entry index (output)
 *
 * @return the service
 */
NGhostActionListService NGhostCommon_Service_Action_NGhostActionListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NGhostActionList *actionList,
	__OUTPUT NU32 *actionIndex )
{
	// Buffer
	char *buffer;

	// Service
	__OUTPUT NGhostActionListService serviceType;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_ACTION_LIST_SERVICES;
	}

	// Look for service
	for( serviceType = (NGhostActionListService)0; serviceType < NGHOST_ACTION_LIST_SERVICES; serviceType++ )
		switch( serviceType )
		{
			case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
				// Entry index?
				if( NLib_Chaine_EstUnNombreEntier( buffer,
					10 ) )
				{
					// Is it a correct index?
					if( ( *actionIndex = (NU32)strtol( buffer,
						NULL,
						10 ) ) < NGhostCommon_Action_NGhostActionList_GetCount( actionList ) )
					{
						// Free
						NFREE( buffer );

						// OK
						return serviceType;
					}
				}
				// Entry string?
				else
					if( ( *actionIndex = NGhostCommon_Action_NGhostActionList_FindActionFromBase64Hash( actionList,
						buffer ) ) != NERREUR )
					{
						// Free
						NFREE( buffer );

						// OK
						return serviceType;
					}
				break;

			default:
				if( NLib_Chaine_Comparer( buffer,
					NGhostActionListServiceName[ serviceType ],
					NTRUE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// OK
					return serviceType;
				}
				break;
		}
	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_ACTION_LIST_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Action_NGhostActionListService_GetName( NGhostActionListService serviceType )
{
	return NGhostActionListServiceName[ serviceType ];
}
