#define NGHOSTCOMMON_SERVICE_ACTION_NGHOSTACTIONSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// -------------------------------------------------------
// enum NGhostCommon::Service::Action::NGhostActionService
// -------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostActionService NGhostCommon_Service_Action_NGhostActionService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Buffer
	char *buffer;

	// Service type
	__OUTPUT NGhostActionService serviceType;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_ACTION_SERVICES;
	}

	// Look for element
	for( serviceType = (NGhostActionService)0; serviceType < NGHOST_ACTION_SERVICES; serviceType++ )
		// Check service name
		if( NLib_Chaine_Comparer( buffer,
			NGhostActionServiceName[ serviceType ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Fount it!
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_ACTION_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Action_NGhostActionService_GetName( NGhostActionService serviceType )
{
	return NGhostActionServiceName[ serviceType ];
}

/**
 * Get service type
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service type
 */
NParserOutputType NGhostCommon_Service_Action_NGhostActionService_GetServiceType( NGhostActionService serviceType )
{
	return NGhostActionServiceType[ serviceType ];
}