#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTLISTSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// ---------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventListService
// ---------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param eventList
 * 		The event list (must be protected)
 * @param eventIndex
 * 		The event index (output)
 *
 * @return the service type or NGHOST_EVENT_LIST_SERVICES
 */
NGhostEventListService NGhostCommon_Service_Event_NGhostEventListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED NGhostEventList *eventList,
	__OUTPUT NU32 *eventIndex )
{
	// Output
	NGhostEventListService serviceType;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_EVENT_LIST_SERVICES;
	}

	// Look for
	for( serviceType = (NGhostEventListService)0; serviceType < NGHOST_EVENT_LIST_SERVICES; serviceType++ )
		// Check service
		switch( serviceType )
		{
			default:
				// Compare name
				if( NLib_Chaine_Comparer( buffer,
					NGhostEventListServiceName[ serviceType ],
					NTRUE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// OK
					return serviceType;
				}
				break;
			case NGHOST_EVENT_LIST_SERVICE_EVENT:
				// Index?
				if( NLib_Chaine_EstUnNombreEntier( buffer,
					10 ) )
				{
					// Get index
					*eventIndex = (NU32)strtol( buffer,
						NULL,
						10 );

					// Check index
					if( *eventIndex < NGhostCommon_Event_NGhostEventList_GetEventCount( eventList ) )
					{
						// Free
						NFREE( buffer );

						// Found it!
						return serviceType;
					}
				}
				// Hash?
				else
					// Do we find event hash?
					if( ( *eventIndex = NGhostCommon_Event_NGhostEventList_FindEventIndexFromEventHash( eventList,
						buffer ) ) != NERREUR )
					{
						// Free
						NFREE( buffer );

						// Found it!
						return serviceType;
					}
				break;
		}

	// Free
	NFREE( buffer );
	
	// Not found
	return NGHOST_EVENT_LIST_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventListService_GetName( NGhostEventListService serviceType )
{
	return NGhostEventListServiceName[ serviceType ];
}
