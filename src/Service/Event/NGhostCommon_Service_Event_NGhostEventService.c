#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// -----------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventService
// -----------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventService NGhostCommon_Service_Event_NGhostEventService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NGhostEventService out;

	// Buffer
	char *buffer;

	// Read buffer
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_EVENT_SERVICES;
	}

	// Look for
	for( out = (NGhostEventService)0; out < NGHOST_EVENT_SERVICES; out++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostEventServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found!
	return NGHOST_EVENT_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventService_GetName( NGhostEventService serviceType )
{
	return NGhostEventServiceName[ serviceType ];
}
