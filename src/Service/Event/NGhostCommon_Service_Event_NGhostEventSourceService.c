#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTSOURCESERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// -----------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventSourceService
// -----------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventSourceService NGhostCommon_Service_Event_NGhostEventSourceService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NGhostEventSourceService out;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_EVENT_SOURCE_SERVICES;
	}

	// Look for
	for( out = (NGhostEventSourceService)0; out < NGHOST_EVENT_SOURCE_SERVICES; out++ )
		// Check name
		if( NLib_Chaine_Comparer( buffer,
			NGhostEventSourceServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_EVENT_SOURCE_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventSourceService_GetName( NGhostEventSourceService serviceType )
{
	return NGhostEventSourceServiceName[ serviceType ];
}

/**
 * Get service full name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full service name
 */
const char *NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGhostEventSourceService serviceType )
{
	return NGhostEventSourceServiceFullName[ serviceType ];
}
