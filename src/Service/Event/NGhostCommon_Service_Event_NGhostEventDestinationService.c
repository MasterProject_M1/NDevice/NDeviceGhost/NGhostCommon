#define NGHOSTCOMMON_SERVICE_EVENT_NGHOSTEVENTDESTINATIONSERVICE_INTERNE
#include "../../../include/NGhostCommon.h"

// ----------------------------------------------------------------
// enum NGhostCommon::Service::Event::NGhostEventDestinationService
// ----------------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostEventDestinationService NGhostCommon_Service_Event_NGhostEventDestinationService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NGhostEventDestinationService serviceType;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_EVENT_DESTINATION_SERVICES;
	}

	// Look for
	for( serviceType = (NGhostEventDestinationService)0; serviceType < NGHOST_EVENT_DESTINATION_SERVICES; serviceType++ )
		// Check name
		if( NLib_Chaine_Comparer( NGhostEventDestinationServiceName[ serviceType ],
			buffer,
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it!
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_EVENT_DESTINATION_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostCommon_Service_Event_NGhostEventDestinationService_GetName( NGhostEventDestinationService serviceType )
{
	return NGhostEventDestinationServiceName[ serviceType ];
}

/**
 * Get full service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full service name
 */
const char *NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGhostEventDestinationService serviceType )
{
	return NGhostEventDestinationServiceFullName[ serviceType ];
}
