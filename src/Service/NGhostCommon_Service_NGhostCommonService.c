#define NGHOSTCOMMON_SERVICE_NGHOSTCOMMONSERVICE_INTERNE
#include "../../include/NGhostCommon.h"

// -----------------------------------------------
// enum NGhostCommon::Service::NGhostCommonService
// -----------------------------------------------

/**
 * Get name API
 *
 * @param serviceType
 * 		The service type
 *
 * @return the api URI
 */
const char *NGhostCommon_Service_NGhostCommonService_GetServiceURI( NGhostCommonService serviceType )
{
	return NGhostCommonServiceURL[ serviceType ];
}

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param httpRequestType
 * 		The http request type
 *
 * @return the service type
 */
NGhostCommonService NGhostCommon_Service_NGhostCommonService_FindService( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP httpRequestType )
{
	// Iterator
	__OUTPUT NGhostCommonService output = (NGhostCommonService)0;

	// Look for
	for( ; output < NGHOST_COMMON_SERVICES; output++ )
		// Check if this is the requested service
		if( NLib_Chaine_EstChaineCommencePar( requestedElement,
			NGhostCommonServiceURL[ output ] )
			&& httpRequestType == NGhostCommonServiceRequestType[ output ] )
		{
			// Save in cursor
			*cursor = (NU32)strlen( NGhostCommonServiceURL[ output ] );

			// Found it!
			return output;
		}


	// Not found
	return NGHOST_COMMON_SERVICES;
}