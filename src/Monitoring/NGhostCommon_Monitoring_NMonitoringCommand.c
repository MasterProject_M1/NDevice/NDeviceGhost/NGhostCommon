#define NGHOSTCOMMON_MONITORING_NMONITORINGCOMMAND_INTERNE
#include "../../include/NGhostCommon.h"

// -------------------------------------------------
// enum NGhostCommon::Monitoring::NMonitoringCommand
// -------------------------------------------------

/**
 * Parse command
 *
 * @param command
 * 		The command to parse
 *
 * @return the command type
 */
NMonitoringCommand NGhostCommon_Monitoring_NMonitoringCommand_Parse( const char *command )
{
	// Output
	__OUTPUT NMonitoringCommand out = (NMonitoringCommand)0;

	// Look for
	for( ; out < NMONITORING_COMMANDS; out++ )
		// Check
		if( NLib_Chaine_Comparer( command,
			NMonitoringCommandText[ out ],
			NFALSE,
			0 ) )
			// Found it
			return out;

	// Not found
	return NMONITORING_COMMANDS;
}

/**
 * Get command
 *
 * @param command
 * 		The command
 *
 * @return the command
 */
const char *NGhostCommon_Monitoring_NMonitoringCommand_GetCommand( NMonitoringCommand command )
{
	return NMonitoringCommandText[ command ];
}

/**
 * Get command description
 *
 * @param command
 * 		The command
 *
 * @return the command description
 */
const char *NGhostCommon_Monitoring_NMonitoringCommand_GetDescription( NMonitoringCommand command )
{
	return NMonitoringCommandDescription[ command ];
}
