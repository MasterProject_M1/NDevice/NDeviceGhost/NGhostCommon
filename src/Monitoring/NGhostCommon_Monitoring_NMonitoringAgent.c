#include "../../include/NGhostCommon.h"

// -------------------------------------------------
// struct NGhostCommon::Monitoring::NMonitoringAgent
// -------------------------------------------------

/**
 * Build monitoring agent instance
 *
 * @param tokenManager
 * 		The token manager
 * @param clientData
 * 		The client data
 * @param isContinue
 * 		Is continuing?
 * @param callbackUnknownCommand
 * 		The callback to call for unknown command
 *
 * @return NTRUE
 */
__ALLOC NMonitoringAgent *NGhostCommon_Monitoring_NMonitoringAgent_Build( NGhostTokenManager *tokenManager,
	void *clientData,
	NBOOL *isContinue,
	NBOOL ( ___cdecl *callbackUnknownCommand )( const char *command,
		void *clientData ) )
{
	// Output
	__OUTPUT NMonitoringAgent *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NMonitoringAgent ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_callbackUnknownCommand = callbackUnknownCommand;
	out->m_clientData = clientData;
	out->m_isContinue = isContinue;
	out->m_tokenManager = tokenManager;

	// Create thread
	if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostCommon_Monitoring_MonitoringThread,
		out,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy monitoring data
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_Destroy( NMonitoringAgent **this )
{
	// Destroy thread
	NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Free
	NFREE( (*this) );
}

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if is running
 */
NBOOL NGhostCommon_Monitoring_NMonitoringAgent_IsRunning( const NMonitoringAgent *this )
{
	return *this->m_isContinue;
}

/**
 * Stop main loop
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_Stop( NMonitoringAgent *this )
{
	*this->m_isContinue = NFALSE;
}

/**
 * Allow token generation
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Monitoring_NMonitoringAgent_AllowTokenGeneration( NMonitoringAgent *this )
{
	// Authorize token generation
	NGhostCommon_Token_NGhostTokenManager_AuthorizeTokenGeneration( this->m_tokenManager );
}

