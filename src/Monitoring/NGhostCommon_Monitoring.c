#include "../../include/NGhostCommon.h"

// ----------------------------------
// namespace NGhostCommon::Monitoring
// ----------------------------------

/**
 * Display help
 */
void NGhostCommon_Monitoring_DisplayHelp( void )
{
	// Iterator
	NMonitoringCommand command = (NMonitoringCommand)0;

	// Iterate
	for( ; command < NMONITORING_COMMANDS; command++ )
		// Display
		printf( "%s: %s\n",
			NGhostCommon_Monitoring_NMonitoringCommand_GetCommand( command ),
			NGhostCommon_Monitoring_NMonitoringCommand_GetDescription( command ) );
}

/**
 * Monitoring thread
 *
 * @param monitoringAgent
 * 		The monitoring data
 *
 * @return NTRUE
 */
__THREAD NBOOL NGhostCommon_Monitoring_MonitoringThread( NMonitoringAgent *monitoringAgent )
{
	// Buffer
	char buffer[ 256 ];

	// Main loop
	do
	{
		// Read
		if( NLib_Chaine_LireStdin2( buffer,
			256 ) )
			// Parse
			switch( NGhostCommon_Monitoring_NMonitoringCommand_Parse( buffer ) )
			{
				default:
				case NMONITORING_COMMAND_HELP:
					NGhostCommon_Monitoring_DisplayHelp( );
					break;

				case NMONITORING_COMMAND_ALLOW_TOKEN_GENERATION:
					NGhostCommon_Monitoring_NMonitoringAgent_AllowTokenGeneration( monitoringAgent );
					break;

				case NMONITORING_COMMAND_QUIT:
					NGhostCommon_Monitoring_NMonitoringAgent_Stop( monitoringAgent );
					break;
			}

		// Wait
		NLib_Temps_Attendre( 16 );
	} while( NGhostCommon_Monitoring_NMonitoringAgent_IsRunning( monitoringAgent ) );

	// OK
	return NTRUE;
}
