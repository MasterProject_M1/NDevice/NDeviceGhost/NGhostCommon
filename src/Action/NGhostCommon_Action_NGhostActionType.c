#define NGHOSTCOMMON_ACTION_NGHOSTACTIONTYPE_INTERNE
#include "../../include/NGhostCommon.h"

// -------------------------------------------
// enum NGhostCommon::Action::NGhostActionType
// -------------------------------------------

/**
 * Get sentence associated with action
 *
 * @param actionType
 * 		The action type
 *
 * @return the sentence
 */
const char *NGhostCommon_Action_NGhostActionType_GetSentence( NGhostActionType actionType )
{
	return NGhostDeviceActionTypeSentence[ actionType ];
}

/**
 * Get action name
 *
 * @param actionType
 * 		The action type
 *
 * @return the name
 */
const char *NGhostCommon_Action_NGhostActionType_GetName( NGhostActionType actionType )
{
	return NGhostDeviceActionTypeName[ actionType ];
}

/**
 * Parser action type
 *
 * @param actionType
 * 		The action type
 *
 * @return the action type
 */
NGhostActionType NGhostCommon_Action_NGhostActionType_ParseActionType( const char *actionType )
{
	// Output
	__OUTPUT NGhostActionType out = (NGhostActionType)0;

	// Look for
	for( ; out < NGHOST_ACTION_TYPES; out++ )
		// Check
		if( NLib_Chaine_Comparer( actionType,
			NGhostDeviceActionTypeName[ out ],
			NTRUE,
			0 ) )
			// Found it!
			return out;

	// Not found
	return NGHOST_ACTION_TYPES;
}
