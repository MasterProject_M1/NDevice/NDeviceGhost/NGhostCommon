#include "../../include/NGhostCommon.h"
#include "../../../../../NLib/NLib/NLib/include/NLib/Module/Reseau/Client/NLib_Module_Reseau_Client.h"

// ---------------------------------------------
// struct NGhostCommon::Action::NGhostActionList
// ---------------------------------------------

/**
 * Build action list
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param ghostStatusReferal
 * 		The ghost status referal
 *
 * @return the action list instance
 */
__ALLOC NGhostActionList *NGhostCommon_Action_NGhostActionList_Build( NAuthenticationManager *authenticationManager,
	void *ghostStatusReferal,
	void *ghostCommon )
{
	// Output
	__OUTPUT NGhostActionList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostActionList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_authenticationManager = authenticationManager;
	out->m_ghostStatusReferal = ghostStatusReferal;
	out->m_ghostCommon = ghostCommon;

	// Build action list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NGhostCommon_Action_NGhostAction_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy action list
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostActionList_Destroy( NGhostActionList **this )
{
	// Destroy action list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this) );
}

/**
 * Add an action
 *
 * @param this
 * 		This instance
 * @param action
 * 		The action to add
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_AddAction( NGhostActionList *this,
	__WILLBEOWNED NGhostAction *action )
{
	// Check if action exists
	if( NGhostCommon_Action_NGhostActionList_IsActionExist( this,
		action ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_ALREADY_EXISTS );

		// Destroy action
		NGhostCommon_Action_NGhostAction_Destroy( &action );

		// Quit
		return NFALSE;
	}

	// Add to list
	return NLib_Memoire_NListe_Ajouter( this->m_list,
		action );
}

/**
 * Is action already present?
 *
 * @param this
 * 		This instance
 * @param action
 * 		The action to check
 *
 * @return if action already present
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_IsActionExist( const NGhostActionList *this,
	const NGhostAction *action )
{
	// Iterator
	NU32 i = 0;

	// Action
	const NGhostAction *existingAction;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Get action
		if( ( existingAction = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) != NULL )
			// Check
			if( NGhostCommon_Action_NGhostAction_IsSame( existingAction,
				action ) )
				// Already present
				return NTRUE;

	// Not found
	return NFALSE;
}

/**
 * Find action index from hash
 *
 * @param this
 * 		This instance
 * @param hash
 * 		The action hash (base64)
 *
 * @return the action index or NERREUR
 */
__MUSTBEPROTECTED NU32 NGhostCommon_Action_NGhostActionList_FindActionFromBase64Hash( const NGhostActionList *this,
	const char *hash )
{
	// Iterator
	NU32 i = 0;

	// Action
	const NGhostAction *action;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Get action
		if( ( action = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) != NULL )
			// Correct hash?
			if( NLib_Chaine_Comparer( hash,
				NGhostCommon_Action_NGhostAction_GetBase64Hash( action ),
				NTRUE,
				0 ) )
				// Found it!
				return i;

	// Not found
	return NERREUR;
}

/**
 * Build parser output list (internal)
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param userData
 * 		The user data
 * @param serviceType
 * 		The service type
 * @param actionIndex
 * 		The action index
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_BuildParserOutputListInternal( const NGhostActionList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const NParserOutputList *userData,
	NGhostActionListService serviceType,
	NU32 actionIndex )
{
	// Key
	char key[ 128 ];

	// Action
	const NGhostAction *action;

	// Process service
	switch( serviceType )
	{
		case NGHOST_ACTION_LIST_SERVICE_COUNT:
			// Build key
			snprintf( key,
				128,
				"%s",
				NGhostCommon_Service_Action_NGhostActionListService_GetName( NGHOST_ACTION_LIST_SERVICE_COUNT ) );

			// Add value
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NLib_Memoire_NListe_ObtenirNombre( this->m_list ) );
		case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
			// Build key
			snprintf( key,
				128,
				"%d",
				actionIndex );

			// Get action
			if( ( action = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				actionIndex ) ) != NULL )
				// Process
				return NGhostCommon_Action_NGhostAction_BuildParserOutputList( action,
					key,
					userData,
					parserOutputList );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_BuildParserOutputList( const NGhostActionList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const NParserOutputList *userData )
{
	// Service type
	NGhostActionListService serviceType = (NGhostActionListService)0;

	// Iterator
	NU32 i;

	// Iterate
	for( ; serviceType < NGHOST_ACTION_LIST_SERVICES; serviceType++ )
		switch( serviceType )
		{
			case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
				for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
					NGhostCommon_Action_NGhostActionList_BuildParserOutputListInternal( this,
						parserOutputList,
						userData,
						serviceType,
						i );
				break;

			default:
				NGhostCommon_Action_NGhostActionList_BuildParserOutputListInternal( this,
					parserOutputList,
					userData,
					serviceType,
					0 );
				break;
		}

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ProcessRESTGETRequest( NGhostActionList *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostActionListService serviceType;

	// Action index
	NU32 actionIndex;

	// Action
	NGhostAction *ghostAction;

	// Process service
	switch( serviceType = NGhostCommon_Service_Action_NGhostActionListService_FindService( requestedElement,
		cursor,
		this,
		&actionIndex ) )
	{
		case NGHOST_ACTION_LIST_SERVICE_ROOT:
			return NGhostCommon_Action_NGhostActionList_BuildParserOutputList( this,
				parserOutputList,
				userData );

		case NGHOST_ACTION_LIST_SERVICE_COUNT:
			return NGhostCommon_Action_NGhostActionList_BuildParserOutputListInternal( this,
				parserOutputList,
				userData,
				serviceType,
				actionIndex );

		case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
			// Get action
			if( ( ghostAction = (NGhostAction*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				actionIndex ) ) != NULL )
				return NGhostCommon_Action_NGhostAction_ProcessRESTGETRequest( ghostAction,
					requestedElement,
					cursor,
					userData,
					parserOutputList );
			else
				return NFALSE;

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}

/**
 * Process POST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param processedUserData
 * 		The user data (parameters)
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ProcessRESTPOSTRequest( NGhostActionList *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *processedUserData )
{
	// Action index
	NU32 actionIndex;

	// Action
	NGhostAction *ghostAction;

	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Process service
	switch( NGhostCommon_Service_Action_NGhostActionListService_FindService( requestedElement,
		cursor,
		this,
		&actionIndex ) )
	{
		case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
			// Get action
			if( ( ghostAction = (NGhostAction*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				actionIndex ) ) != NULL )
			{
				// Lock authentication manager
				NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( this->m_authenticationManager );

				// Execute action
				result = NGhostCommon_Action_NGhostAction_Execute( ghostAction,
					this->m_authenticationManager,
					processedUserData );

				// Unlock authentication manager
				NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( this->m_authenticationManager );
			}

			// OK?
			return result;

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The parameters
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ProcessRESTPUTRequest( NGhostActionList *this,
	const char *requestedElement,
	NU32 *cursor,
	NParserOutputList *userData,
	const char *contactingIP )
{
	// Action index
	NU32 actionIndex;

	// Action
	NGhostAction *ghostAction;

	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Process service
	switch( NGhostCommon_Service_Action_NGhostActionListService_FindService( requestedElement,
		cursor,
		this,
		&actionIndex ) )
	{
		case NGHOST_ACTION_LIST_SERVICE_ACTION_ENTRY:
			// Get action
			if( ( ghostAction = (NGhostAction*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				actionIndex ) ) != NULL )
				// Execute action
				result = NGhostCommon_Action_NGhostAction_AddActionToWatcher( ghostAction,
					this->m_ghostStatusReferal,
					userData,
					this->m_ghostCommon,
					contactingIP );

			// OK?
			return result;

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}

/**
 * Add action from action data
 *
 * @param this
 * 		This instance
 * @param actionData
 * 		The action data
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_AddActionFromActionData( NGhostActionList *this,
	const NParserOutputList *actionData,
	const char *keyRoot )
{
	// Action
	NGhostAction *action;

	// Build action
	if( !( action = NGhostCommon_Action_NGhostAction_Build2( actionData,
		keyRoot,
		NULL ) ) ) // TODO change with correct device UUID after presentation
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add action
	return NGhostCommon_Action_NGhostActionList_AddAction( this,
		action );
}

/**
 * Will extract and add all the actions to the action list
 *
 * @param this
 * 		This instance
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ExtractAndAddActionFromUserData( NGhostActionList *this,
	const NParserOutputList *userData )
{
	// Root key
	char keyRoot[ 32 ];

	// Parser output
	const NParserOutput *parserOutput;

	// Parser output list
	NParserOutputList *actionData;

	// Action count
	NU32 actionCount;

	// Iterator
	NU32 i;

	// Check parameter
	if( !userData )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Find action count
	if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostCommon_Service_Action_NGhostActionListService_GetName( NGHOST_ACTION_LIST_SERVICE_COUNT ),
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER
		|| ( actionCount = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) ) <= 0 )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Iterate action
	for( i = 0; i < actionCount; i++ )
	{
		// Build root key
		snprintf( keyRoot,
			32,
			"%d.",
			i );

		// Extract data informations
		if( ( actionData = NParser_Output_NParserOutputList_GetAllOutput( userData,
			keyRoot,
			NTRUE,
			NFALSE ) ) )
		{
			// Add action
			NGhostCommon_Action_NGhostActionList_AddActionFromActionData( this,
				actionData,
				keyRoot );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &actionData );
		}
	}

	// OK
	return NTRUE;
}

/**
 * Process REST PATCH request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The parameters
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ProcessRESTPATCHRequest( NGhostActionList *this,
	const char *requestedElement,
	NU32 *cursor,
	NParserOutputList *userData )
{
	// Action index
	NU32 actionIndex;

	// Process service
	switch( NGhostCommon_Service_Action_NGhostActionListService_FindService( requestedElement,
		cursor,
		this,
		&actionIndex ) )
	{
		case NGHOST_ACTION_LIST_SERVICE_ROOT:
			return NGhostCommon_Action_NGhostActionList_ExtractAndAddActionFromUserData( this,
				userData );

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param requestType
 * 		The request type
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 * @param deviceContactingIP
 * 		The IP used to contact the device
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostCommon_Action_NGhostActionList_ProcessRESTRequest( NGhostActionList *this,
	const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *userData,
	NU32 clientID,
	const char *deviceContactingIP )
{
	// User data
	NParserOutputList *processedUserData = NULL;

	// Response
	__OUTPUT NReponseHTTP *response = NULL;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Parse user data
	if( userData != NULL )
		processedUserData = NJson_Engine_Parser_Parse( userData,
			(NU32)strlen( userData ) );

	// Process
	switch( requestType )
	{
		case NTYPE_REQUETE_HTTP_GET:
			// Build parser output list
			if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Destroy
				if( processedUserData != NULL )
					NParser_Output_NParserOutputList_Destroy( &processedUserData );

				// Quit
				return NULL;
			}

			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_list );

			// Process GET request
			if( !NGhostCommon_Action_NGhostActionList_ProcessRESTGETRequest( this,
				requestedElement,
				cursor,
				processedUserData,
				parserOutputList ) )
				// Build response
				response = NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
					NHTTP_CODE_400_BAD_REQUEST,
					clientID );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_list );

			// Build packet
			if( NParser_Output_NParserOutputList_GetEntryCount( parserOutputList ) > 0
				&& !response )
				// Build response
				response = NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
					parserOutputList,
					clientID );
			else
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );
			break;

		// Execute an action
		case NTYPE_REQUETE_HTTP_POST:
			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_list );

			// Process
			response = NGhostCommon_Action_NGhostActionList_ProcessRESTPOSTRequest( this,
				requestedElement,
				cursor,
				processedUserData ) ?
					NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
						NHTTP_CODE_200_OK,
						clientID )
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						clientID );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_list );
			break;

		// Add action to known ghost watchers
		case NTYPE_REQUETE_HTTP_PUT:
			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_list );

			// Process
			response = NGhostCommon_Action_NGhostActionList_ProcessRESTPUTRequest( this,
				requestedElement,
				cursor,
				processedUserData,
				deviceContactingIP ) ?
					NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
						NHTTP_CODE_200_OK,
						clientID )
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						clientID );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_list );
			break;

		// Add a new action to the action set
		case NTYPE_REQUETE_HTTP_PATCH:
			// Lock
			NLib_Memoire_NListe_ActiverProtection( this->m_list );

			// Process
			response = NGhostCommon_Action_NGhostActionList_ProcessRESTPATCHRequest( this,
				requestedElement,
				cursor,
				processedUserData ) ?
					NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
						NHTTP_CODE_200_OK,
						clientID )
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						clientID );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_list );
			break;

		default:
			response = NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				clientID );
			break;
	}

	// Destroy
	if( processedUserData != NULL )
		NParser_Output_NParserOutputList_Destroy( &processedUserData );

	// OK?
	return response;
}

/**
 * Get action count
 *
 * @param this
 * 		This instance
 *
 * @return the action count
 */
NU32 NGhostCommon_Action_NGhostActionList_GetCount( const NGhostActionList *this )
{
		puts(" ALL AUTHENTICATION" );
	return NLib_Memoire_NListe_ObtenirNombre( this->m_list );
}

/**
 * Share action list
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The client on which to send
 * @param authenticationEntry
 * 		The authentication entry associated with destination
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostCommon_Action_NGhostActionList_ShareActionList( const NGhostActionList *this,
	NClientHTTP *httpClient,
	const NAuthenticationEntry *authenticationEntry,
	const char *contactingIP )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Json
	char *json;

	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Action
	const NGhostAction *action;

	// Key
	char keyRoot[ 128 ];

	// Iterator
	NU32 i;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Fill parser output list
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGhostCommon_Service_Action_NGhostActionListService_GetName( NGHOST_ACTION_LIST_SERVICE_COUNT ),
		NLib_Memoire_NListe_ObtenirNombre( this->m_list ) );

	// Add actions
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Get action
		if( ( action = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) != NULL )
		{
			// Build key
			snprintf( keyRoot,
				128,
				"%d.",
				i );

			// Add
			NGhostCommon_Action_NGhostAction_AddToParserOutputList( action,
				parserOutputList,
				keyRoot,
				contactingIP );
		}

	// Build json from parser output list
	json = NJson_Engine_Builder_Build( parserOutputList );

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Check
	if( !json )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Send
	switch( NDeviceCommon_Authentication_NAuthenticationEntry_GetMethod( authenticationEntry ) )
	{
		case NAUTHENTICATION_METHOD_TOKEN:
			result = NHTTP_Client_NClientHTTP_EnvoyerRequete3( httpClient,
				NTYPE_REQUETE_HTTP_PATCH,
				NDEVICE_COMMON_TYPE_GHOST_ACTION_SERVICE,
				json,
				(NU32)strlen( json ),
				NGHOST_COMMON_ACTION_CONNECTION_TIMEOUT,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry ),
				NULL );
			break;
		case NAUTHENTICATION_METHOD_PASSWORD:
			result = NHTTP_Client_NClientHTTP_EnvoyerRequete3( httpClient,
				NTYPE_REQUETE_HTTP_PATCH,
				NDEVICE_COMMON_TYPE_GHOST_ACTION_SERVICE,
				json,
				(NU32)strlen( json ),
				NGHOST_COMMON_ACTION_CONNECTION_TIMEOUT,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ),
				NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry ) );
			break;

		default:
			break;
	}

	// Free
	NFREE( json );

	// OK?
	return result;
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostCommon_Action_NGhostAction_ActivateProtection( NGhostActionList *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_list );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostCommon_Action_NGhostAction_DeactivateProtection( NGhostActionList *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );
}
