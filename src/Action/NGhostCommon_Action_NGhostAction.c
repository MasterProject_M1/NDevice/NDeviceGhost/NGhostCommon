#include "../../include/NGhostCommon.h"

// -----------------------------------------
// struct NGhostCommon::Action::NGhostAction
// -----------------------------------------

/**
 * Finalize action build
 *
 * @param this
 * 		This instance
 * @param deviceUUID
 * 		The device UUID
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_FinalizeActionBuild( NGhostAction *this,
	const char *deviceUUID )
{
	// MD5 state
	NMD5_CTX md5CTX;

	// Init MD5
	NLib_Math_MD5_Init( &md5CTX );

	// Add requested element
	NLib_Math_MD5_Update( &md5CTX,
		this->m_requestedElement,
		strlen( this->m_requestedElement ) );

	// Add sentence
	NLib_Math_MD5_Update( &md5CTX,
		this->m_sentence,
		strlen( this->m_sentence ) );

	// Add device UUID
	if( deviceUUID != NULL ) // TODO remove this when presentation is over
		NLib_Math_MD5_Update( &md5CTX,
			deviceUUID,
			strlen( deviceUUID ) );

	// Finalize MD5
	NLib_Math_MD5_Final( this->m_hash,
		&md5CTX );

	// Build base64 hash
	if( !( this->m_base64Hash = NLib_Chaine_ConvertirBase64_2( (char*)this->m_hash,
		16,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Build http client
	if( !( this->m_httpClient = NHTTP_Client_NClientHTTP_Construire( this->m_destinationHostname,
		this->m_destinationPort,
		this ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( this->m_base64Hash );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build action
 *
 * @param destinationHostname
 * 		The destination hostname
 * @param destinationPort
 * 		The destination port
 * @param requestedElement
 * 		The element to be requested
 * @param httpRequestType
 * 		The http request type
 * @param parserOutputList
 * 		The parser output list (json data)
 * @param actionType
 * 		The action type
 * @param sentence
 * 		The sentence associated with the action
 * @param deviceUUID
 * 		The device UUID
 *
 * @return the built action
 */
__ALLOC NGhostAction *NGhostCommon_Action_NGhostAction_Build( const char *destinationHostname,
	NU32 destinationPort,
	const char *requestedElement,
	NTypeRequeteHTTP httpRequestType,
	__WILLBEOWNED NParserOutputList *parserOutputList,
	NGhostActionType actionType,
	const char *sentence,
	const char *deviceUUID )
{
	// Output
	__OUTPUT NGhostAction *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAction ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Destroy parser output list
		if( parserOutputList != NULL )
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Duplicate data
	if( !( out->m_destinationHostname = NLib_Chaine_Dupliquer( destinationHostname ) )
		|| !( out->m_requestedElement = NLib_Chaine_Dupliquer( requestedElement ) )
		|| !( out->m_sentence = NLib_Chaine_Dupliquer( sentence ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_sentence );
		NFREE( out->m_requestedElement );
		NFREE( out->m_destinationHostname );
		NFREE( out );

		// Destroy parser output list
		if( parserOutputList != NULL )
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Save
	out->m_destinationPort = destinationPort;
	out->m_requestType = httpRequestType;
	out->m_type = actionType;

	// Generate json data
	if( parserOutputList != NULL )
	{
		if( !( out->m_requestData = NJson_Engine_Builder_Build2( parserOutputList,
			json_serialize_mode_packed ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Free
			NFREE( out->m_sentence );
			NFREE( out->m_requestedElement );
			NFREE( out->m_destinationHostname );
			NFREE( out );

			// Destroy parser output list
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NULL;
		}
	}
	else
		if( !( out->m_requestData = calloc( 1,
			sizeof( char ) ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Free
			NFREE( out->m_sentence );
			NFREE( out->m_requestedElement );
			NFREE( out->m_destinationHostname );
			NFREE( out );

			// Quit
			return NULL;
		}

	// Destroy parser output list
	if( parserOutputList != NULL )
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Finalize action
	if( !NGhostCommon_Action_NGhostAction_FinalizeActionBuild( out,
		deviceUUID ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_requestData );
		NFREE( out->m_sentence );
		NFREE( out->m_requestedElement );
		NFREE( out->m_destinationHostname );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Build an action from action data
 *
 * @param actionData
 * 		The action data
 * @param keyRoot
 * 		The key root
 * @param deviceUUID
 * 		The device UUID
 *
 * @return the build instance
 */
__ALLOC NGhostAction *NGhostCommon_Action_NGhostAction_Build2( const NParserOutputList *actionData,
	const char *keyRoot,
	const char *deviceUUID )
{
	// Key
	char key[ 256 ];

	// Iterator
	NGhostActionService ghostActionService;

	// Parser output
	const NParserOutput *parserOutput;

	// Output
	__OUTPUT NGhostAction *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAction ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Iterate services
	for( ghostActionService = (NGhostActionService)0; ghostActionService < NGHOST_ACTION_SERVICES; ghostActionService++ )
		switch( ghostActionService )
		{
			default:
				// Build key
				snprintf( key,
					256,
					"%s%s",
					keyRoot,
					NGhostCommon_Service_Action_NGhostActionService_GetName( ghostActionService ) );

				// Get output
				if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( actionData,
					key,
					NTRUE,
					NTRUE ) )
					|| NParser_Output_NParserOutput_GetType( parserOutput ) != NGhostCommon_Service_Action_NGhostActionService_GetServiceType( ghostActionService ) )
					if( ghostActionService != NGHOST_ACTION_SERVICE_JSON )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

						// Free
						NFREE( out->m_sentence );
						NFREE( out->m_base64Hash );
						NFREE( out->m_destinationHostname );
						NFREE( out->m_requestedElement );
						NFREE( out->m_requestData );
						NFREE( out );

						// Quit
						return NULL;
					}

				// Process data
				switch( ghostActionService )
				{
					case NGHOST_ACTION_SERVICE_TYPE:
						if( ( out->m_type = NGhostCommon_Action_NGhostActionType_ParseActionType( NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) >= NGHOST_ACTION_TYPES )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_SENTENCE:
						if( !( out->m_sentence = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_COPY );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_HOSTNAME:
						if( !( out->m_destinationHostname = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_COPY );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_PORT:
						if( ( out->m_destinationPort = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) ) >= (NU32)0xFFFF )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_REQUEST_URL:
						if( !( out->m_requestedElement = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_COPY );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_REQUEST_TYPE:
						if( ( out->m_requestType = NHTTP_Commun_HTTP_NTypeRequeteHTTP_Interpreter( NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) >= NTYPES_REQUETE_HTTP )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( out->m_sentence );
							NFREE( out->m_base64Hash );
							NFREE( out->m_destinationHostname );
							NFREE( out->m_requestedElement );
							NFREE( out->m_requestData );
							NFREE( out );

							// Quit
							return NULL;
						}
						break;

					case NGHOST_ACTION_SERVICE_JSON:
						if( parserOutput != NULL )
							if( !( out->m_requestData = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
							{
								// Notify
								NOTIFIER_ERREUR( NERREUR_COPY );

								// Free
								NFREE( out->m_sentence );
								NFREE( out->m_base64Hash );
								NFREE( out->m_destinationHostname );
								NFREE( out->m_requestedElement );
								NFREE( out->m_requestData );
								NFREE( out );

								// Quit
								return NULL;
							}
						break;

					default:
						break;
				}
				break;

			case NGHOST_ACTION_SERVICE_ROOT:
			case NGHOST_ACTION_SERVICE_JSON_PARAMETER:
			case NGHOST_ACTION_SERVICE_FULL_REQUEST_URL:
			case NGHOST_ACTION_SERVICE_HASH:
				break;
		}

	// Finalize
	if( !NGhostCommon_Action_NGhostAction_FinalizeActionBuild( out,
		deviceUUID ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_requestData );
		NFREE( out->m_sentence );
		NFREE( out->m_requestedElement );
		NFREE( out->m_destinationHostname );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy action
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostAction_Destroy( NGhostAction **this )
{
	// Destroy http client
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_httpClient );

	// Free
	NFREE( (*this)->m_base64Hash );
	NFREE( (*this)->m_requestData );
	NFREE( (*this)->m_sentence );
	NFREE( (*this)->m_requestedElement );
	NFREE( (*this)->m_destinationHostname );
	NFREE( (*this) );
}

/**
 * Execute action
 *
 * @param this
 * 		This instance
 * @param authenticationManager
 * 		The authentication manager
 * @param parameterList
 * 		The parameters list
 *
 * @return if the operation started successfully
 */
NBOOL NGhostCommon_Action_NGhostAction_Execute( NGhostAction *this,
	__MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const NParserOutputList *parameterList )
{
	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Json data
	char *json = NULL;

	// Result
	__OUTPUT NBOOL result;

	// Look for authentication entry
	if( !( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForIPAndPort( authenticationManager,
		this->m_destinationHostname,
		this->m_destinationPort ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_AUTH_FAILED );

		// Quit
		return NFALSE;
	}

	// Build request data
	if( this->m_requestData != NULL )
		if( parameterList != NULL )
		{
			if( !( json = NGhostCommon_Action_SetJsonArgument( this->m_requestData,
				parameterList,
				NGhostCommon_Service_Action_NGhostActionService_GetName( NGHOST_ACTION_SERVICE_JSON_PARAMETER ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}
		}
		else
			if( !( json = NLib_Chaine_Dupliquer( this->m_requestData ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_COPY );

				// Quit
				return NFALSE;
			}

	// Remove trailing slash
	NLib_Chaine_SupprimerCaractere( json,
		'\\' );

	// Send the request
	result = NHTTP_Client_NClientHTTP_EnvoyerRequete3( this->m_httpClient,
		this->m_requestType,
		this->m_requestedElement,
		json != NULL ?
			json
			: NULL,
		(NU32)( json != NULL ?
			strlen( json )
			: 0 ),
		NGHOST_COMMON_ACTION_CONNECTION_TIMEOUT,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry ) );

	// Free
	NFREE( json );

	// OK?
	return result;
}

/**
 * Add action to watcher internal
 *
 * @param this
 * 		This instance
 * @param ghostStatusWatcherReferal
 * 		The watcher referal
 * @param userData
 * 		The user data (parameters)
 * @param ghostCommon
 * 		The ghost common
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Action_NGhostAction_AddActionToWatcherInternal( const NGhostAction *this,
	const void *ghostStatusWatcherReferal,
	const NParserOutputList *userData,
	NGhostCommon *ghostCommon,
	const char *contactingIP )
{
	// HTTP client
	NClientHTTP *httpClient;

	// Authentication manager
	NAuthenticationManager *authenticationManager;

	// Iterator
	NU32 i;

	// Json
	char *json;

	// Ghost info
	const NGhostInfo *ghostInfo;

	// Parameters
	NParserOutputList *parameterList;
	const NParserOutput *parameter;

	// Data for event
	NParserOutputList *parserOutputList;

	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Get http client
	if( !( httpClient = NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetHTTPClient( ghostStatusWatcherReferal ) )
		|| !( authenticationManager = NGhostCommon_NGhostCommon_GetAuthenticationManager( ghostCommon ) )
		|| !( ghostInfo = NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	/* Build data to be sent */
	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add data
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_ACTION_HASH ),
		this->m_base64Hash );
	if( NLib_Chaine_Comparer( this->m_destinationHostname,
		"127.0.0.1",
		NTRUE,
		0 ) )
		NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
			NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGHOST_EVENT_DESTINATION_SERVICE_HOSTNAME ),
			contactingIP );
	else
		NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
			NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGHOST_EVENT_DESTINATION_SERVICE_HOSTNAME ),
			this->m_destinationHostname );
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventDestinationService_GetFullName( NGHOST_EVENT_DESTINATION_SERVICE_PORT ),
		this->m_destinationPort );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGHOST_EVENT_SOURCE_SERVICE_DEVICE_TYPE ),
		NDeviceCommon_Type_NDeviceType_GetName( NGhostCommon_Service_Info_NGhostInfo_GetDeviceType( ghostInfo ) ) );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostCommon_Service_Event_NGhostEventSourceService_GetFullName( NGHOST_EVENT_SOURCE_SERVICE_DEVICE_UUID ),
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( ghostInfo ) );
	if( userData != NULL )
	{
		if( ( parameterList = NParser_Output_NParserOutputList_GetAllOutput( userData,
			NGhostCommon_Service_Action_NGhostActionService_GetName( NGHOST_ACTION_SERVICE_JSON_PARAMETER ),
			NTRUE,
			NTRUE ) ) != NULL )
		{
			if( NParser_Output_NParserOutputList_GetEntryCount( parameterList ) > 0 )
				for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( parameterList ); i++ )
					if( ( parameter = NParser_Output_NParserOutputList_GetEntry( parameterList,
						i ) ) != NULL )
						NParser_Output_NParserOutputList_AddEntry3( parserOutputList,
							parameter );

			NParser_Output_NParserOutputList_Destroy( &parameterList );
		}
		if( ( parameter = NParser_Output_NParserOutputList_GetFirstOutput( userData,
			NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_EVENT_DELAY_BEFORE_ACTIVATION ),
			NTRUE,
			NTRUE ) ) != NULL
			&& NParser_Output_NParserOutput_GetType( parameter ) == NPARSER_OUTPUT_TYPE_INTEGER )
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				NGhostCommon_Service_Event_NGhostEventService_GetName( NGHOST_EVENT_SERVICE_EVENT_DELAY_BEFORE_ACTIVATION ),
				*(NU32*)NParser_Output_NParserOutput_GetValue( parameter ) );

	}

	// Build json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Lock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( authenticationManager );

	// Find authentication entry
	if( !( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( authenticationManager,
		this->m_destinationHostname,
		this->m_destinationPort ) ) )
	{
		// Unlock authentication manager
		NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( authenticationManager );

		// Notify
		NOTIFIER_ERREUR( NERREUR_AUTH_FAILED );

		// Free
		NFREE( json );

		// Quit
		return NFALSE;
	}

	// Send to watcher
	NHTTP_Client_NClientHTTP_EnvoyerRequete3( httpClient,
		NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_WATCHER_EVENT_SERVICE,
		json,
		(NU32)strlen( json ),
		NGHOST_COMMON_ACTION_CONNECTION_TIMEOUT,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry ) );

	// Unlock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( authenticationManager );

	// Free
	NFREE( json );

	// OK
	return result;
}

/**
 * Add action to watcher
 *
 * @param this
 * 		This instance
 * @param ghostStatusReferal
 * 		The ghost status referal
 * @param userData
 * 		The user data
 * @param ghostCommon
 * 		The ghost common
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_AddActionToWatcher( const NGhostAction *this,
	const void *ghostStatusReferal,
	const NParserOutputList *userData,
	void *ghostCommon,
	const char *contactingIP )
{
	// IP list
	const NListe *watcherList;

	// IP
	const NGhostStatusServiceReferal *watcher;

	// Iterator
	NU32 i;

	// Lock
	NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ActivateProtection( (NGhostStatusReferal*)ghostStatusReferal );

	// Get ip list
	if( !( watcherList = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetWatcherIPList( ghostStatusReferal ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock
		NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( (NGhostStatusReferal*)ghostStatusReferal );

		// Quit
		return NFALSE;
	}

	// Process
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( watcherList ); i++ )
		// Get IP
		if( ( watcher = NLib_Memoire_NListe_ObtenirElementDepuisIndex( watcherList,
			i ) ) != NULL )
			NGhostCommon_Action_NGhostAction_AddActionToWatcherInternal( this,
				watcher,
				userData,
				ghostCommon,
				contactingIP );

	// Unlock
	NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( (NGhostStatusReferal*)ghostStatusReferal );

	// OK
	return NTRUE;
}

/**
 * Is action the same?
 *
 * @param this
 * 		This instance
 * @param action
 * 		The other action to compare with
 *
 * @return if the actions are the same
 */
NBOOL NGhostCommon_Action_NGhostAction_IsSame( const NGhostAction *this,
	const NGhostAction *action )
{
	return (NBOOL)!memcmp( this->m_hash,
		action->m_hash,
		16 );
}

/**
 * Get base64 hash
 *
 * @param this
 * 		This instance
 *
 * @return the base64 hash
 */
const char *NGhostCommon_Action_NGhostAction_GetBase64Hash( const NGhostAction *this )
{
	return this->m_base64Hash;
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostCommon_Action_NGhostAction_BuildParserOutputListInternal( const NGhostAction *this,
	const char *keyRoot,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostActionService serviceType )
{
	// Key
	char key[ 128 ];

	// URL
	char url[ 256 ];

	// Json
	char *json;

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
   			: "",
		NGhostCommon_Service_Action_NGhostActionService_GetName( serviceType ) );

	// Add value
	switch( serviceType )
	{
		case NGHOST_ACTION_SERVICE_HASH:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_base64Hash );

		case NGHOST_ACTION_SERVICE_TYPE:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NGhostCommon_Action_NGhostActionType_GetName( this->m_type ) );

		case NGHOST_ACTION_SERVICE_SENTENCE:
			// Set parameters
			if( ( json = NGhostCommon_Action_SetJsonArgument( this->m_sentence,
				userData,
				NGhostCommon_Service_Action_NGhostActionService_GetName( NGHOST_ACTION_SERVICE_JSON_PARAMETER ) ) ) != NULL )
			{
				// Add
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					json );

				// Free
				NFREE( json );

				// OK
				return NTRUE;
			}
			else
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_sentence );

		case NGHOST_ACTION_SERVICE_HOSTNAME:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_destinationHostname );

		case NGHOST_ACTION_SERVICE_PORT:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_destinationPort );

		case NGHOST_ACTION_SERVICE_REQUEST_URL:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_requestedElement );

		case NGHOST_ACTION_SERVICE_FULL_REQUEST_URL:
			// Build url
			snprintf( url,
				256,
				"%s:%d%s",
				this->m_destinationHostname,
				this->m_destinationPort,
				this->m_requestedElement );

			// Add
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				url );

		case NGHOST_ACTION_SERVICE_REQUEST_TYPE:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( this->m_requestType ) );

		case NGHOST_ACTION_SERVICE_JSON:
			// Set parameters
			if( ( json = NGhostCommon_Action_SetJsonArgument( this->m_requestData,
				userData,
				NGhostCommon_Service_Action_NGhostActionService_GetName( NGHOST_ACTION_SERVICE_JSON_PARAMETER ) ) ) != NULL )
			{
				// Add
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					json );

				// Free
				NFREE( json );

				// OK
				return NTRUE;
			}
			else
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_requestData );

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_BuildParserOutputList( const NGhostAction *this,
	const char *keyRoot,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NGhostActionService ghostActionService = (NGhostActionService)0;

	// Iterate
	for( ; ghostActionService < NGHOST_ACTION_SERVICES; ghostActionService++ )
		NGhostCommon_Action_NGhostAction_BuildParserOutputListInternal( this,
			keyRoot,
			userData,
			parserOutputList,
			ghostActionService );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_ProcessRESTGETRequest( NGhostAction *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Action service
	NGhostActionService serviceType;

	// Process service
	switch( serviceType = NGhostCommon_Service_Action_NGhostActionService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_ACTION_SERVICE_ROOT:
			return NGhostCommon_Action_NGhostAction_BuildParserOutputList( this,
				"",
				userData,
				parserOutputList );

		default:
			return NGhostCommon_Action_NGhostAction_BuildParserOutputListInternal( this,
				"",
				userData,
				parserOutputList,
				serviceType );
	}
}

/**
 * Add detail to parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param contactingIP
 * 		The contacting IP
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_Action_NGhostAction_AddToParserOutputList( const NGhostAction *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	const char *contactingIP )
{
	// Ghost action service
	NGhostActionService ghostActionService = (NGhostActionService)0;

	// Key
	char key[ 128 ];

	// Add
	for( ; ghostActionService < NGHOST_ACTION_SERVICES; ghostActionService++ )
	{
		// Build key
		snprintf( key,
			128,
			"%s%s",
			keyRoot,
			NGhostCommon_Service_Action_NGhostActionService_GetName( ghostActionService ) );

		// Process
		switch( ghostActionService )
		{
			case NGHOST_ACTION_SERVICE_TYPE:
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NGhostCommon_Action_NGhostActionType_GetName( this->m_type ) );
				break;
			case NGHOST_ACTION_SERVICE_SENTENCE:
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_sentence );
				break;
			case NGHOST_ACTION_SERVICE_HOSTNAME:
				if( NLib_Chaine_Comparer( this->m_destinationHostname,
					"127.0.0.1",
					NTRUE,
					0 ) )
					NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
						key,
						contactingIP );
				else
					NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
						key,
						this->m_destinationHostname );
				break;
			case NGHOST_ACTION_SERVICE_PORT:
				NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					this->m_destinationPort );
				break;
			case NGHOST_ACTION_SERVICE_REQUEST_URL:
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_requestedElement );
				break;
			case NGHOST_ACTION_SERVICE_REQUEST_TYPE:
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( this->m_requestType ) );
				break;
			case NGHOST_ACTION_SERVICE_JSON:
				if( this->m_requestData != NULL
					&& strlen( this->m_requestData ) > 0 )
					NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
						key,
						this->m_requestData );
				break;

			default:
				break;
		}
	}

	// OK
	return NTRUE;
}
