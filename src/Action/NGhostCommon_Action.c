#include "../../include/NGhostCommon.h"

// ------------------------------
// namespace NGhostCommon::Action
// ------------------------------

/**
 * Set arguments in json
 *
 * @param json
 * 		The json text
 * @param userParameterList
 * 		The user parameter list
 * @param parameterKeyName
 * 		The parameter key name
 *
 * @return the new built json
 */
__ALLOC char *NGhostCommon_Action_SetJsonArgument( const char *json,
	const NParserOutputList *userParameterList,
	const char *parameterKeyName )
{
#define TAILLE_BUFFER		4096
	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// String buffer
	char stringBuffer[ 2048 ];

	// Cursor
	NU32 cursorInJson = 0,
		cursorInBuffer = 0;

	// New position
	NU32 newPositionJson = 0;

	// Argument list
	NParserOutputList *argumentList;

	// Argument
	const NParserOutput *argument;

	// Iterator
	NU32 i;

	// Check parameter
	if( !userParameterList )
		// Quit
		return NULL;

	// Extract arguments
	if( !( argumentList = NParser_Output_NParserOutputList_GetAllOutput( userParameterList,
		parameterKeyName,
		NTRUE,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Empty buffer
	memset( buffer,
		0,
		TAILLE_BUFFER );

	// Iterate arguments
	for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( argumentList ); i++ )
		// Get entry
		if( ( argument = NParser_Output_NParserOutputList_GetEntry( argumentList,
			i ) ) != NULL )
			// Look for json argument
			if( NLib_Chaine_PlacerAuCaractere( json,
				'#',
				&cursorInJson ) )
			{
				// Add basis to buffer
				if( cursorInBuffer + ( cursorInJson - 1 - newPositionJson ) + 1 < TAILLE_BUFFER )
					memcpy( buffer + cursorInBuffer,
						json + newPositionJson,
						cursorInJson - 1 - newPositionJson );

				// Increase cursor
				cursorInBuffer = (NU32)strlen( buffer );

				// Eliminate '"' character
				if( buffer[ cursorInBuffer - 1 ] == '"' )
					cursorInBuffer--;

				// Add parameter
				switch( NParser_Output_NParserOutput_GetType( argument ) )
				{
					case NPARSER_OUTPUT_TYPE_BOOLEAN:
						snprintf( stringBuffer,
							2048,
							*(NBOOL*)NParser_Output_NParserOutput_GetValue( argument ) ?
								"true"
								: "false" );
						break;
					case NPARSER_OUTPUT_TYPE_DOUBLE:
						snprintf( stringBuffer,
							2048,
							"%f",
							*(double*)NParser_Output_NParserOutput_GetValue( argument ) );
						break;
					case NPARSER_OUTPUT_TYPE_INTEGER:
						snprintf( stringBuffer,
							2048,
							"%d",
							*(NU32*)NParser_Output_NParserOutput_GetValue( argument ) );
						break;
					case NPARSER_OUTPUT_TYPE_STRING:
						snprintf( stringBuffer,
							2048,
							"\"%s\"",
							NParser_Output_NParserOutput_GetValue( argument ) );
						break;

					default:
						break;
				}

				// Add string buffer
				if( cursorInBuffer + strlen( stringBuffer ) + 1 < TAILLE_BUFFER )
					memcpy( buffer + cursorInBuffer,
						stringBuffer,
						strlen( stringBuffer ) );

				// Increase cursor
				cursorInBuffer = (NU32)strlen( buffer );

				// Look for closing '#'
				NLib_Chaine_PlacerAuCaractere( json,
					'#',
					&cursorInJson );

				// Save position
				newPositionJson = cursorInJson;

				// Correct position
				if( json[ newPositionJson ] == '"' )
					newPositionJson++;
			}

	// Copy string end
	if( cursorInBuffer + strlen( json ) - newPositionJson + 1 < TAILLE_BUFFER )
		memcpy( buffer + cursorInBuffer,
			json + newPositionJson,
			strlen( json ) - newPositionJson );

	// Destroy argument list
	NParser_Output_NParserOutputList_Destroy( &argumentList );

	// OK
	return NLib_Chaine_Dupliquer( buffer );
}
