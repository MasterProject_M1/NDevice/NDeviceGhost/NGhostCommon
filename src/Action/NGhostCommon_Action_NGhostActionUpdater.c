#include "../../include/NGhostCommon.h"

// ------------------------------------------------
// struct NGhostCommon::Action::NGhostActionUpdater
// ------------------------------------------------

/**
 * Build action updater
 *
 * @param actionList
 * 		The action list
 * @param device
 * 		The device
 * @param updateThread
 * 		The action update thread
 *
 * @return the action updater instance
 */
__ALLOC NGhostActionUpdater *NGhostCommon_Action_NGhostActionUpdater_Build( NGhostActionList *actionList,
	const void *device,
	NBOOL ( ___cdecl *updateThread )( NGhostActionUpdater *device ) )
{
	// Output
	__OUTPUT NGhostActionUpdater *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostActionUpdater ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_device = device;
	out->m_actionList = actionList;

	// Build update thread
	out->m_isUpdateThreadRunning = NTRUE;
	if( !( out->m_updateThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))updateThread,
		out,
		&out->m_isUpdateThreadRunning ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy action updater
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_Action_NGhostActionUpdater_Destroy( NGhostActionUpdater **this )
{
	// Destroy thread
	NLib_Thread_NThread_Detruire( &(*this)->m_updateThread );

	// Free
	NFREE( (*this) );
}

/**
 * Get action list
 *
 * @param this
 * 		This instance
 *
 * @return the action list
 */
NGhostActionList *NGhostCommon_Action_NGhostActionUpdater_GetActionList( NGhostActionUpdater *this )
{
	return this->m_actionList;
}

/**
 * Get device
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
const void *NGhostCommon_Action_NGhostActionUpdater_GetDevice( NGhostActionUpdater *this )
{
	return this->m_device;
}

/**
 * Is update thread running
 *
 * @param this
 * 		This instance
 *
 * @return if the update thread is running
 */
NBOOL NGhostCommon_Action_NGhostActionUpdater_IsUpdateThreadRunning( const NGhostActionUpdater *this )
{
	return this->m_isUpdateThreadRunning;
}
