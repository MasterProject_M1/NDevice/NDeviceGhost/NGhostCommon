#include "../include/NGhostCommon.h"

// ---------------------------------
// struct NGhostCommon::NGhostCommon
// ---------------------------------

/**
 * Get my ip
 *
 * @return the IP
 */
__PRIVATE __ALLOC char *NGhostCommon_NGhostCommon_GetMyIP( void )
{
	// Interface list
	NListe *interfaceList;

	// Interface
	const NInterfaceReseau *interface;

	// IP
	const char *readIP;
	__OUTPUT char *ip;

	// Iterator
	NU32 i;

	// Get interfaces
	if( !( interfaceList = NLib_Module_Reseau_Interface_TrouverTouteInterface( ) ) )
		// Using loopback...
		return NLib_Chaine_Dupliquer( "127.0.0.1" );

	// Iterate interfaces
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( interfaceList ); i++ )
		if( ( interface = NLib_Memoire_NListe_ObtenirElementDepuisIndex( interfaceList,
			i ) ) != NULL )
			if( ( readIP = NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresse( interface ) ) != NULL )
				// Check IP
				if( !NLib_Chaine_Comparer( readIP,
					"127.0.0.1",
					NTRUE,
					0 ) )
				{
					// Duplicate IP
					ip = NLib_Chaine_Dupliquer( readIP );

					// Free
					NLib_Memoire_NListe_Detruire( &interfaceList );

					// OK
					return ip;
				}

	// Destroy list
	NLib_Memoire_NListe_Detruire( &interfaceList );

	// Loopback...
	return NLib_Chaine_Dupliquer( "127.0.0.1" );
}

/**
 * Build common ghost basis
 *
 * @param listeningInterface
 * 		The listening interface (NULL for all interfaces)
 * @param listeningPort
 * 		The listening port
 * @param callbackPacketReceive
 * 		The receive callback
 * @param clientName
 * 		The client name
 * @param clientInstance
 * 		The client instance
 * @param deviceType
 * 		The device type
 * @param isAuthenticationManagerDataPersistent
 * 		Is authentication manager data persistent?
 * @param deviceName
 * 		The device name
 * @param authorizedUsername
 * 		The authorized username
 * @param authorizedPassword
 * 		The authorized password
 * @param majorVersion
 * 		The major version
 * @param minorVersion
 * 		The minor version
 * @param trustedTokenIPList
 * 		The trusted token ip list
 *
 * @return the basis instance
 */
__ALLOC NGhostCommon *NGhostCommon_NGhostCommon_Build( const char *listeningInterface,
	NU32 listeningPort,
	__ALLOC NReponseHTTP *( __CALLBACK *callbackPacketReceive )( const NRequeteHTTP*,
		const NClientServeur* ),
	const char *clientName,
	void *clientInstance,
	NDeviceType deviceType,
	NBOOL isAuthenticationManagerDataPersistent,
	const char *deviceName,
	const char *authorizedUsername,
	const char *authorizedPassword,
	NU32 majorVersion,
	NU32 minorVersion,
	const NGhostTrustedTokenIPList *trustedTokenIPList )
{
	// Output
	__OUTPUT NGhostCommon *out;

	// Log names
	char serverLogName[ 128 ],
		packetLogName[ 128 ];

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostCommon ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Get my IP
	if( !( out->m_ip = NGhostCommon_NGhostCommon_GetMyIP( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_NO_NETWORK_INTERFACE );

		// Free
		NFREE( out->m_ip );

		// Quit
		return NULL;
	}

	// Save
	out->m_deviceType = deviceType;

	// Create authentication manager
	if( !( out->m_authentication = NDeviceCommon_Authentication_NAuthenticationManager_Build( out->m_deviceType,
		isAuthenticationManagerDataPersistent,
		authorizedUsername,
		authorizedPassword,
		listeningPort,
		out->m_ip ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Create token manager
	if( !( out->m_tokenManager = NGhostCommon_Token_NGhostTokenManager_Build( NDeviceCommon_Type_NDeviceType_GetShortHeaderName( deviceType ),
		clientName,
		trustedTokenIPList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build info details
	if( !( out->m_info = NGhostCommon_Service_Info_NGhostInfo_Build( deviceName,
		authorizedUsername,
		authorizedPassword,
		majorVersion,
		minorVersion,
		deviceType ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Token_NGhostTokenManager_Destroy( &out->m_tokenManager );
		NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build empty status
	if( !( out->m_status = NGhostCommon_Service_Status_NGhostDeviceStatus_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Service_Info_NGhostInfo_Destroy( &out->m_info );
		NGhostCommon_Token_NGhostTokenManager_Destroy( &out->m_tokenManager );
		NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build action list
	if( !( out->m_action = NGhostCommon_Action_NGhostActionList_Build( out->m_authentication,
		NGhostCommon_Service_Status_NGhostDeviceStatus_GetGhostStatusReferal( out->m_status ),
		out ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Service_Status_NGhostDeviceStatus_Destroy( &out->m_status );
		NGhostCommon_Service_Info_NGhostInfo_Destroy( &out->m_info );
		NGhostCommon_Token_NGhostTokenManager_Destroy( &out->m_tokenManager );
		NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Create log names
	snprintf( packetLogName,
		128,
		"NHTTPPacket%s.log",
		clientName );
	snprintf( serverLogName,
		128,
		"NHTTPServer%s.log",
		clientName );

	// Build http server
	if( !( out->m_server = NHTTP_Serveur_NHTTPServeur_Construire( listeningPort,
		listeningInterface,
		NTRUE,
		NFALSE,
		serverLogName,
		packetLogName,
		NHTTP_SERVEUR_LOG_FICHIER_TAILLE_MAXIMALE_DEFAUT,
		NHTTP_SERVEUR_LOG_REPERTOIRE_ARCHIVAGE_DEFAUT,
		callbackPacketReceive,
		clientInstance ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Action_NGhostActionList_Destroy( &out->m_action );
		NGhostCommon_Service_Status_NGhostDeviceStatus_Destroy( &out->m_status );
		NGhostCommon_Service_Info_NGhostInfo_Destroy( &out->m_info );
		NGhostCommon_Token_NGhostTokenManager_Destroy( &out->m_tokenManager );
		NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy basis instance
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_NGhostCommon_Destroy( NGhostCommon **this )
{
	// Destroy
	NGhostCommon_Action_NGhostActionList_Destroy( &(*this)->m_action );
	NHTTP_Serveur_NHTTPServeur_Detruire( &(*this)->m_server );
	NGhostCommon_Token_NGhostTokenManager_Destroy( &(*this)->m_tokenManager );
	NGhostCommon_Service_Status_NGhostDeviceStatus_Destroy( &(*this)->m_status );
	NGhostCommon_Service_Info_NGhostInfo_Destroy( &(*this)->m_info );
	NDeviceCommon_Authentication_NAuthenticationManager_Destroy( &(*this)->m_authentication );

	// Free
	NFREE( (*this)->m_ip );
	NFREE( (*this) );
}

/**
 * Increase request count
 *
 * @param this
 * 		This instance
 */
void NGhostCommon_NGhostCommon_IncreaseRequestCount( NGhostCommon *this )
{
	NGhostCommon_Service_Status_NGhostDeviceStatus_IncreaseProcessRequestCount( this->m_status );
}

/**
 * Get device informations
 *
 * @param this
 * 		This instance
 *
 * @return the ghost informations
 */
const NGhostInfo *NGhostCommon_NGhostCommon_GetDeviceInfo( const NGhostCommon *this )
{
	return this->m_info;
}

/**
 * Get device status
 *
 * @param this
 * 		This instance
 *
 * @return the device status
 */
const NGhostDeviceStatus *NGhostCommon_NGhostCommon_GetDeviceStatus( const NGhostCommon *this )
{
	return this->m_status;
}

/**
 * Get authentication manager
 *
 * @param this
 * 		This instance
 *
 * @return the authentication manager
 */
NAuthenticationManager *NGhostCommon_NGhostCommon_GetAuthenticationManager( NGhostCommon *this )
{
	return this->m_authentication;
}

/**
 * Get token manager
 *
 * @param this
 * 		This instance
 *
 * @return the token manager
 */
NGhostTokenManager *NGhostCommon_NGhostCommon_GetTokenManager( NGhostCommon *this )
{
	return this->m_tokenManager;
}

/**
 * Get action list
 *
 * @param this
 * 		This instance
 *
 * @return the action list
 */
NGhostActionList *NGhostCommon_NGhostCommon_GetActionList( NGhostCommon *this )
{
	return this->m_action;
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostCommon_NGhostCommon_GetIP( const NGhostCommon *this )
{
	return this->m_ip;
}

/**
 * Add watcher
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The watcher ip device
 *
 * @return if the operation succeeded
 */
NBOOL NGhostCommon_NGhostCommon_AddWatcherIP( NGhostCommon *this,
	const char *ip )
{
	return NGhostCommon_Service_Status_NGhostDeviceStatus_AddWatcherIP( this->m_status,
		ip );
}
